Test the MiniSAT bindings.
  $ ./test.exe --all *.formula
  Solving 1.formula...
  Solution 1:
    a=true
    b=true
  Solution 2:
    a=true
    b=false
  Solution 3:
    a=false
    b=false
  Solving 2.formula...
  Solution 1:
    v_2_1=true
    v_2_3=false
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 2:
    v_2_1=true
    v_2_3=false
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 3:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=false
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 4:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=false
    v_2_2=false
  Solution 5:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=false
  Solution 6:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 7:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=false
  Solution 8:
    v_2_1=true
    v_2_3=false
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 9:
    v_2_1=true
    v_2_3=false
    v_2_0=false
    v_1_2=false
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 10:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 11:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=false
    v_1_0=false
    v_2_2=true
  Solution 12:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=false
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 13:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=false
    v_1_0=false
    v_2_2=false
  Solution 14:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=false
    v_1_1=false
    v_1_0=true
    v_2_2=false
  Solution 15:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 16:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=false
    v_1_0=true
    v_2_2=false
  Solution 17:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 18:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=false
    v_2_2=true
  Solution 19:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=false
    v_2_2=false
  Solution 20:
    v_2_1=true
    v_2_3=false
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=false
  Solution 21:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=false
  Solution 22:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=false
  Solution 23:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 24:
    v_2_1=false
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 25:
    v_2_1=false
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 26:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 27:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 28:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=false
    v_2_2=false
  Solution 29:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 30:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 31:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=false
    v_2_2=false
  Solution 32:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 33:
    v_2_1=false
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 34:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 35:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 36:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=false
    v_2_2=true
  Solution 37:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 38:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=false
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 39:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=false
    v_1_3=false
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 40:
    v_2_1=true
    v_2_3=false
    v_2_0=false
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 41:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=false
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 42:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 43:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 44:
    v_2_1=false
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 45:
    v_2_1=false
    v_2_3=false
    v_2_0=false
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 46:
    v_2_1=false
    v_2_3=false
    v_2_0=false
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 47:
    v_2_1=false
    v_2_3=true
    v_2_0=false
    v_1_2=false
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 48:
    v_2_1=false
    v_2_3=true
    v_2_0=false
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 49:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 50:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=false
    v_1_0=true
    v_2_2=true
  Solution 51:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 52:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 53:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 54:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 55:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 56:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=false
  Solution 57:
    v_2_1=false
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=false
  Solution 58:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=false
  Solution 59:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solution 60:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 61:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 62:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 63:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=false
    v_1_0=false
    v_2_2=true
  Solution 64:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=false
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 65:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=false
    v_1_0=false
    v_2_2=true
  Solution 66:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 67:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 68:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=false
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 69:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 70:
    v_2_1=true
    v_2_3=false
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 71:
    v_2_1=false
    v_2_3=false
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=false
    v_2_2=true
  Solution 72:
    v_2_1=false
    v_2_3=false
    v_2_0=true
    v_1_2=false
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 73:
    v_2_1=false
    v_2_3=false
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 74:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=true
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 75:
    v_2_1=false
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 76:
    v_2_1=true
    v_2_3=true
    v_2_0=true
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 77:
    v_2_1=true
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 78:
    v_2_1=false
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=true
  Solution 79:
    v_2_1=false
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solving 3.formula...
  unsat
