(lang dune 3.0)
(source (bitbucket uog-bigraph/bigraph-tools))
(license BSD-3-Clause)
(authors "Michele Sevegnani <michele.sevegnani@glasgow.ac.uk>"
         "Blair Archibald <blair.archibald@glasgow.ac.uk>")
(maintainers "Michele Sevegnani <michele.sevegnani@glasgow.ac.uk>"
         "Blair Archibald <blair.archibald@glasgow.ac.uk>")
(homepage "https://uog-bigraph.bitbucket.io")
(documentation "https://uog-bigraph.bitbucket.io/docs/index.html")

(using menhir 2.0)
(cram enable)

(package
  (name minisat)
  (version 0.5.3)
  (synopsis "Bindings for the MiniSAT SAT solver")
  (tags ("SAT solver" "org:dcs"))
  (depends
    (dune-configurator :build)))

(package
  (name minicard)
  (version 0.1.0)
  (synopsis "Bindings for the cardinality solver MiniCARD")
  (tags ("Pseudo-boolean solver" "org:dcs"))
  (depends
    (dune-configurator :build)))

(package
  (name gbs)
  (version 0.0.1)
  (synopsis "Bindings for the Glasgow Bigraph Solver")
  (depends
    (dune (and :build (>= 2.0.0)))
    (dune-configurator :build)
    (odoc :with-doc)))

(package
  (name bigraph)
  (version 2.0.0)
  (synopsis "Library to programmatically manipulate bigraphs and Bigraphical Reactive Systems")
  (description "bigraph is a library to programmaticaly manipulate bigraphs, reaction rules and Bigraphical Reactive Systems (BRS).")
  (tags ("bigraphs" "rewriting" "org:dcs"))
  (depends
    (ocaml (>= 4.08))
    (ppx_yojson_conv :build)
    (minisat (>= 0.5.3))
    (minicard (>= 0.1.0))
    (zarith (>= 1.3))))

(package
  (name bigrapher)
  (version 2.0.0)
  (synopsis "Implementation of Bigraphical Reactive Systems (BRS)")
  (description "BigraphER is an efficient implementation of Bigraphical Reactive Systems (BRS)
that supports bigraphs with sharing, stochastic and probabilistic reaction
rules, rule priorities, conditional rules, graphical output, and predicate checking. Models can be
exported to probabilistic model checker PRISM.")
  (tags ("bigraphs" "rewriting engine" "stochastic simulation" "org:dcs"))
  (depends
    (menhir :build)
    (ocaml (>= 4.08))
    (bigraph (>= 2.0.0))
    (cmdliner (> 1.0.4))
    progress))
