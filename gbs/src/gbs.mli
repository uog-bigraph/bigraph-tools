(** This module provides a basic interface to the GBS solver.

    @author Blair Archibald *)

type node = {
  ident : int;
  lbl : string;
  name : string;
  in_deg : int list; (* Only if root is parent *)
  out_deg : int list (* Only if site is child *);
}
(** Flattened node representation *)

type t = { nodes : node list; adj : (int * int) list }
(** Type of flattened digraphs *)

val sols_to_str :
  ((int * int) list * (int * int) list * (int * int) list) list -> string

val match_one :
  t -> t -> ((int * int) list * (int * int) list * (int * int) list) list

val match_all :
  t -> t -> ((int * int) list * (int * int) list * (int * int) list) list

val equal : t -> t -> bool
val count_sols : t -> t -> int

(**/**)
