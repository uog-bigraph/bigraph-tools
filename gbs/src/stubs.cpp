#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS

#include "glasgow_bigraph_lib.hh"
#include <cstring>

extern "C" {
#define CAML_NAME_SPACE
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>
}

/* Declaring the functions which should be accessible on the C side. */
extern "C" {
CAMLprim value ocaml_gbs_match_one(value unit);
CAMLprim value ocaml_gbs_match_all(value unit);
CAMLprim value ocaml_gbs_count_sols(value unit);
CAMLprim value ocaml_gbs_equal(value unit);
void ocaml_gbs_start_pattern(value size);
void ocaml_gbs_start_target(value size);
void ocaml_gbs_add_node(value id, value lbl, value name, value ideg, value odeg);
void ocaml_gbs_add_edge(value i, value j);
}

// For working with values/lists
static inline CAMLprim value triple(value a, value b, value c) {
  CAMLparam3(a, b, c);
  CAMLlocal1(triple);

  triple = caml_alloc(3, 0);

  Store_field(triple, 0, a);
  Store_field(triple, 1, b);
  Store_field(triple, 2, c);

  CAMLreturn(triple);
}

static inline CAMLprim value ocaml_tuple(value a, value b) {
  CAMLparam2(a, b);
  CAMLlocal1(tuple);

  tuple = caml_alloc(2, 0);

  Store_field(tuple, 0, a);
  Store_field(tuple, 1, b);

  CAMLreturn(tuple);
}

static inline CAMLprim value append(value hd, value tl) {
  CAMLparam2(hd, tl);
  CAMLlocal1(result);

  result = ocaml_tuple(hd, tl);

  CAMLreturn(result);
}

CAMLprim value gbs_res_to_ocaml(VertexToVertexMapping m) {
  CAMLparam0();
  CAMLlocal5(nds, edgs, hyp, res, tup);
  nds = Val_emptylist;
  edgs = Val_emptylist;
  hyp = Val_emptylist;

  if (!m.empty()) {
    std::map<int, int> nodes = gbs_getNodes(m);
    std::map<int, int> edges = gbs_getEdges(m);
    auto hedges = gbs_getHyp(m);

    for (const auto& n : nodes) {
      tup = ocaml_tuple(Val_int(n.first), Val_int(n.second));
      nds = append(tup, nds);
    }

    for (const auto& e : edges) {
      tup = ocaml_tuple(Val_int(e.first), Val_int(e.second));
      edgs = append(tup, edgs);
    }

    for (const auto& h : hedges) {
      tup = ocaml_tuple(Val_int(h.first), Val_int(h.second));
      hyp = append(tup, hyp);
    }
  }
  res = triple(nds, edgs, hyp);
  CAMLreturn(res);
}

void ocaml_gbs_start_pattern(value size) {
  CAMLparam1(size);
  gbs_start_pattern(Int_val(size));
  CAMLreturn0;
}

void ocaml_gbs_start_target(value size) {
  CAMLparam1(size);
  gbs_start_target(Int_val(size));
  CAMLreturn0;
}

void ocaml_gbs_add_node(value id, value lbl, value name, value ideg, value odeg) {
  CAMLparam5(id, lbl, name, ideg, odeg);
  CAMLlocal1(x);

  std::vector<int> ind;
  x = ideg;
  while (x != Val_emptylist) {
    int i = Int_val(Field(x, 0));
    ind.push_back(i);
    x = Field(x,1);
  }

  std::vector<int> outd;
  x = odeg;
  while (x != Val_emptylist) {
    int i = Int_val(Field(x, 0));
    outd.push_back(i);
    x = Field(x,1);
  }

  gbs_add_node(Int_val(id), String_val(lbl), String_val(name), ind, outd);
  CAMLreturn0;
}

void ocaml_gbs_add_edge(value i, value j) {
  CAMLparam2(i, j);
  gbs_add_edge(Int_val(i), Int_val(j));
  CAMLreturn0;
}

CAMLprim value ocaml_gbs_match_one(value unit) {
  CAMLparam1(unit);
  CAMLlocal2(ls, occs);
  ls = Val_emptylist;

  gbs_match_one();

  VertexToVertexMapping res = gbs_nextsol();
  if (!res.empty()) {
    occs = gbs_res_to_ocaml(res);
    ls = append(occs, ls);
  }

  CAMLreturn(ls);
}

CAMLprim value ocaml_gbs_match_all(value unit) {
  CAMLparam1(unit);
  CAMLlocal2(res, occs);

  res = Val_emptylist;

  gbs_match_all();
  VertexToVertexMapping sol = gbs_nextsol();
  while (!sol.empty()) {
    occs = gbs_res_to_ocaml(sol);
    res = append(occs, res);
    sol = gbs_nextsol();
  }

  CAMLreturn(res);
}

CAMLprim value ocaml_gbs_count_sols(value unit) {
  CAMLparam1(unit);
  CAMLlocal1(res);

  int sols = gbs_count_sols();
  res = Val_int(sols);

  CAMLreturn(res);
}

CAMLprim value ocaml_gbs_equal(value unit) {
  CAMLparam1(unit);
  CAMLlocal1(res);

  bool eq = gbs_equal();
  res = Val_bool(eq);

  CAMLreturn(res);
}
