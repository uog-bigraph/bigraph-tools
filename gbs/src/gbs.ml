external gbs_start_pattern : int -> unit = "ocaml_gbs_start_pattern"
external gbs_start_target : int -> unit = "ocaml_gbs_start_target"

external gbs_add_node :
  int -> string -> string -> int list -> int list -> unit
  = "ocaml_gbs_add_node"

external gbs_add_edge : int -> int -> unit = "ocaml_gbs_add_edge"

external gbs_match_one :
  unit -> ((int * int) list * (int * int) list * (int * int) list) list
  = "ocaml_gbs_match_one"

external gbs_match_all :
  unit -> ((int * int) list * (int * int) list * (int * int) list) list
  = "ocaml_gbs_match_all"

external gbs_count_sols : unit -> int = "ocaml_gbs_count_sols"
external gbs_equal : unit -> bool = "ocaml_gbs_equal"

(* Debugging *)
let sols_to_str sols =
  let sol_str (nodes, edgs, hyp) =
    let to_arr (i, j) = string_of_int i ^ "->" ^ string_of_int j in
    let es = edgs |> List.map to_arr |> String.concat "," in
    let ns = nodes |> List.map to_arr |> String.concat "," in
    let hs = hyp |> List.map to_arr |> String.concat "," in
    ns ^ "; " ^ es ^ "; " ^ hs
  in
  List.map sol_str sols |> String.concat "\n"

type node = {
  ident : int;
  lbl : string;
  name : string;
  in_deg : int list; (* Only if root is parent *)
  out_deg : int list (* Only if site is child *);
}

type t = { nodes : node list; adj : (int * int) list }
(** Type of flattened digraphs *)

let _print_node_list t =
  let print_node n = Printf.printf "Node: %d %s\n" n.ident n.lbl in
  List.iter print_node t.nodes;
  flush stdout

let setup_solver p t =
  gbs_start_pattern (List.length p.nodes);
  List.iter
    (fun (n : node) -> gbs_add_node n.ident n.lbl n.name n.in_deg n.out_deg)
    p.nodes;
  List.iter (fun (i, j) -> gbs_add_edge i j) p.adj;
  gbs_start_target (List.length t.nodes);
  List.iter
    (fun (n : node) -> gbs_add_node n.ident n.lbl n.name n.in_deg n.out_deg)
    t.nodes;
  List.iter (fun (i, j) -> gbs_add_edge i j) t.adj

let match_one p t =
  setup_solver p t;
  gbs_match_one ()

let match_all p t =
  setup_solver p t;
  gbs_match_all ()

let count_sols p t =
  setup_solver p t;
  gbs_count_sols ()

let equal p t =
  setup_solver p t;
  gbs_equal ()
