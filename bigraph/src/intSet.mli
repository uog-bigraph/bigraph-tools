(** This module implements finite sets of integers.

    @author Michele Sevegnani
    @author Blair Archibald *)

module type S = sig
  include Base.Set.S with type elt = int

  val to_string : t -> string
  (** Return the string representation of a set. Example:

      {[
        {1,4,7}
      ]} *)

  val pp : Format.formatter -> t -> unit
  (** Pretty printer. *)

  val is_singleton : t -> bool
  (** Test whether a set is a singleton or not. *)

  val of_int : int -> t
  (** [of_int n] returns set [{0, 1, ...., n - 1}]. *)

  val off : int -> t -> t
  (** [off i s] adds offset [i] to all the elements of set [s]. *)

  val fix : t -> Iso.t
  (** Compute an isomorphism to fix the numbering of a set. For example, the
      isomorphism for set [{2, 5, 6, 7}] is [{(2,0), (5,1), (6,2), (7,3)}]. *)

  val apply : Iso.t -> t -> t
  (** [apply iso s] applies [iso] to each element of [s]. Elements not mapped
      by [iso] are ignored. *)

  val union_list : t list -> t
  (** Compute the union of all the sets in a list. *)

  val merge : t list -> t list
  (** Merge sets with common elements. *)

  val iso_dom : Iso.t -> t
  (** Return the domain of an isomorphism. *)

  val iso_codom : Iso.t -> t
  (** Return the co-domain of an isomorphism. *)
end

module BitIntSet : S
module StdIntSet : S

module IntSet : S
(** Currently used IntSet type; allows toggling to bitsets *)

type t = IntSet.t

(**/**)
