type node = {
  ident : int;
  lbl : string; (* Probably make this an int colour instead *)
  name : string; (* Probably make this an int colour instead *)
  in_deg : int list; (* Only if root is parent *)
  out_deg : int list (* Only if site is child *);
}

(* Smart constructor *)
let node ?(ind = []) ?(od = []) ?(name = "") i l =
  { ident = i; lbl = l; name; in_deg = ind; out_deg = od }

type t = { nodes : node list; adj : Sparse.t }

let is_closed_link l =
  let open Link in
  Face.is_empty l.o && Face.is_empty l.i

let cartesian l l' =
  List.concat (List.map (fun e -> List.map (fun e' -> (e, e')) l') l)

(* Node creation functions *)
let create_ns ns f n b l =
  List.fold_left (fun acc i -> node ~name:(f i) (b + i) l :: acc)
    ns (List.init n (fun i -> i))

let gen_links (b : Big.t) nbase pbase ns adj =
  (* Counters to track ids *)
  let pnum = ref pbase in
  let _nopn = ref 0 in
  let _nclsd = ref 0 in
  let nlink = ref 0 in

  let new_open_port i prnt adj n =
    let pid = !pnum in
    let n =
      node ~name:(Printf.sprintf ":OPX:%s:%d:%d" n !nlink i) pid "LINK"
    in
    let adj' = Sparse.add (nbase + prnt) pid adj in
    incr pnum;
    (n, adj', pid)
  in

  let new_closed_port i prnt anchor adj =
    let pid = !pnum in
    let n = node ~name:(Printf.sprintf ":CLX:%d:%d" !nlink i) pid "LINK" in
    let adj' = Sparse.add (nbase + prnt) pid adj in
    let adj'' = Sparse.add pid anchor adj' in
    incr pnum;
    (n, adj'', pid)
  in

  let join_links pids adj =
    cartesian pids pids
    |> List.filter (fun (x, y) -> x != y)
    |> Sparse.add_list adj
  in

  let oname l =
    match Link.Face.min_elt_opt l.Link.o with
    | Some (Name n) -> n
    | None -> ""
  in

  let go l (nds, adj) =
    let ports_to_add =
        Link.Ports.fold
          (fun x ar acc -> List.init ar (fun _ -> x) @ acc)
          l.Link.p []
    in
    if is_closed_link l then (
      let anchor =
        node ~name:(Printf.sprintf "C_LINK_%d" !nlink) !pnum "ANCHOR"
      in
      let anchor_id = !pnum in
      incr pnum;
      let _, ns, adj, pids =
        List.fold_left
          (fun (i, new_, adj, pids) n ->
            let nd, adj', pid = new_closed_port i n anchor_id adj in
            (i + 1, nd :: new_, adj', pid :: pids))
          (0, nds, adj, []) ports_to_add
      in
      let adj' = join_links pids adj in
      ((anchor :: ns), adj'))
    else
      (* Add nodes including duplicates, e.g. (1,1), (1,2), (2,1) ->
         [1,1,2] *)
      let _, ns, adj, pids =
        List.fold_left
          (fun (i, new_, adj, pids) n ->
            let nd, adj', pid = new_open_port i n adj (oname l) in
            (i + 1, nd :: new_, adj', pid :: pids))
          (0, nds, adj, []) ports_to_add
      in
      let adj' = join_links pids adj in
      (ns, adj')
  in
  Link.Lg.fold
    (fun l (nodes, adj) ->
      go l (nodes, adj) |> fun (new_, adj) ->
      incr nlink;
      (new_, adj))
    b.l (ns, adj)

(* Solver assumes a very specific format: ROOTS|SITES|Nodes|LINKS *)
let flatten_target b =
  let open Big in
  let num_nodes = Nodes.size b.n in
  let root_base = 0 in
  let site_base = root_base + b.p.r in
  let node_base = site_base + b.p.s in
  let port_base = node_base + num_nodes in
  let total_nodes =
    port_base
    + Nodes.fold (fun _ c acc -> acc + Ctrl.arity c) b.n 0
    + Link.closed_edges b.l
  in

  (* Add Place Nodes *)
  let ns =
    Nodes.fold
        (fun i c acc ->
          node (i + node_base) (Ctrl.long_name c) ~name:(string_of_int i)
            :: acc)
        b.n []
    |> fun ns -> create_ns ns (fun i -> "ROOT" ^ string_of_int i) b.p.r root_base "ROOT"
    |> fun ns -> create_ns ns (fun i -> "SITE" ^ string_of_int i) b.p.s site_base "SITE"
  in
  (* Add Links *)
  let adj =
    Sparse.make total_nodes total_nodes
    |> Sparse.fold
      (fun i j acc -> Sparse.add (i + node_base) (j + node_base) acc)
      b.p.nn
    |> Sparse.fold
      (fun i j acc -> Sparse.add (i + root_base) (j + node_base) acc)
      b.p.rn
    |> Sparse.fold
      (fun i j acc -> Sparse.add (i + node_base) (j + site_base) acc)
      b.p.ns
  in

  (* Handle Link Graph *)
  let ns', adj' = gen_links b node_base port_base ns adj in

  { nodes = List.rev ns'; adj = adj' }

let flatten_pattern b =
  let open Big in
  let num_nodes = Nodes.size b.n in
  let node_base = 0 in
  let port_base = num_nodes in
  let total_nodes =
    port_base
    + Nodes.fold (fun _ c acc -> acc + Ctrl.arity c) b.n 0
    + Link.closed_edges b.l
  in

  let create_nodes =
    Nodes.fold
      (fun i c acc ->
         let nprnts =
           Sparse.fold
             (fun r n acc -> if i = n then r :: acc else acc)
             b.p.rn []
         in
         let nchld =
           Sparse.fold
             (fun n s acc -> if i = n then s :: acc else acc)
             b.p.ns []
         in
         node i (Ctrl.long_name c) ~ind:nprnts ~od:nchld :: acc)
      b.n []
  in

  (* Add Nodes *)
  let ns = create_nodes in

  (* Add Links *)
  let adj =
    Sparse.make total_nodes total_nodes
    |> Sparse.fold (fun i j acc -> Sparse.add i j acc) b.p.nn
  in

  let ns', adj' = gen_links b node_base port_base ns adj in

  { nodes = List.rev ns'; adj = adj' }

let toGBS g : Gbs.t =
  let togbs n : Gbs.node =
    {
      ident = n.ident;
      lbl = n.lbl;
      name = n.name;
      in_deg = n.in_deg;
      out_deg = n.out_deg;
    }
  in
  {
    nodes = List.map togbs g.nodes;
    adj = Sparse.fold (fun i j acc -> (i, j) :: acc) g.adj [];
  }
