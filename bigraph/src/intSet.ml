open Base
open Ppx_yojson_conv_lib.Yojson_conv.Primitives

module type S = sig
  include Base.Set.S with type elt = int

  val to_string : t -> string
  (** Return the string representation of a set. Example:

      {[
        {1,4,7}
      ]} *)

  val pp : Format.formatter -> t -> unit
  (** Pretty printer. *)

  val is_singleton : t -> bool
  (** Test whether a set is a singleton or not. *)

  val of_int : int -> t
  (** [of_int n] returns set [{0, 1, ...., n - 1}]. *)

  val off : int -> t -> t
  (** [off i s] adds offset [i] to all the elements of set [s]. *)

  val fix : t -> Iso.t
  (** Compute an isomorphism to fix the numbering of a set. For example, the
      isomorphism for set [{2, 5, 6, 7}] is [{(2,0), (5,1), (6,2), (7,3)}]. *)

  val apply : Iso.t -> t -> t
  (** [apply iso s] applies [iso] to each element of [s]. Elements not mapped
      by [iso] are ignored. *)

  val union_list : t list -> t
  (** Compute the union of all the sets in a list. *)

  val merge : t list -> t list
  (** Merge sets with common elements. *)

  val iso_dom : Iso.t -> t
  (** Return the domain of an isomorphism. *)

  val iso_codom : Iso.t -> t
  (** Return the co-domain of an isomorphism. *)
end

(* Derived IntSet operations given a set *)
module IntSetDerived (T : Set.S with type elt = int) = struct
  module S =
    Base.S_opt
      (T)
      (struct
        type t = int

        let pp = Format.pp_print_int
      end)

  include S

  let pp =
    S.pp ~open_b:Format.pp_open_hbox
      ~first:(fun out -> Format.pp_print_string out "{")
      ~last:(fun out -> Format.pp_print_string out "}")
      ~sep:(fun out -> Format.pp_print_string out ",")

  let to_string s =
    "{" ^ (List.map string_of_int (T.elements s) |> String.concat ",") ^ "}"

  (* Given a non-negative integer i return ordinal i = {0,1,....,i-1} i.e.
     set with cardinality i. *)
  let of_int i =
    assert (i >= 0);
    let rec loop i acc =
      match i with 0 -> acc | _ -> loop (i - 1) (T.add (i - 1) acc)
    in
    loop i T.empty

  (* Add offset i to every element in set s. *)
  let off i s = T.fold (fun x acc -> T.add (x + i) acc) s T.empty

  let apply iso s =
    T.fold
      (fun i acc ->
        match Iso.apply iso i with None -> acc | Some i' -> T.add i' acc)
      s T.empty

  (* Generates an isomorphism to fix the numbering of a set of int. [2;5;6;7]
     --> [(2,0),(5,1),(6,2),(7,3)] *)
  let fix s =
    T.elements (of_int (T.cardinal s))
    |> List.combine (T.elements s)
    |> Iso.of_list

  let union_list = List.fold_left (fun acc s -> T.union s acc) T.empty

  let rec augment s l =
    let l1, l2 = List.partition (fun s' -> T.is_empty (T.inter s s')) l in
    match l2 with [] -> (s, l1) | _ -> augment (union_list (s :: l2)) l1

  (* Merge sets with common elements *)
  let rec merge = function
    | [] -> []
    | s :: l' ->
        let s', l'' = augment s l' in
        s' :: merge l''

  let iso_dom i = Iso.fold (fun i _ acc -> T.add i acc) i T.empty
  let iso_codom i = Iso.fold (fun _ j acc -> T.add j acc) i T.empty
end

module StdIntSet = struct
  module ST = Set.Make (struct
    type t = int [@@deriving yojson]

    let compare = int_compare
  end)

  include IntSetDerived (ST)
end

module BitIntSet : S = struct
  include IntSetDerived (Bitset)

  (** Performance overrides *)
  let of_int = Bitset.ordinal
  let off i s = Bitset.shift i s
  let is_singleton s = cardinal s = 1
end

(* Where the magic can happen; Later we should parameterise everything with
   functors *)
(* module IntSet = StdIntSet *)
module IntSet = BitIntSet

type t = IntSet.t
