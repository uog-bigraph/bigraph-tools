(** Dynamically resizing Bitsets with immutable interface.

    @author Blair Archibald *)

include Set.S with type elt = int

val to_string : t -> string
val shift : int -> t -> t
val ordinal : int -> t
val t_of_yojson : Yojson.Safe.t -> t
val yojson_of_t : t -> Yojson.Safe.t

(* API Change for Set in ocaml 5.1 *)
val to_list : t -> int list
