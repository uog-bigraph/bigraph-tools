open Ppx_yojson_conv_lib.Yojson_conv.Primitives

type param =
    I of int [@name "ctrl_int"]
  | F of float [@name "ctr_float"]
  | S of string [@name "ctrl_string"]

let yojson_of_param param =
  match param with
    I i -> `Assoc [("ctrl_int", yojson_of_int i)]
  | F f -> `Assoc [("ctrl_float", yojson_of_float f)]
  | S s -> `Assoc [("ctrl_string", yojson_of_string s)]

let param_of_yojson json =
  match json with
    `Assoc [(name, yojson)] ->
    if String.equal name "ctrl_int" then I (int_of_yojson yojson)
    else if String.equal name "ctrl_float" then F (float_of_yojson yojson)
    else if String.equal name "ctrl_string" then S (string_of_yojson yojson)
    else failwith "error_name"
  | _ -> failwith "error_format_json"


type t = {
  s : string; [@key "ctrl_name"]
  p : param list; [@key "ctrl_params"]
  i : int [@key "ctrl_arity"]
} [@@deriving yojson]

let string_of_param = function
  | I i -> string_of_int i
  | F f -> string_of_float f
  | S s -> s

(* Name and parameters *)
let long_name = function
  | {s=c; p=[]; i=_} -> c
  | {s=c; p=ps; i=_} ->
      c ^ "(" ^ (List.map string_of_param ps |> String.concat ",") ^ ")"

let err = "Not a valid string representation of a control"

let parse_name s a =
  match Str.(split (regexp_string "(")) s with
  | [ n ] -> {s=n; p=[];i= a}
  | [ n; args ] ->
      let ps =
        String.(sub args 0 (length args - 1))
        |> Str.(split (regexp_string ","))
        |> List.map (fun p ->
               try I (int_of_string p)
               with Failure _ -> (
                 try F (float_of_string p) with Failure _ -> S p))
      in
      {s=n;p= ps; i=a}
  | _ -> invalid_arg err

let arity = function {s=_; p=_; i=ar} -> ar

(* Name, parameters and arity *)
let to_string c = long_name c ^ ":" ^ string_of_int (arity c)

let of_string s =
  match Str.(split (regexp_string ":")) s with
  | [ n; a ] -> parse_name n (int_of_string a)
  | _ -> invalid_arg err

(* Just the name of a control *)
let name = function {s=n; p=_; i=_} -> n

let param_compare = function
  | I x, I y -> Base.int_compare x y
  | F x, F y -> Float.compare x y
  | S x, S y -> String.compare x y
  | _ -> assert false

(* Only invoked when names are equal *)
let rec params_compare = function
  | [], [] -> 0
  | x :: xs, y :: ys -> (
      match param_compare (x, y) with
      | 0 -> params_compare (xs, ys)
      | res -> res)
  | _ -> assert false

let compare ctrl1 ctrl2 =
  match String.compare ctrl1.s ctrl2.s with 0 -> params_compare (ctrl1.p, ctrl2.p) | x -> x

let equal c0 c1 = compare c0 c1 = 0
