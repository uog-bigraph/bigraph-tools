(** The module of bigraphical controls.

    @author Michele Sevegnani *)

(** The type of control parameters. *)
type param =
  | I of int  (** Integer parameter *)
  | F of float  (** Float parameter *)
  | S of String.t  (** String parameter *)
[@@deriving yojson]

(** [Ctrl.{s; p; i}] creates a control of arity [i] named [s] with parameters
    [p]. *)

type t = { s : string; p : param list; i : int } [@@deriving yojson]

val arity : t -> int
(** [arity c] returns the arity of control [c]. *)

val compare : t -> t -> int
(** Comparison function. Equal control names imply equal arities and
    parameter types. {b Note, no check is performed.} *)

val equal : t -> t -> bool
(** Equality for type {!Ctrl.t}. *)

val long_name : t -> string
(** Return a string containing the name of a control and the values of its
    parameters. For example, [long_name {s="A"; i=2; p = [F 3.4; I 4]}]
    produces the string ["A(3.4,4)"]. *)

val name : t -> string
(** [name c] returns the name of control [c]. *)

val of_string : string -> t
(** Opposite of {!val:Ctrl.to_string}.

    @raise Invalid_argument if the input cannot be parsed. *)

val parse_name : string -> int -> t
(** [parse_name s ar] returns a control of arity [ar] obtained by parsing
    string [s]. *)

val to_string : t -> string
(** [to_string c] gives the string representation of control [c] in the form
    ["name:arity"] or ["name(par_0,par_1,...,par_n):arity"]. *)

(**/**)
