open Ppx_yojson_conv_lib.Yojson_conv.Primitives

let int_equal (a : int) (b : int) = a = b [@@inline]
let int_compare a b = a - b [@@inline]

module type OrderedType = sig
  type t [@@deriving yojson]

  val compare : t -> t -> int
end

(* TODO: Possibly define non-serialisable variants for internal only data *)
module Set = struct
  module type S = sig
    include Set.S

    val t_of_yojson : Yojson.Safe.t -> t
    val yojson_of_t : t -> Yojson.Safe.t
  end

  module Make (O : OrderedType) = struct
    include Set.Make (O)

    let t_of_yojson (json : Yojson.Safe.t) =
      list_of_yojson O.t_of_yojson json |> of_list

    let yojson_of_t (t : t) =
      to_seq t |> List.of_seq |> yojson_of_list O.yojson_of_t
  end
end

module Map = struct
  module type S = sig
    include Map.S

    val t_of_yojson : (Yojson.Safe.t -> 'a) -> Yojson.Safe.t -> 'a t
    val yojson_of_t : ('a -> Yojson.Safe.t) -> 'a t -> Yojson.Safe.t
  end

  module Make (O : OrderedType) = struct
    include Map.Make (O)

    let t_of_yojson data_of_yojson (json : Yojson.Safe.t) =
      list_of_yojson
        (fun (t : Yojson.Safe.t) ->
          match t with
          | `List [ key_yojson; data_yojson ] ->
              let key = O.t_of_yojson key_yojson in
              let data = data_of_yojson data_yojson in
              (key, data)
          | _ -> failwith "invalid format")
        json
      |> List.to_seq |> of_seq

    let yojson_of_t yojson_of_data (map : 'a t) =
      to_seq map |> List.of_seq
      |> yojson_of_list (fun (i, j) ->
             `List [ O.yojson_of_t i; yojson_of_data j ])
  end
end

module type HashedType = sig
  type t [@@deriving yojson]

  val equal : t -> t -> bool
  val hash : t -> int
end

module Hashtbl_yojson = struct
  module type S = sig
    include Hashtbl.S

    val t_of_yojson : (Yojson.Safe.t -> 'a) -> Yojson.Safe.t -> 'a t
    val yojson_of_t : ('a -> Yojson.Safe.t) -> 'a t -> Yojson.Safe.t
  end

  module Make (H : HashedType) = struct
    include Hashtbl.Make (H)

    let t_of_yojson data_of_yojson (json : Yojson.Safe.t) =
      list_of_yojson
        (fun (t : Yojson.Safe.t) ->
          match t with
          | `List [ key_yojson; data_yojson ] ->
              let key = H.t_of_yojson key_yojson in
              let data = data_of_yojson data_yojson in
              (key, data)
          | _ -> failwith "invalid format")
        json
      |> List.to_seq |> of_seq

    let yojson_of_t yojson_of_data (map : 'a t) =
      to_seq map |> List.of_seq
      |> yojson_of_list (fun (i, j) ->
             `List [ H.yojson_of_t i; yojson_of_data j ])

    let stats_descr
        Hashtbl.{ num_bindings; num_buckets; max_bucket_length; _ } =
      [
        ("Bindings:", Printf.sprintf "%-8d" num_bindings, true);
        ("Buckets:", Printf.sprintf "%-8d" num_buckets, true);
        ("Max bucket:", Printf.sprintf "%-8d" max_bucket_length, true);
        (* ("Histogram:")*)
      ]
  end
end

module type Pp = sig
  type t

  val pp : Format.formatter -> t -> unit
end

module M_opt (M_lib : Map.S) (P : Pp with type t = M_lib.key) = struct
  include M_lib

  let pp ~open_b ~first ~last ~sep pp_b out m =
    let open Format in
    let pp_print_pair a b out p =
      pp_open_hbox out ();
      pp_print_string out "(";
      a out (fst p);
      pp_print_string out ",";
      pp_print_space out ();
      b out (snd p);
      pp_print_string out ")";
      pp_close_box out ()
    in
    let pp_binding out a b = pp_print_pair P.pp pp_b out (a, b) in
    open_b out ();
    first out;
    (match min_binding_opt m with
    | None -> ()
    | Some (a, b) ->
        let m' = remove a m in
        pp_binding out a b;
        iter
          (fun a b ->
            sep out;
            pp_binding out a b)
          m');
    last out;
    pp_close_box out ()
end

module S_opt (S_lib : Set.S) (P : Pp with type t = S_lib.elt) = struct
  include S_lib

  let is_singleton s =
    try
      fold (fun _ acc -> if acc > 1 then raise_notrace Exit else acc + 1) s 0
      = 1
    with Exit -> false

  let pp ~open_b ~first ~last ~sep out s =
    let open Format in
    open_b out ();
    first out;
    (match min_elt_opt s with
    | None -> ()
    | Some min ->
        let s' = remove min s in
        P.pp out min;
        iter
          (fun x ->
            sep out;
            P.pp out x)
          s');
    last out;
    pp_close_box out ()
end

module Predicate = struct
  type t = string * int [@@deriving yojson]

  let compare = compare
  let equal x y = x = y
  let hash = Hashtbl.hash
end

module M_int =
  M_opt
    (Map.Make (struct
      type t = int [@@deriving yojson]

      let compare = int_compare
    end))
    (struct
      type t = int

      let pp = Format.pp_print_int
    end)

module M_string =
  M_opt
    (Map.Make (struct
      type t = string [@@deriving yojson]

      let compare = compare
    end))
    (struct
      type t = String.t

      let pp = Format.pp_print_string
    end)

module S_string =
  S_opt
    (Set.Make (struct
      type t = string [@@deriving yojson]

      let compare = compare
    end))
    (struct
      type t = String.t

      let pp = Format.pp_print_string
    end)

module S_predicate = Set.Make (Predicate)

module H_int = Hashtbl_yojson.Make (struct
  type t = int [@@deriving yojson]

  let equal = int_equal
  let hash = Hashtbl.hash
end)

(* TODO: Does this differ from Hashtbl.Make(String) *)
module H_string = Hashtbl.Make (struct
  type t = string

  let equal (x : string) y = x = y
  let hash = Hashtbl.hash
end)

module H_predicate = Hashtbl_yojson.Make (Predicate)

let safe = function Some v -> v | None -> assert false

let pair_compare fa fb ((a0, b0) : 'a * 'b) ((a1, b1) : 'a * 'b) =
  match fa a0 a1 with 0 -> fb b0 b1 | x -> x
[@@inline]

let ints_compare (i0, p0) (i1, p1) =
  match i0 - i1 with 0 -> p0 - p1 | x -> x
[@@inline]

let string_of_ints a b = "(" ^ string_of_int a ^ "," ^ string_of_int b ^ ")"

let opt_equal eq a b =
  match (a, b) with
  | None, None -> true
  | None, _ | _, None -> false
  | Some v, Some v' -> eq v v'

let flip f x y = f y x [@@inline]
let flip2 f a b c = f a c b [@@inline]

(* "\n12\n4\n678\n" -> ["12"; "4"; "678"] "01234\n" -> ["01234"] "0123" ->
   ["0123"] *)
let parse_lines = Str.(split (regexp_string "\n"))

(* "{}" -> "" "[123]" -> "123" *)
let remove_block_delims s =
  if String.length s >= 2 then String.(sub s 1 (length s - 2))
  else invalid_arg "String \"" ^ s ^ "\" has no block delimiters"

let fold_matrix f m acc =
  Array.fold_left
    (fun (i, acc) r ->
      ( i + 1,
        Array.fold_left (fun (j, acc) x -> (j + 1, f acc i j x)) (0, acc) r
        |> snd ))
    (0, acc) m
  |> snd

let iter_matrix f m =
  Array.iteri (fun i r -> Array.iteri (fun j x -> f i j x) r) m

let is_square_matrix m =
  let x = Array.length m in
  Array.for_all (fun r -> Array.length r = x) m

let elements_matrix m = fold_matrix (fun acc _ _ x -> x :: acc) m []

(* m is assumed square *)
let size_matrix m =
  match Array.length m with 0 -> 0 | r -> r * Array.length m.(0)

(* List utilities *)

let list_equal f a b = List.length a = List.length b && List.for_all2 f a b
let list_of_pair (a, b) = [ a; b ] [@@inline]

let cartesian a b =
  List.fold_left
    (fun acc j -> List.fold_left (fun acc j' -> (j, j') :: acc) acc b)
    [] a

let list_rev_split_left l = List.fold_left (fun res (a, _) -> a :: res) [] l

(* Fold over n-1,..,0 *)
let rec fold_n acc n f =
  assert (n >= 0);
  if n >= 1 then fold_n (f (n - 1) acc) (n - 1) f else acc

(* Iterate until you get a Some value. None if you never get a Some value *)
let iter_till_some f ls =
  let rec aux_ acc xs =
    match acc with
    | Some _ as r -> r
    | None -> ( match xs with [] -> None | y :: ys -> aux_ (f y) ys)
  in
  aux_ None ls

let avg xs =
  let (n, sum) =
    Seq.fold_left (fun (l, acc) x ->
        (l + 1, x +. acc)) (0, 0.0) xs in
  sum /. (Float.of_int n)
