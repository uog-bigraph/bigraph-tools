(** Simple Tikz Output for Bigraphs. Output often needs modified for the best
    placement, but should give some good-enough bigraphs where possible

    @author Blair Archibald *)

(** Configuration options *)
type options = {
  mutable spacing : float;
  mutable link_step_size : int;
  mutable numerical_instmap : bool;
  mutable vertical_rules : bool
} [@@deriving yojson]

val read_config : Yojson.Safe.t -> unit

val big_to_tikz : Big.t -> string
(** [big_to_tikz b] compute a string expressing bigraph [b] in [tikz]
    format. *)

module type R = sig
  type t

  val rr_to_tikz : t -> string
(** [rr_to_tikz r] compute a string expressing reaction rule [r] in [tikz]
    format. *)
end

module Make (Rt : Rs.RS) : R with type t := Rt.react
