(** This module provides operation to flatten a bigraph to a directed graph
    with special nodes and constraints as defined in:

    B. Archibald, K. Burns, C. McCreesh, M. Sevegnani.
    {e
       {{:https://doi.org/10.4230/LIPIcs.CP.2021.15} Practical Bigraphs via
         Subgraph Isomorphism}, International Conference on Principles and
       Practice of Constraint Programming (CP 2021).}

    @author Blair Archibald *)

type node = {
  ident : int;
  lbl : string;
  name : string;
  in_deg : int list; (* Only if root is parent *)
  out_deg : int list (* Only if site is child *);
}
(** Flattened node representation *)

type t = { nodes : node list; adj : Sparse.t }
(** Type of flattened digraphs *)

val flatten_target : Big.t -> t
(** Convert a bigraph to flattened target representation *)

val flatten_pattern : Big.t -> t
(** Convert a bigraph to flattened pattern representation *)

val toGBS : t -> Gbs.t
