open IntSet
open Ppx_yojson_conv_lib.Yojson_conv.Primitives

type options = {
  mutable spacing : float [@default 1.0];
  mutable link_step_size : int [@default 50];
  mutable numerical_instmap : bool [@default false];
  mutable vertical_rules : bool [@default false]
} [@@deriving yojson]

let defaults = {
  spacing = 1.0;
  link_step_size = 50;
  numerical_instmap = false;
  vertical_rules = false;
}

let read_config json =
  let cfg = options_of_yojson json in
  defaults.spacing <- cfg.spacing;
  defaults.link_step_size <- cfg.link_step_size;
  defaults.numerical_instmap <- cfg.numerical_instmap;
  defaults.vertical_rules <- cfg.vertical_rules

let standalone_header =
{|
\documentclass[crop,tikz]{standalone}
\usetikzlibrary{fit, positioning, calc, shapes, shapes.geometric, arrows, arrows.meta}
\tikzset{
  big edge/.style={green, thick,},
  big edgec/.style={big edge, -{Bar[fill=green,green,width=4,length=0,sep=0]}},
  big region/.style={draw, rectangle, rounded corners=1.5, dashed, dash pattern=on 1pt off 1pt, thin, gray,},
  big site/.style={big region, fill=gray!60, text=black,},
  big react/.style={black, thick, -stealth, line width=3, shorten <=3, shorten >=3,},
  big react rev/.style={black, thick, stealth-stealth, line width=3, shorten <=3, shorten >=3,},
  big inst map/.style={thick, -stealth, blue, dashed},
  lbl/.style={font=\tiny\sf, inner sep=1,},
  lbl conc/.style={font=\tiny, inner sep=1,}
}
\usepackage{amsmath,amssymb}
\DeclareMathOperator{\react}{\mathrel{\frac{\raisebox{0.75mm}{\begin{scriptsize}\ensuremath{\hspace*{1mm}\ \hspace*{1mm}}\end{scriptsize}}}{}} \joinrel{\!\!\vartriangleright}}
\newcommand{\reactp}[1]{\operatorname{\mathrel{\frac{\raisebox{0.75mm}{\begin{scriptsize}\ensuremath{\hspace*{1mm}\ #1 \hspace*{1mm}}\end{scriptsize}}}{}} \joinrel{\!\!\vartriangleright}}}
\DeclareMathOperator{\rrul}{\mathrel{\frac{\raisebox{0.75mm}{\begin{scriptsize}\ensuremath{\hspace*{1mm}\ \hspace*{1mm}}\end{scriptsize}}}{}} \joinrel{\!\!\blacktriangleright}}
\newcommand{\rrulp}[1]{\operatorname{\mathrel{\frac{\raisebox{0.75mm}{\begin{scriptsize}\ensuremath{\hspace*{1mm}\ #1 \hspace*{1mm}}\end{scriptsize}}}{}} \joinrel{\!\!\blacktriangleright}}}
\newcommand{\rrula}[2]{\operatorname{\mathrel{\frac{\raisebox{0.75mm}{\begin{scriptsize}\ensuremath{\hspace*{1mm}\ #1 \hspace*{1mm}}\end{scriptsize}}}{\begin{scriptsize}\ensuremath{\hspace*{1mm}\ #2 \hspace*{1mm}}\end{scriptsize}}}\joinrel{\!\!\blacktriangleright}}}
|}

(* Util functions *)
let sanitise_lbls = Str.global_replace (Str.regexp "_") "\\_"

let ctrl_name c = "_BIG_" ^ (String.uncapitalize_ascii @@ Ctrl.name c)

(* Create the tree structure through lists (in order) *)
type ntype = [ `N of int | `S of int * bool | `None ]

let ntype_str = function
  | `N i -> "n" ^ string_of_int i
  | `S (i, left) -> "s" ^ string_of_int i ^ (if left then "l" else "r")
  | `None -> ""

let leaves pg isLeft =
  let isLeaf n =
    let s = Sparse.chl pg.Place.ns n |> IntSet.cardinal in
    let ns = Sparse.chl pg.Place.nn n |> IntSet.cardinal in
    s + ns = 0
  in
  let rec go (t : int) : ntype list =
    if isLeaf t then [ `N t ]
    else
      let ns = Sparse.chl pg.Place.nn t |> IntSet.elements in
      let ss =
        Sparse.chl pg.Place.ns t |> IntSet.elements
        |> List.map (fun s -> `S (s, isLeft))
      in
      let children = List.map go ns |> List.concat in
      children @ ss
  in
  (* TODO: Doesn't handle id only bigraphs *)
  let top =
    List.init pg.Place.r (fun i -> i)
    |> List.map (fun r -> Sparse.chl pg.Place.rn r |> IntSet.elements)
    |> List.concat
  in
  List.map go top |> List.concat |> List.rev

(* Main algorithm *)
let get_all_control_names big =
  let module S = Set.Make (String) in
  let names =
    Nodes.fold
      (fun _ ctrl acc -> S.add (ctrl_name ctrl) acc)
      big.Big.n S.empty
  in
  S.elements names

let gen_names b =
  let out =
    Link.outer b.Big.l |> Link.Face.elements
    |> List.map (fun (Link.Name n) -> n)
  in
  let name prev n =
    if prev != "" then
      Printf.sprintf "\n\\node[right=1 of name_%s] (name_%s) {\\tiny $%s$};"
        prev n n
    else
      Printf.sprintf
        "\n\
         \\node[] at ($(r0.north west) + (0,0.3)$) (name_%s) {\\tiny $%s$};"
        n n
  in
  let s, _ =
    List.fold_left (fun (s, prev) n -> (s ^ name prev n, n)) ("", "") out
  in
  s

let num_connections e =
  let ports = IntSet.cardinal (Link.Ports.to_IntSet e.Link.p) in
  let onames = Link.Face.cardinal e.Link.o in
  ports + onames

let gen_links b =
  let ports_dir_map = Hashtbl.create (Nodes.size b.Big.n) in
  let next_dir n =
    (* Always enter names from below *)
    if Str.string_match (Str.regexp "name_*") n 0 then -90
    else
      match Hashtbl.find_opt ports_dir_map n with
      | Some d -> Hashtbl.replace ports_dir_map n (d + defaults.link_step_size); d
      | None -> Hashtbl.add ports_dir_map n defaults.link_step_size; 0
  in
  let output_closure e =
    if Link.Ports.cardinal e.Link.p = 0 then "" else
    let n = Link.Ports.to_IntSet e.Link.p |> IntSet.elements |> List.hd in
    let dir = next_dir @@ "n" ^ (string_of_int n) in
    Printf.sprintf
      "\n\\draw[big edgec] (n%d) to[out=%d,in=-90] ($(n%d.%d) + (0.2,0.2)$);" n dir n dir
  in
  let output_direct e =
    let ps =
      Link.Ports.to_IntSet e.Link.p
      |> IntSet.elements
      |> List.map (fun n -> "n" ^ string_of_int n)
    in
    let ns =
      Link.Face.elements e.Link.o
      |> List.map (fun (Link.Name n) -> n)
      |> List.map (fun n -> "name_" ^ n)
    in
    let ns' = ps @ ns in
    let n1 = List.hd ns' in
    let n2 = List.hd (List.tl ns') in
    Printf.sprintf "\n\\draw[big edge] (%s) to[out=%d,in=%d] (%s);"
      n1 (next_dir n1) (next_dir n2) n2
  in
  let output_hyper e i =
    let ps =
      Link.Ports.to_IntSet e.Link.p
      |> IntSet.elements
      |> List.map (fun n -> "n" ^ string_of_int n)
    in
    let ns =
      Link.Face.elements e.Link.o
      |> List.map (fun (Link.Name n) -> n)
      |> List.map (fun n -> "name_" ^ n)
    in
    let p_h = ps |> List.hd in
    (* Will fail for name only links *)
    let h_def =
      Printf.sprintf "\n\\coordinate (h%d) at ($(%s) + (0.3,0.3)$);" i p_h
    in
    h_def
    ^ List.fold_left
        (fun acc n ->
          acc
          ^ Printf.sprintf "\n\\draw[big edge] (%s) to[out=%d,in=-90] (h%d);"
              n (next_dir n) i)
        "" ps
    ^ List.fold_left
        (fun acc n ->
          acc
          ^ Printf.sprintf "\n\\draw[big edge] (%s) to[out=-90,in=90] (h%d);"
              n i)
        "" ns
  in
  let f edg i =
    match num_connections edg with
    | 1 -> (output_closure edg, i)
    | 2 -> (output_direct edg, i)
    | _ -> (output_hyper edg i, i + 1)
  in
  Link.Lg.fold
    (fun edg (s, i) ->
      let s', i' = f edg i in
      (s ^ s', i'))
    b.Big.l ("", 0)
  |> fst

let gen_regions b =
  let rs = List.init b.Big.p.r (fun i -> i) in
  let getFitNodes r =
    Sparse.fold
      (fun i j acc -> if i = r then j :: acc else acc)
      b.Big.p.rn []
  in
  let nodeDef r fit =
    let fit_str =
      match fit with
      | [] -> ""
      | fs -> List.map
        (fun i -> "(n" ^ string_of_int i ^ ")(n" ^ string_of_int i ^ "l)")
        fs |> String.concat "" |> fun s -> "fit=" ^ s
    in
    Printf.sprintf "\n\\node[big region, %s] (r%d) {};" fit_str r
  in
  List.fold_left
    (fun acc r ->
      let fit = getFitNodes r in
      acc ^ nodeDef r fit)
    "" rs

let gen_fit_nodes levels b left =
  let getFitNodes l =
    let nfit =
      Sparse.fold
        (fun i j acc -> if i = l then j :: acc else acc)
        b.Big.p.nn []
      |> List.map (fun i -> Printf.sprintf "(n%d)(n%dl)" i i)
      |> String.concat ""
    in
    let sfit =
      Sparse.fold
        (fun i j acc -> if i = l then j :: acc else acc)
        b.Big.p.ns []
      |> List.map (fun i -> Printf.sprintf "(s%d%s)" i (if left then "l" else "r"))
      |> String.concat ""
    in
    nfit ^ sfit
  in
  let nodeDef n =
    let c = Base.safe @@ Nodes.get_ctrl n b.Big.n in
    let s = getFitNodes n in
    if s = "" then ""
    else
      Printf.sprintf
        "\n\
         \\node[%s, fit=%s, label={[inner sep=0.5, \
         name=n%dl]north:{\\sf\\tiny %s}}] (n%d) {};"
        (ctrl_name c) s n
        (sanitise_lbls @@ Ctrl.long_name c)
        n
  in
  let nodeDefns ls = List.fold_left (fun acc l -> acc ^ nodeDef l) "" ls in
  List.fold_left (fun acc ls -> acc ^ nodeDefns ls) "" levels

let gen_leaf_nodes ?instmap left b =
  let levels = Sparse.levels b.Big.p.nn |> List.rev in
  let leafs = leaves b.Big.p left |> List.rev in
  let outputNode prev c n =
    let outN loc =
      Printf.sprintf
        "\n\
         \\node[%s, %s label={[inner sep=0.5, name=%sl]north:{\\sf\\tiny \
         %s}}] (%s) {};"
        (ctrl_name c) loc (ntype_str n)
        (sanitise_lbls @@ Ctrl.long_name c)
        (ntype_str n)
    in
    if prev != `None then
      outN (Printf.sprintf "right=%.2f of %s," defaults.spacing (ntype_str prev))
    else outN ""
  in
  let outputSite prev s =
    let outS loc =
      let outPlain = Printf.sprintf "\n\\node[big site, %s] (%s) {};" loc (ntype_str s)
      in if not defaults.numerical_instmap then outPlain else
        match instmap with
          | None -> outPlain
          | Some imap ->
            let num =
              let n = match s with | `S (n, _) -> n | _ -> -1 in
              if left then n else Base.safe @@ Fun.apply imap n
            in
            Printf.sprintf "\n\\node[big site, %s] (%s) {%d};" loc (ntype_str s) num
    in
    if prev != `None then
      outS (Printf.sprintf "right=%.2f of %s," defaults.spacing (ntype_str prev))
    else outS ""
  in
  let leafDefn n prev =
    match n with
    | `N n as n' ->
        let c = Base.safe @@ Nodes.get_ctrl n b.Big.n in
        outputNode prev c n'
    | `S _ as s' -> outputSite prev s'
    | `None -> ""
  in
  let ls, _ =
    List.fold_right
      (fun n (s, prev) -> (s ^ leafDefn n prev, n))
      leafs ("", `None)
  in
  ls ^ gen_fit_nodes (List.map IntSet.elements levels) b left

let gen_tikz ?(left=true) ?instmap b =
  gen_leaf_nodes ?instmap:instmap left b ^ gen_regions b ^ gen_names b ^ gen_links b

(* Generat tikz string *)
let tikz_state b =
  let ctrl_str =
    List.fold_right
      (fun n acc -> acc ^ ",\n" ^ n ^ "/.append style = {draw, rounded corners=0.8}")
      (get_all_control_names b) ""
  in
  Printf.sprintf
    {|
\begin{tikzpicture}[
  %s
  ]
    %s

\end{tikzpicture}
  |}
    ctrl_str (gen_tikz b)

let big_to_tikz (b : Big.t) =
  Printf.sprintf
{|
%s
\begin{document}
%s
\end{document}
|}
  standalone_header (tikz_state b)

module type R = sig
  type t
  val rr_to_tikz : t -> string
end

module Make (Rt : Rs.RS) : R with type t := Rt.react = struct
  type t = Rt.react

  let rr_to_tikz (r : t) =
    let allCtrls =
      get_all_control_names (Rt.lhs r)
      @ get_all_control_names (Rt.rhs r)
      @ List.concat_map (fun c -> get_all_control_names c.AppCond.pred) (Rt.conds r)
      |> List.sort_uniq String.compare
    in
    let ctrl_str =
      List.fold_right
        (fun n acc -> acc ^ ",\n" ^ n ^ "/.append style = {draw}")
        allCtrls ""
    in
    let imap = match Rt.map r with
      | None -> Fun.of_list (List.init ((Rt.lhs r).Big.p.s) (fun i -> (i, i)))
      | Some imap -> imap
    in
    let draw_inst_map =
      let draw_arrows im =
        Fun.fold (fun r l acc ->
            let s = Printf.sprintf "\\draw[big inst map] (s%dr) to[looseness=0.8, out=-90, in=-90] (s%dl);" r l in
            s :: acc) im []
        |> String.concat "\n"
      in
      if not defaults.numerical_instmap then
        draw_arrows imap
      else ""
    in
    let draw_conds =
      let draw_cond x_shift c =
        let open AppCond in
    Printf.sprintf
        {|
  \begin{scope}[shift={($(lhs.south) + (%2f,-1)$)}, scale=0.66, transform shape]
  \node[] (start) {$\langle %s$,};

  \begin{scope}[shift={(1,0)}]
  %s
  \end{scope}

  \node[right=1.2 of start, inner sep=0.5] (end) {, $%s \rangle$};
\end{scope}
        |}
        x_shift
        (if c.neg then "-" else "+")
        (gen_tikz c.pred)
        (match c.where with | Ctx -> "\\uparrow" | Param -> "\\downarrow")
      in
      if List.length (Rt.conds r) == 0 then
        ""
      else
        let cond_str = List.fold_left (fun (acc, shift) c ->
            let cstr = draw_cond shift c in
            (cstr :: acc, shift +. 2.)
          ) ([], 0.5) (Rt.conds r)
        |> fst |> List.rev |> String.concat ""
        in
        {|
          \begin{scope}[shift={($(lhs.south) + (0,-1)$)}, scale=0.66, transform shape]
            \node[] (if) {\textbf{if}};"
          \end{scope}
        |} ^ cond_str
    in
    let get_rhs_pos =
      if not defaults.vertical_rules then
        "shift={($(lhs.east) + (1,0)$)}"
      else
        "shift={($(lhs.south) + (0,3)$)}"
    in
    let get_arrow_direction =
      if not defaults.vertical_rules then
        "($(lhs.east)!0.5!(rhs.west)$) {$\\rrul$};"
      else
        "($(lhs.south)!0.5!($(rhs.north) + (0,1)$)$) {\\rotatebox{270}{$\\rrul$}};"
    in
    Printf.sprintf
      {|
  %s
  \begin{document}
  \begin{tikzpicture}[
    %s
    ]
    \begin{scope}[local bounding box=lhs, shift={(0,0)}]
      %s

    \end{scope}
    \begin{scope}[local bounding box=rhs, %s]
      %s

    \end{scope}

  %s

    \node[xshift=0] at %s;

  %s
  \end{tikzpicture}
  \end{document}
    |}
      standalone_header
      ctrl_str
      (gen_tikz ~left:true ?instmap:(Some imap) (Rt.lhs r))
      get_rhs_pos
      (gen_tikz ~left:false ?instmap:(Some imap) (Rt.rhs r))
      draw_inst_map
      get_arrow_direction
      draw_conds
end
