open Ppx_yojson_conv_lib.Yojson_conv.Primitives

type elt = int
type t = Z.t

(* Internal bit ops *)
let set_bit i n = Z.logor (Z.shift_left Z.one i) n
let unset_bit i n = Z.logand (Z.lognot (Z.shift_left Z.one i)) n

(* Set API *)
let empty = Z.zero
let add = set_bit

(* let is_empty bs = Z.equal bs Z.zero *)
let is_empty bs = Z.popcount bs = 0
let mem n bs = Z.testbit bs n
let remove = unset_bit
let union n m = Z.logor n m
let inter n m = Z.logand n m
let diff x y = Z.logxor x y |> Z.logand x

let iter f bs =
  let rec go n =
    if is_empty n then ()
    else
      let v = Z.trailing_zeros n in
      f v;
      go (unset_bit v n)
  in
  go bs

let elements bs =
  let elems = ref [] in
  iter (fun i -> elems := i :: !elems) bs;
  !elems

let of_list = List.fold_left (fun bs x -> set_bit x bs) empty
let equal = Z.equal
let compare = Z.compare
let singleton x = set_bit x empty
let disjoint xs ys = inter xs ys |> is_empty
let subset xs ys = inter xs ys |> equal xs

let fold f bs acc =
  let rec go a n =
    if is_empty n then a
    else
      let v = Z.trailing_zeros n in
      go (f v a) (unset_bit v n)
  in
  go acc bs

let map f xs = fold (fun i bs -> add (f i) bs) xs empty
let cardinal = Z.popcount

let for_all p bs =
  try
    iter (fun i -> if not (p i) then raise_notrace Exit else ()) bs;
    true
  with Exit -> false

let exists p bs =
  try
    iter (fun i -> if p i then raise_notrace Exit else ()) bs;
    false
  with Exit -> true

let filter p bs = fold (fun i acc -> if p i then add i acc else acc) bs empty

let filter_map p bs =
  fold
    (fun i acc -> match p i with Some v -> add v acc | None -> acc)
    bs empty

let partition p bs =
  fold
    (fun i (t, f) -> if p i then (add i t, f) else (t, add i f))
    bs (empty, empty)

let min_elt bs =
  let res = ref (-1) in
  try
    iter
      (fun i ->
        res := i;
        raise Exit)
      bs;
    raise Not_found
  with Exit -> !res

let min_elt_opt bs =
  let res = ref (-1) in
  try
    iter
      (fun i ->
        res := i;
        raise Exit)
      bs;
    None
  with Exit -> Some !res

let max_elt bs =
  let r = fold (fun i _ -> i) bs (-1) in
  if r < 0 then raise Not_found else r

let max_elt_opt bs =
  let r = fold (fun i _ -> i) bs (-1) in
  if r < 0 then None else Some r

let choose = min_elt
let choose_opt = min_elt_opt

let split x bs =
  fold
    (fun i (l, p, g) ->
      match Int.compare i x with
      | 0 -> (l, true, g)
      | -1 -> (add i l, p, g)
      | 1 -> (l, p, add i g)
      | _ -> assert false)
    bs (empty, false, empty)

let find i bs = if mem i bs then i else raise Not_found

let find_opt i bs =
  let r = mem i bs in
  if r then Some i else None

let find_first f bs =
  let res = ref (-1) in
  try
    iter
      (fun i ->
        if f i then res := i;
        raise Exit)
      bs;
    raise Not_found
  with Exit -> !res

let find_first_opt f bs =
  let res = ref (-1) in
  try
    iter
      (fun i ->
        if f i then res := i;
        raise Exit)
      bs;
    None
  with Exit -> Some !res

let find_last f bs =
  let res = ref (-1) in
  try
    for i = Z.numbits bs to 0 do
      if Z.testbit bs i then
        if f i then (
          res := i;
          raise Exit)
        else ()
      else ()
    done;
    raise Not_found
  with Exit -> !res

let find_last_opt f bs =
  try
    let res = find_last f bs in
    Some res
  with Not_found -> None

(* Non Set functions *)
let to_string bs =
  let b = Buffer.create 100 in
  for i = 0 to Z.numbits bs do
    if Z.testbit bs i then Buffer.add_char b '1' else Buffer.add_char b '0'
  done;
  Buffer.contents b

let shift i bs =
  if i < 0 then Z.shift_right_trunc bs (abs i) else Z.shift_left bs i

let ordinal i = Z.sub (Z.pow (Z.of_int 2) i) Z.one

let to_list = elements

(* Iterators *)
let to_seq_from x bs =
  fold (fun i s -> if i >= x then Seq.cons i s else s) bs Seq.empty

let to_seq bs = to_seq_from 0 bs
let to_rev_seq bs = elements bs |> List.rev |> List.to_seq
let add_seq i m = Seq.fold_left (fun s x -> add x s) m i
let of_seq i = add_seq i empty

let t_of_yojson (json:Yojson.Safe.t) =
  list_of_yojson int_of_yojson json |> of_list

let yojson_of_t (t:t) =
  to_seq t |> List.of_seq |> yojson_of_list yojson_of_int
