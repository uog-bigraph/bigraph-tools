open Bigraph
module S = Solver.Make_SAT (Solver.MS)
module BRS = Brs.Make (S)

let () =
  Printexc.record_backtrace true;
  let bgs =
    Io.parse_all Sys.argv.(1) (fun x -> Filename.check_suffix x ".big")
    |> List.map (fun (n, ls) -> (n, Big.of_string ls))
  in
  (* Check the rountrip worked *)
  List.iter
    (fun (n, bs) ->
      let r = Big.yojson_of_t bs |> Big.t_of_yojson in
      match S.equal bs r with
      | true -> ()
      | false ->
          print_endline @@ "FAILURE: Bigraphs and JSON do no match: " ^ n)
    bgs;
  (* Check reaction rules parse. Very basic for now *)
  let s0 =
    Io.parse (Sys.argv.(2) ^ "/h0.json")
    |> String.concat "\n" |> Yojson.Safe.from_string |> Big.t_of_yojson
  in
  let r =
    Io.parse (Sys.argv.(2) ^ "/send_doc.json")
    |> String.concat "\n" |> Yojson.Safe.from_string |> BRS.react_of_yojson
  in
  (match BRS.apply s0 [ r ] with
  | Some _ -> ()
  | None -> print_endline "REACT FAILURE");
  print_endline "DONE"
