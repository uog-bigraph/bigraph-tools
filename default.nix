{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/24.05.tar.gz") {
  overlays = [
    (super: self: {
      # opaline definition doesn't work with static by default (not sure why!)
      # so we redefine
      opaline = self.opaline.overrideAttrs (o: rec{
        nativeBuildInputs = [
          self.ocaml
          self.ocamlPackages.findlib
          self.ocamlPackages.ocamlbuild
        ];
      });
    })
  ];
}}:

let
  ocaml_version = "ocamlPackages_5_0";

  bigrapher_builder = {static ? false} :
    let
      pkgs' = if static then pkgs.pkgsStatic else pkgs;
      ocamlPkgs = pkgs'.ocaml-ng.${ocaml_version};
    in
      pkgs'.stdenv.mkDerivation rec {
        name = "bigrapher-${version}";
        version = "2.0.0";

        src = pkgs.lib.cleanSource ./.;

        buildInputs = [
          pkgs'.zlib
          pkgs'.boost
          pkgs'.bzip2
          ocamlPkgs.cmdliner
          ocamlPkgs.mtime
          ocamlPkgs.progress
          ocamlPkgs.zarith
          ocamlPkgs.dune-configurator
          ocamlPkgs.yojson
          ocamlPkgs.ppx_yojson_conv
        ];

        nativeBuildInputs = [
          ocamlPkgs.dune_3
          ocamlPkgs.ocaml
          ocamlPkgs.findlib
          ocamlPkgs.menhir
          pkgs'.cmake
        ];

        buildPhase = ''
        dune build @install --profile=release
      '';

        installPhase = ''
        dune install --profile=release --prefix=$out
      '';
      };
in
rec {
  bigrapher = bigrapher_builder { static = false; };

  shell = pkgs.mkShell {
      buildInputs = [
          (bigrapher_builder {}).buildInputs
          (bigrapher_builder {}).nativeBuildInputs
          pkgs.ocamlformat
          pkgs.git # Needed for CI
      ];
  };

  bigrapher_docker =
      pkgs.dockerTools.buildImage {
          name = "bigrapher";
          tag = "latest";
          copyToRoot = bigrapher_static;
          config = {
              CMD = [ "bigrapher" ];
              WorkingDir = "/data";
          };
      };

  bigrapher_static = (bigrapher_builder { static = true; }).overrideAttrs (o: rec {
    name = "bigrapher-${o.version}-static";

    patches = [ ./nix/minisat_static.patch
                ./nix/minisat_static_dune.patch
                ./nix/minicard_static_build.patch
                ./nix/minicard_static.patch
                ./nix/gbs_static_build.patch
              ];
  });
}
