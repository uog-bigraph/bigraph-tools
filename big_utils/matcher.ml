open Bigraph
open Bigraph.Solver
open Printf

let rec read_lines out file =
  try read_lines (input_line file :: out) file
  with End_of_file ->
    close_in file;
    List.rev out

let parse path =
  open_in path
  |> read_lines []
  |> String.concat "\n"
  |> Yojson.Safe.from_string

type solver = SAT | PB | GBS


let print_res (res : Bigraph.Solver.occ list) =
  let out =
    List.map
      (fun (sol : Bigraph.Solver.occ) ->
        sprintf "mapping = %s" (Iso.to_string sol.nodes)
        (* (Iso.to_string j) *))
      res
  in
  match out with [] -> "{ }" | _ -> "{\n" ^ String.concat "\n" out ^ "\n}"

let print_sol res t =
  let str =
    [
      "solution_count = " ^ string_of_int (List.length res);
      (* Using to_uint64_ns instead of the float version since that's only in mtime v2 and above *)
      "search_time = " ^ Int64.to_string (
          Int64.div
          (Mtime.Span.to_uint64_ns t)
          (Int64.of_int 1000000)
          ) ^ " ms";
      print_res res;
    ]
  in
  print_endline (String.concat "\n" str)

module SAT_SOL = Solver.Make_SAT (Solver.MS)
module PB_SOL = Solver.Make_SAT (Solver.MC)

module BRS = Brs.Make (SAT_SOL)

let solver = ref SAT
let isreact = ref false
let pattern = ref ""
let target = ref ""

let set_solver s =
  match s with
  | "gbs" | "GBS" | "sip" | "SIP" -> solver := GBS
  | "pb" | "PB" -> solver := PB
  | "sat" | "SAT" | _ -> solver := SAT (* Defaults to SAT *)

let () =
  let options =
    [
      ( "-solver", Arg.String set_solver, "Set the solver type" );
      ( "-rr", Arg.Set isreact, "Read from reaction (Json) file" );
      ( "-pat", Arg.Set_string pattern, "Pattern (Json) file" );
      ( "-tar", Arg.Set_string target, "Target (Json) file" )
    ] in
  Arg.parse options (fun _ -> ()) "Usage: matcher <pattern> <target>";
  (* Solver doesn't matter here, we just want the LHS *)
  let p = if !isreact then
      BRS.lhs @@ BRS.react_of_yojson (parse !pattern)
    else
        Big.t_of_yojson (parse !pattern)
  in
  let t = Big.t_of_yojson (parse !target) in
  let s = Mtime_clock.now () in
  let (module S) =
    match !solver with
    | SAT -> (module SAT_SOL : Solver.M)
    | PB -> (module PB_SOL : Solver.M)
    | GBS -> (module Solver.GBS : Solver.M)
  in
  let res = S.occurrences ~target:t ~pattern:p () in
  let e = Mtime_clock.now () in
  print_sol res (Mtime.span s e)
