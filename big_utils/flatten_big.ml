open Bigraph
(* let pp_occ out { nodes; edges; hyper_edges } = *)
(*   let open Format in *)
(*   fprintf out "@[{@[<v>%a,@;%a,@;%a@]}@]" Iso.pp nodes Iso.pp edges Fun.pp *)
(*     hyper_edges *)

(* Sets of occurrences *)
module O = struct
  include Base.S_opt
      (Set.Make (struct
         type t = Solver.occ

         open Solver

         let compare { nodes = n0; edges = e0; hyper_edges = h0 }
             { nodes = n1; edges = e1; hyper_edges = h1 } =
           Base.(
             pair_compare Iso.compare
               (pair_compare Iso.compare Fun.compare)
               (n0, (e0, h0))
               (n1, (e1, h1)))
       end))
      (struct
        type t = Solver.occ

        let pp = Solver.pp_occ
      end)

  exception Result of Solver.occ

  (* Find the first element in s satisfying p avoiding a full scan of s *)
  let scan_first p s =
    try
      fold
        (fun o res -> if p o then raise_notrace (Result o) else res)
        s None
    with Result o -> Some o

  (* Filter isomorphic occurrences *)
  let rec filter_iso_occs p_autos (res : t) (occs : t) =
    match min_elt occs with
    | None -> res
    | Some min_occ ->
      let apply_auto iso auto =
        Iso.(
          fold
            (fun i j iso' ->
               match apply auto i with
               | None -> iso'
               | Some i' -> add i' j iso')
            iso empty)
      in
      (* Occurrences isomorphic to min_occ *)
      List.rev_map
        (fun (auto_n, auto_e) ->
           ( apply_auto min_occ.nodes auto_n,
             apply_auto min_occ.edges auto_e ))
        p_autos
      (* Remove from occs all the isomorphic occurrences *)
      |> List.fold_left
        (fun res (iso_n, iso_e) ->
           let eq_occ =
             scan_first
               (fun o ->
                  Iso.equal o.nodes iso_n && Iso.equal o.edges iso_e)
               res
             |> Base.safe
           in
           remove eq_occ res)
        occs
      |> remove min_occ
      |> filter_iso_occs p_autos (add min_occ res)
end

(* Arg handling *)
(* Read all lines from a channel and return a string *)
let read_ch ch =
  let rec read_lines out ch =
    try read_lines (input_line ch :: out) ch
    with End_of_file ->
      close_in ch;
      List.rev out
  in
  read_lines [] ch |> String.concat "\n"

let in_files = ref []
let gather_files f = in_files := !in_files @ [f]

let pattern = ref false

let options =
  [
    ( "-pat",
      Arg.Set pattern,
      "Compute pattern flattening instead of target" )
  ]

(* let to_dot (t : Bigraph.Flatten.node list) = *)
(*   let str_degs n = *)
(*     Printf.sprintf "{%d, %d}" (List.length n.in_deg) (List.length n.out_deg) *)
(*   in *)
(*   let to_digraph_nodes = *)
(*     List.fold_left (fun acc n -> *)
(*         (Printf.sprintf "%d [label=\"%s %s %s\"]\n" n.ident n.lbl (str_degs n) n.name) ^ acc) *)
(*       "" t.nodes; *)
(*   in *)
(*   let to_digraph_edges = *)
(*     Sparse.fold (fun i j acc -> (Printf.sprintf "%d -> %d\n" i j) ^ acc) t.adj "" *)
(*   in *)
(*   "digraph {\n" ^ to_digraph_nodes ^ to_digraph_edges ^ "}" *)

module PB_SOL = Solver.Make_SAT (Solver.BasicInstances.MC)
module SIP_SOL = Solver.BasicInstances.MSIP

let flat_p p = Flatten.flatten_pattern p |> Flatten.toGBS
let flat_t t = Flatten.flatten_target t |> Flatten.toGBS

let solver_sol_to_occ (nodes, edges, hyp) : Solver.occ =
  { nodes = Iso.of_list nodes;
    edges = Iso.of_list edges;
    hyper_edges = Fun.of_list hyp}

let solver_sols_to_occs sols = List.rev_map solver_sol_to_occ sols

let print_occs s occs =
  let open Format in
  fprintf std_formatter "%s (num occs: %d)\n" s (List.length occs);
  List.iter (fun o -> Solver.pp_occ std_formatter o; fprintf std_formatter "\n---\n") occs;
  fprintf std_formatter "\n"

let print_autos s autos =
  let open Format in
  fprintf std_formatter "%s (num autos: %d)\n" s (List.length autos);
  List.iter (fun (n,e) ->
     fprintf std_formatter "@[{@[<v>%a,@;%a@]}@]\n" Iso.pp n Iso.pp e) autos;
  fprintf std_formatter "\n"

let sol_equals (a : Solver.occ) (b : Solver.occ) =
  Iso.equal a.nodes b.nodes
  && Iso.equal a.edges b.edges
  && Fun.equal a.hyper_edges b.hyper_edges

(* Assumes solutions appear in the same order, this might be an issue *)
let sols_equals a b =
  if List.length a != List.length b then
    ( Printf.printf "Number of solutions doesn't match (%d, %d)\n" (List.length a) (List.length b); false )
  else
  (* Silly O(N^2) for now *)
  List.map (fun x ->
    List.exists (fun y -> sol_equals x y) b)
  a
  |> List.for_all (fun x -> x == true)

let print_mismatch a b =
  List.map (fun x ->
      List.exists (fun y -> sol_equals x y) b)
    a
  |>
    fun bs ->
    List.iter2 (fun x y -> if not x then print_occs "No Match" [y] else ()) bs a

(* Solver sometimes gives duplicates, so lets remove these here. I think the iso
   check usually would do this anyway *)
let filter_uniq_sols xs =
  List.fold_left (fun acc e -> if List.exists (sol_equals e) acc then acc else e :: acc) [] xs

(* Main *)
let () =
  Arg.parse options gather_files
    "Usage: flatten_big <pattern> <target>";
  if List.length !in_files != 2 then
    failwith "You must provide 2 files <pat> <tar>";
  let pat = Big.of_string (read_ch (open_in (List.nth !in_files 0))) in
  let tar = Big.of_string (read_ch (open_in (List.nth !in_files 1))) in

  (* let str_chk = Gbs.match_all (Big.to_string pat) (Big.to_string tar) |> solver_sols_to_occs in *)

  (* For now we check occurrences_raw agrees with GBS solver for all instances (and that's it) *)
  let pboccs = PB_SOL.occurrences_raw ~target:tar ~pattern:pat in
  (* let sipoccs = Gbs.match_all_flat (flat_p pat) (flat_t tar) |> solver_sols_to_occs |> filter_uniq_sols in *)
  let sipoccs = SIP_SOL.occurrences_raw ~target:tar ~pattern:pat in

  (* let autos = PB_SOL.auto pat in *)
  (* print_autos "Autos" autos; *)

  (* let pb_occs_filtered = pboccs |> O.of_list |> O.filter_iso_occs autos O.empty |> O.elements in *)
  (* let sip_occs_filtered = sipoccs |> O.of_list |> O.filter_iso_occs autos O.empty |> O.elements in *)
  let pb_occs_filtered = PB_SOL.occurrences ~target:tar ~pattern:pat () in
  let sip_occs_filtered = SIP_SOL.occurrences ~target:tar ~pattern:pat () in


  Format.fprintf Format.std_formatter "PB\n";
  List.iter (Solver.pp_occ Format.std_formatter) pb_occs_filtered;
  Format.fprintf Format.std_formatter "SIP\n";
  List.iter (Solver.pp_occ Format.std_formatter) sip_occs_filtered;
  Format.fprintf Format.std_formatter "\n";


  if not (sols_equals pb_occs_filtered sip_occs_filtered) then (
    Printf.printf "MISMATCH BETWEEN PB AND SIP (flat)\n";
    Format.fprintf Format.std_formatter "MISMATCH PB->SIP\n";
    print_mismatch pboccs sipoccs;
    Format.fprintf Format.std_formatter "MISMATCH SIP->PB\n";
    print_mismatch sipoccs pboccs
  )
  else
    Printf.printf "PB and SIP Agree; PB Occs %d; SIP Occs %d\n"
      (List.length pb_occs_filtered)
      (List.length sip_occs_filtered);

  (* Equality check *)
  let pb_equal = PB_SOL.equal pat tar in
  let sip_equal = SIP_SOL.equal pat tar in
  if not (pb_equal == sip_equal) then
    Printf.printf "EQUALITY PB and SIP Disagree; PB:%s; SIP:%s\n" (if pb_equal then "T" else "F") (if sip_equal then "T" else "F")
  else
    Printf.printf "EQUALITY PB and SIP Agree\n"
