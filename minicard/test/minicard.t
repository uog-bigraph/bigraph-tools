Test the MiniCARD bindings.
  $ ./test.exe *.formula
  Solving 1.formula...
  sat
    a=false
    b=false
  Solving 2.formula...
  sat
    v_2_1=false
    v_2_3=true
    v_2_0=false
    v_1_2=true
    v_1_3=false
    v_1_1=true
    v_1_0=true
    v_2_2=false
  Solving 3.formula...
  unsat
  Solving 4.formula...
  unsat
  Solving 5.formula...
  sat
    a=false
    b=true
    c=false
