module C = Configurator.V1

(* Assuming prefix/bin/executable *)
let root path =
  Filename.dirname path
  |> Filename.dirname

let () =
  C.main ~name:"foo" (fun c ->
      let default : C.Pkg_config.package_conf =
        {
          libs = [ "-Lminicard/src"; "-lminicard_c" ];
          cflags =
            [
              "-pedantic";
              "-fpermissive";
              "-fPIC";
              "-O3";
              "-std=c++11";
              "-Iminicard_c";
            ];
        }
     and prefix =
        (* Homebrew *)
        match C.which c "brew" with
        | Some path -> root path
        | None ->  (* pkgsrc *)
          match C.which c "pkg_add" with
          | Some path -> root path
          | None -> "/usr/local" (* Homebrew on Intel *) in
      let conf : C.Pkg_config.package_conf =
        match C.ocaml_config_var c "system" with
        | Some "freebsd" ->
            {
              libs = ("-lc++" :: default.libs) @ [ "-L/usr/local/lib" ];
              cflags = "-I/usr/local/include" :: default.cflags;
            }
        | Some "netbsd" | Some "openbsd" ->
            {
              libs = ("-lstdc++" :: default.libs) @ [ "-L/usr/local/lib" ];
              cflags = "-I/usr/local/include" :: default.cflags;
            }
        | Some "macosx" ->
          {
            libs = ("-lc++" :: default.libs) @ [ "-L" ^ prefix ^ "/lib" ];
            cflags = ("-I" ^ prefix ^ "/include") :: default.cflags;
            }
        | Some _ | None ->
            { libs = "-lstdc++" :: default.libs; cflags = default.cflags }
      in
      C.Flags.write_sexp "libs.sexp" conf.libs;
      C.Flags.write_sexp "flags.sexp" conf.cflags)
