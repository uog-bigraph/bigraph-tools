Test BigraphER format.
  $ for model in ../examples/rts_cts.big \
  >              ../examples/actors.big \
  >              ../examples/wireless.big \
  >              ../examples/savannah.big \
  >              ../examples/rrim.big \
  >              ../examples/rrim2.big \
  >              ../examples/closures.big \
  >              ../examples/merge.big \
  >              ../examples/prob.big \
  >              ../examples/link_inst_map.big \
  >              ../examples/string_params.big \
  >              ../examples/iter-merge.big \
  >              ../examples/norules.big \
  >              ../examples/unique_entities.big \
  >              ../examples/conditional_turn_taking.big \
  >              ../examples/multiset-matching.big \
  >              ../examples/virus-simpl.big \
  >              ../examples/virus-multifirewall.big \
  >              ../examples/abrs-mobilesink.big \
  >              ../examples/prob_wsn.big \
  >              ../examples/mdp_wsn.big \
  >              ../examples/stoch_occ.big \
  >              ../examples/river-cross.big \
  >              ../examples/ccs.big \
  >              ../examples/ccs_rec.big \
  >              ../examples/plato-graphical.big \
  >              ../examples/plato-graphical-loc.big \
  > ; do
  >   echo "${model}";
  >   bigrapher format -q -N "$model" | bigrapher full --no-colors --debug -M 140;
  >   bigrapher format -q -N --no_comments "$model" | bigrapher full --no-colors --debug -M 140;
  >   bigrapher format -q -N -o out1.big "$model";
  >   bigrapher format -q -N -o out2.big out1.big;
  >   diff -c out1.big out2.big;
  > done
  ../examples/rts_cts.big
  Type:           Stochastic BRS
  Bindings:       66      
  Rules:          487     
  Max # states:   140     
  States:         128     
  Transitions:    154     
  Occurrences:    430     
  Type:           Stochastic BRS
  Bindings:       66      
  Rules:          487     
  Max # states:   140     
  States:         128     
  Transitions:    154     
  Occurrences:    430     
  ../examples/actors.big
  Type:           BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         10      
  Transitions:    12      
  Occurrences:    12      
  Type:           BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         10      
  Transitions:    12      
  Occurrences:    12      
  ../examples/wireless.big
  Type:           Stochastic BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  Type:           Stochastic BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/savannah.big
  Type:           BRS
  Bindings:       34      
  Rules:          11      
  Max # states:   140     
  States:         107     
  Transitions:    252     
  Occurrences:    252     
  Type:           BRS
  Bindings:       34      
  Rules:          11      
  Max # states:   140     
  States:         107     
  Transitions:    252     
  Occurrences:    252     
  ../examples/rrim.big
  Type:           BRS
  Bindings:       9       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  Type:           BRS
  Bindings:       9       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/rrim2.big
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/closures.big
  Type:           BRS
  Bindings:       5       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  Type:           BRS
  Bindings:       5       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  ../examples/merge.big
  Type:           BRS
  Bindings:       16      
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  Type:           BRS
  Bindings:       16      
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/prob.big
  Type:           Probabilistic BRS
  Bindings:       9       
  Rules:          3       
  Max # states:   140     
  States:         14      
  Transitions:    29      
  Occurrences:    64      
  Type:           Probabilistic BRS
  Bindings:       9       
  Rules:          3       
  Max # states:   140     
  States:         14      
  Transitions:    29      
  Occurrences:    64      
  ../examples/link_inst_map.big
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         142     
  Transitions:    141     
  Occurrences:    141     
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         142     
  Transitions:    141     
  Occurrences:    141     
  ../examples/string_params.big
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/iter-merge.big
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    1       
  Occurrences:    21      
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    1       
  Occurrences:    21      
  ../examples/norules.big
  Type:           BRS
  Bindings:       2       
  Rules:          0       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  Type:           BRS
  Bindings:       2       
  Rules:          0       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/unique_entities.big
  Type:           BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  Type:           BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/conditional_turn_taking.big
  Type:           BRS
  Bindings:       12      
  Rules:          4       
  Max # states:   140     
  States:         12      
  Transitions:    16      
  Occurrences:    20      
  Type:           BRS
  Bindings:       12      
  Rules:          4       
  Max # states:   140     
  States:         12      
  Transitions:    16      
  Occurrences:    20      
  ../examples/multiset-matching.big
  Type:           BRS
  Bindings:       8       
  Rules:          3       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  Type:           BRS
  Bindings:       8       
  Rules:          3       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  ../examples/virus-simpl.big
  Type:           Probabilistic BRS
  Bindings:       12      
  Rules:          3       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  Type:           Probabilistic BRS
  Bindings:       12      
  Rules:          3       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  ../examples/virus-multifirewall.big
  Type:           Probabilistic BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  Type:           Probabilistic BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  ../examples/abrs-mobilesink.big
  Type:           Action (Nondeterministic) BRS
  Bindings:       52      
  Rules:          85      
  Max # states:   140     
  States:         142     
  Transitions:    222     
  Occurrences:    444     
  Type:           Action (Nondeterministic) BRS
  Bindings:       52      
  Rules:          85      
  Max # states:   140     
  States:         142     
  Transitions:    222     
  Occurrences:    444     
  ../examples/prob_wsn.big
  Type:           Probabilistic BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    6       
  Occurrences:    12      
  Type:           Probabilistic BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    6       
  Occurrences:    12      
  ../examples/mdp_wsn.big
  Type:           Action (Nondeterministic) BRS
  Bindings:       13      
  Rules:          4       
  Max # states:   140     
  States:         3       
  Transitions:    4       
  Occurrences:    4       
  Type:           Action (Nondeterministic) BRS
  Bindings:       13      
  Rules:          4       
  Max # states:   140     
  States:         3       
  Transitions:    4       
  Occurrences:    4       
  ../examples/stoch_occ.big
  Type:           Stochastic BRS
  Bindings:       7       
  Rules:          2       
  Max # states:   140     
  States:         5       
  Transitions:    4       
  Occurrences:    20      
  Type:           Stochastic BRS
  Bindings:       7       
  Rules:          2       
  Max # states:   140     
  States:         5       
  Transitions:    4       
  Occurrences:    20      
  ../examples/river-cross.big
  Type:           BRS
  Bindings:       15      
  Rules:          6       
  Max # states:   140     
  States:         36      
  Transitions:    76      
  Occurrences:    76      
  Type:           BRS
  Bindings:       15      
  Rules:          6       
  Max # states:   140     
  States:         36      
  Transitions:    76      
  Occurrences:    76      
  ../examples/ccs.big
  Type:           BRS
  Bindings:       7       
  Rules:          1       
  Max # states:   140     
  States:         3       
  Transitions:    2       
  Occurrences:    2       
  Type:           BRS
  Bindings:       7       
  Rules:          1       
  Max # states:   140     
  States:         3       
  Transitions:    2       
  Occurrences:    2       
  ../examples/ccs_rec.big
  Type:           BRS
  Bindings:       11      
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    3       
  Occurrences:    3       
  Type:           BRS
  Bindings:       11      
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    3       
  Occurrences:    3       
  ../examples/plato-graphical.big
  Type:           BRS
  Bindings:       18      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    402     
  Occurrences:    489     
  Type:           BRS
  Bindings:       18      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    402     
  Occurrences:    489     
  ../examples/plato-graphical-loc.big
  Type:           BRS
  Bindings:       19      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    302     
  Occurrences:    310     
  Type:           BRS
  Bindings:       19      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    302     
  Occurrences:    310     
