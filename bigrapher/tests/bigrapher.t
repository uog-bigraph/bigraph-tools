Test BigraphER output.
  $ solvers="MSAT MCARD GBS"
  > for model in ../examples/rts_cts.big \
  >              ../examples/actors.big \
  >              ../examples/wireless.big \
  >              ../examples/savannah.big \
  >              ../examples/rrim.big \
  >              ../examples/rrim2.big \
  >              ../examples/hospital.big \
  >              ../examples/closures.big \
  >              ../examples/merge.big \
  >              ../examples/prob.big \
  >              ../examples/link_inst_map.big \
  >              ../examples/iter-merge.big \
  >              ../examples/string_params.big \
  >              ../examples/norules.big \
  >              ../examples/unique_entities.big \
  >              ../examples/conditional_turn_taking.big \
  >              ../examples/multiset-matching.big \
  >              ../examples/virus-simpl.big \
  >              ../examples/virus-multifirewall.big \
  >              ../examples/abrs-mobilesink.big \
  >              ../examples/prob_wsn.big \
  >              ../examples/mdp_wsn.big \
  >              ../examples/stoch_occ.big \
  >              ../examples/dining_philosophers.big \
  >              ../examples/river-cross.big \
  >              ../examples/ccs.big \
  >              ../examples/ccs_rec.big \
  >              ../examples/plato-graphical.big \
  >              ../examples/plato-graphical-loc.big \
  > ; do
  >   for solver in ${solvers};
  >   do
  >     echo "${model} -- ${solver}";
  >     bigrapher full -l out.csl -p out.tra -s -t out --solver=$solver --no-colors --debug -M 140 "$model";
  >   done
  > done
  ../examples/rts_cts.big -- MSAT
  Type:           Stochastic BRS
  Bindings:       66      
  Rules:          487     
  Max # states:   140     
  States:         128     
  Transitions:    154     
  Occurrences:    430     
  ../examples/rts_cts.big -- MCARD
  Type:           Stochastic BRS
  Bindings:       66      
  Rules:          487     
  Max # states:   140     
  States:         128     
  Transitions:    154     
  Occurrences:    430     
  ../examples/rts_cts.big -- GBS
  Type:           Stochastic BRS
  Bindings:       66      
  Rules:          487     
  Max # states:   140     
  States:         128     
  Transitions:    154     
  Occurrences:    430     
  ../examples/actors.big -- MSAT
  Type:           BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         10      
  Transitions:    12      
  Occurrences:    12      
  ../examples/actors.big -- MCARD
  Type:           BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         10      
  Transitions:    12      
  Occurrences:    12      
  ../examples/actors.big -- GBS
  Type:           BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         10      
  Transitions:    12      
  Occurrences:    12      
  ../examples/wireless.big -- MSAT
  Type:           Stochastic BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/wireless.big -- MCARD
  Type:           Stochastic BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/wireless.big -- GBS
  Type:           Stochastic BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/savannah.big -- MSAT
  Type:           BRS
  Bindings:       34      
  Rules:          11      
  Max # states:   140     
  States:         107     
  Transitions:    252     
  Occurrences:    252     
  ../examples/savannah.big -- MCARD
  Type:           BRS
  Bindings:       34      
  Rules:          11      
  Max # states:   140     
  States:         107     
  Transitions:    252     
  Occurrences:    252     
  ../examples/savannah.big -- GBS
  Type:           BRS
  Bindings:       34      
  Rules:          11      
  Max # states:   140     
  States:         107     
  Transitions:    252     
  Occurrences:    252     
  ../examples/rrim.big -- MSAT
  Type:           BRS
  Bindings:       9       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/rrim.big -- MCARD
  Type:           BRS
  Bindings:       9       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/rrim.big -- GBS
  Type:           BRS
  Bindings:       9       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/rrim2.big -- MSAT
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/rrim2.big -- MCARD
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/rrim2.big -- GBS
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/hospital.big -- MSAT
  Type:           BRS
  Bindings:       33      
  Rules:          10      
  Max # states:   140     
  States:         144     
  Transitions:    255     
  Occurrences:    345     
  ../examples/hospital.big -- MCARD
  Type:           BRS
  Bindings:       33      
  Rules:          10      
  Max # states:   140     
  States:         144     
  Transitions:    255     
  Occurrences:    345     
  ../examples/hospital.big -- GBS
  Type:           BRS
  Bindings:       33      
  Rules:          10      
  Max # states:   140     
  States:         144     
  Transitions:    255     
  Occurrences:    345     
  ../examples/closures.big -- MSAT
  Type:           BRS
  Bindings:       5       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  ../examples/closures.big -- MCARD
  Type:           BRS
  Bindings:       5       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  ../examples/closures.big -- GBS
  Type:           BRS
  Bindings:       5       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  ../examples/merge.big -- MSAT
  Type:           BRS
  Bindings:       16      
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/merge.big -- MCARD
  Type:           BRS
  Bindings:       16      
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/merge.big -- GBS
  Type:           BRS
  Bindings:       16      
  Rules:          1       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/prob.big -- MSAT
  Type:           Probabilistic BRS
  Bindings:       9       
  Rules:          3       
  Max # states:   140     
  States:         14      
  Transitions:    29      
  Occurrences:    64      
  ../examples/prob.big -- MCARD
  Type:           Probabilistic BRS
  Bindings:       9       
  Rules:          3       
  Max # states:   140     
  States:         14      
  Transitions:    29      
  Occurrences:    64      
  ../examples/prob.big -- GBS
  Type:           Probabilistic BRS
  Bindings:       9       
  Rules:          3       
  Max # states:   140     
  States:         14      
  Transitions:    29      
  Occurrences:    64      
  ../examples/link_inst_map.big -- MSAT
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         142     
  Transitions:    141     
  Occurrences:    141     
  ../examples/link_inst_map.big -- MCARD
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         142     
  Transitions:    141     
  Occurrences:    141     
  ../examples/link_inst_map.big -- GBS
  Type:           BRS
  Bindings:       10      
  Rules:          1       
  Max # states:   140     
  States:         142     
  Transitions:    141     
  Occurrences:    141     
  ../examples/iter-merge.big -- MSAT
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    1       
  Occurrences:    21      
  ../examples/iter-merge.big -- MCARD
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    1       
  Occurrences:    21      
  ../examples/iter-merge.big -- GBS
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    1       
  Occurrences:    21      
  ../examples/string_params.big -- MSAT
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/string_params.big -- MCARD
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/string_params.big -- GBS
  Type:           BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/norules.big -- MSAT
  Type:           BRS
  Bindings:       2       
  Rules:          0       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/norules.big -- MCARD
  Type:           BRS
  Bindings:       2       
  Rules:          0       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/norules.big -- GBS
  Type:           BRS
  Bindings:       2       
  Rules:          0       
  Max # states:   140     
  States:         1       
  Transitions:    0       
  Occurrences:    0       
  ../examples/unique_entities.big -- MSAT
  Type:           BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/unique_entities.big -- MCARD
  Type:           BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/unique_entities.big -- GBS
  Type:           BRS
  Bindings:       4       
  Rules:          1       
  Max # states:   140     
  States:         2       
  Transitions:    1       
  Occurrences:    1       
  ../examples/conditional_turn_taking.big -- MSAT
  Type:           BRS
  Bindings:       12      
  Rules:          4       
  Max # states:   140     
  States:         12      
  Transitions:    16      
  Occurrences:    20      
  ../examples/conditional_turn_taking.big -- MCARD
  Type:           BRS
  Bindings:       12      
  Rules:          4       
  Max # states:   140     
  States:         12      
  Transitions:    16      
  Occurrences:    20      
  ../examples/conditional_turn_taking.big -- GBS
  Type:           BRS
  Bindings:       12      
  Rules:          4       
  Max # states:   140     
  States:         12      
  Transitions:    16      
  Occurrences:    20      
  ../examples/multiset-matching.big -- MSAT
  Type:           BRS
  Bindings:       8       
  Rules:          3       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  ../examples/multiset-matching.big -- MCARD
  Type:           BRS
  Bindings:       8       
  Rules:          3       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  ../examples/multiset-matching.big -- GBS
  Type:           BRS
  Bindings:       8       
  Rules:          3       
  Max # states:   140     
  States:         2       
  Transitions:    2       
  Occurrences:    3       
  ../examples/virus-simpl.big -- MSAT
  Type:           Probabilistic BRS
  Bindings:       12      
  Rules:          3       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  ../examples/virus-simpl.big -- MCARD
  Type:           Probabilistic BRS
  Bindings:       12      
  Rules:          3       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  ../examples/virus-simpl.big -- GBS
  Type:           Probabilistic BRS
  Bindings:       12      
  Rules:          3       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  ../examples/virus-multifirewall.big -- MSAT
  Type:           Probabilistic BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  ../examples/virus-multifirewall.big -- MCARD
  Type:           Probabilistic BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  ../examples/virus-multifirewall.big -- GBS
  Type:           Probabilistic BRS
  Bindings:       16      
  Rules:          4       
  Max # states:   140     
  States:         142     
  Transitions:    376     
  Occurrences:    411     
  ../examples/abrs-mobilesink.big -- MSAT
  Type:           Action (Nondeterministic) BRS
  Bindings:       52      
  Rules:          85      
  Max # states:   140     
  States:         142     
  Transitions:    222     
  Occurrences:    444     
  ../examples/abrs-mobilesink.big -- MCARD
  Type:           Action (Nondeterministic) BRS
  Bindings:       52      
  Rules:          85      
  Max # states:   140     
  States:         142     
  Transitions:    222     
  Occurrences:    444     
  ../examples/abrs-mobilesink.big -- GBS
  Type:           Action (Nondeterministic) BRS
  Bindings:       52      
  Rules:          85      
  Max # states:   140     
  States:         142     
  Transitions:    222     
  Occurrences:    444     
  ../examples/prob_wsn.big -- MSAT
  Type:           Probabilistic BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    6       
  Occurrences:    12      
  ../examples/prob_wsn.big -- MCARD
  Type:           Probabilistic BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    6       
  Occurrences:    12      
  ../examples/prob_wsn.big -- GBS
  Type:           Probabilistic BRS
  Bindings:       8       
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    6       
  Occurrences:    12      
  ../examples/mdp_wsn.big -- MSAT
  Type:           Action (Nondeterministic) BRS
  Bindings:       13      
  Rules:          4       
  Max # states:   140     
  States:         3       
  Transitions:    4       
  Occurrences:    4       
  ../examples/mdp_wsn.big -- MCARD
  Type:           Action (Nondeterministic) BRS
  Bindings:       13      
  Rules:          4       
  Max # states:   140     
  States:         3       
  Transitions:    4       
  Occurrences:    4       
  ../examples/mdp_wsn.big -- GBS
  Type:           Action (Nondeterministic) BRS
  Bindings:       13      
  Rules:          4       
  Max # states:   140     
  States:         3       
  Transitions:    4       
  Occurrences:    4       
  ../examples/stoch_occ.big -- MSAT
  Type:           Stochastic BRS
  Bindings:       7       
  Rules:          2       
  Max # states:   140     
  States:         5       
  Transitions:    4       
  Occurrences:    20      
  ../examples/stoch_occ.big -- MCARD
  Type:           Stochastic BRS
  Bindings:       7       
  Rules:          2       
  Max # states:   140     
  States:         5       
  Transitions:    4       
  Occurrences:    20      
  ../examples/stoch_occ.big -- GBS
  Type:           Stochastic BRS
  Bindings:       7       
  Rules:          2       
  Max # states:   140     
  States:         5       
  Transitions:    4       
  Occurrences:    20      
  ../examples/dining_philosophers.big -- MSAT
  Type:           BRS
  Bindings:       11      
  Rules:          4       
  Max # states:   140     
  States:         142     
  Transitions:    385     
  Occurrences:    434     
  ../examples/dining_philosophers.big -- MCARD
  Type:           BRS
  Bindings:       11      
  Rules:          4       
  Max # states:   140     
  States:         142     
  Transitions:    385     
  Occurrences:    434     
  ../examples/dining_philosophers.big -- GBS
  Type:           BRS
  Bindings:       11      
  Rules:          4       
  Max # states:   140     
  States:         142     
  Transitions:    385     
  Occurrences:    434     
  ../examples/river-cross.big -- MSAT
  Type:           BRS
  Bindings:       15      
  Rules:          6       
  Max # states:   140     
  States:         36      
  Transitions:    76      
  Occurrences:    76      
  ../examples/river-cross.big -- MCARD
  Type:           BRS
  Bindings:       15      
  Rules:          6       
  Max # states:   140     
  States:         36      
  Transitions:    76      
  Occurrences:    76      
  ../examples/river-cross.big -- GBS
  Type:           BRS
  Bindings:       15      
  Rules:          6       
  Max # states:   140     
  States:         36      
  Transitions:    76      
  Occurrences:    76      
  ../examples/ccs.big -- MSAT
  Type:           BRS
  Bindings:       7       
  Rules:          1       
  Max # states:   140     
  States:         3       
  Transitions:    2       
  Occurrences:    2       
  ../examples/ccs.big -- MCARD
  Type:           BRS
  Bindings:       7       
  Rules:          1       
  Max # states:   140     
  States:         3       
  Transitions:    2       
  Occurrences:    2       
  ../examples/ccs.big -- GBS
  Type:           BRS
  Bindings:       7       
  Rules:          1       
  Max # states:   140     
  States:         3       
  Transitions:    2       
  Occurrences:    2       
  ../examples/ccs_rec.big -- MSAT
  Type:           BRS
  Bindings:       11      
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    3       
  Occurrences:    3       
  ../examples/ccs_rec.big -- MCARD
  Type:           BRS
  Bindings:       11      
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    3       
  Occurrences:    3       
  ../examples/ccs_rec.big -- GBS
  Type:           BRS
  Bindings:       11      
  Rules:          2       
  Max # states:   140     
  States:         4       
  Transitions:    3       
  Occurrences:    3       
  ../examples/plato-graphical.big -- MSAT
  Type:           BRS
  Bindings:       18      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    402     
  Occurrences:    489     
  ../examples/plato-graphical.big -- MCARD
  Type:           BRS
  Bindings:       18      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    402     
  Occurrences:    489     
  ../examples/plato-graphical.big -- GBS
  Type:           BRS
  Bindings:       18      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    402     
  Occurrences:    489     
  ../examples/plato-graphical-loc.big -- MSAT
  Type:           BRS
  Bindings:       19      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    302     
  Occurrences:    310     
  ../examples/plato-graphical-loc.big -- MCARD
  Type:           BRS
  Bindings:       19      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    302     
  Occurrences:    310     
  ../examples/plato-graphical-loc.big -- GBS
  Type:           BRS
  Bindings:       19      
  Rules:          9       
  Max # states:   140     
  States:         142     
  Transitions:    302     
  Occurrences:    310     
