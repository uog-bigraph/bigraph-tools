%{

open Loc
open Ast
open Bigraph

%}

(* BIG *)

%token            EOF

%token <string>   CIDE
%token <string>   IDE
%token <int>      CINT
%token <float>    CFLOAT
%token <string>   CSTRING

%token 	          CTRL
%token            ATOMIC
%token            BIG
%token            REACT
%token            INIT
%token            RULES
%token		  PREDS
%token            INT
%token            FLOAT
%token            STRING
%token            FUN
%token            ACTIONS
%token            BEGIN
%token            BRS
%token            PBRS
%token            SBRS
%token            ABRS
%token            END
%token		  MERGE
%token		  SPLIT
(* %token            SORT TRUE FALSE NOT *)
%token            SHARE
%token            BY
%token            IN
%token            ID
%token            ARR
%token            LARR
%token            RARR
%token            AT
%token            EQUAL
%token            PLUS
%token            MINUS
%token            PROD
%token            SLASH
%token            CARET
%token            PAR
%token            PPAR
%token            IF
%token            CTX
%token            PARAM
%token            BANG
%token            LSBR RSBR LCBR RCBR LPAR RPAR
%token            COLON SEMICOLON COMMA
%token            ABS
%token            PIPE DPIPE
%token            DOT

%left  PIPE DPIPE
%left  PLUS MINUS
%left  PROD SLASH
%right CARET

%start model
%type <Ast.model> model

%%

model:
  | model_decs = dec_list; model_rs = rs; EOF
    {
      { model_decs; model_rs; model_loc = make $loc; }
    }
;

dec_list:
  | decs = nonempty_list(terminated(dec, SEMICOLON))
    { (decs, make $loc) }
;

dec:
  | d = preceded(INT, dec_gen)
    { Dint (d, make $loc) }
  | d = preceded(FLOAT, dec_gen)
    { Dfloat (d, make $loc) }
  | d = preceded(STRING, dec_gen)
    { Dstr (d, make $loc) }
  | d = dec_ctrl
    { Dctrl d }
  | d = dec_big
    { Dbig d }
  | d = dec_react
    { Dreact d }
;

dec_gen:
  | d_id = IDE; EQUAL; d_exp = exp
    {
      { d_id; d_exp; d_loc = make $loc; }
    }
;

dec_ctrl:
  | ATOMIC; exp = ctrl_exp
    { Atomic (exp, make $loc) }
  | exp = ctrl_exp
    { Non_atomic (exp, make $loc) }
;

ctrl_exp:
  | CTRL; id = CIDE; EQUAL; v = CINT
    { Ctrl_exp (id, v, make $loc) }
  | FUN;
    CTRL;
    id = CIDE;
    par = delimited(LPAR, ide_list_nonempty, RPAR);
    EQUAL;
    v = CINT
    { Ctrl_fun_exp (id, par, v, make $loc) }
;

dec_big:
  | BIG; id = IDE; EQUAL; b = bexp
    { Big_exp (id, b, make $loc) }
  | FUN;
    BIG;
    id = IDE;
    par = delimited(LPAR, ide_list_nonempty, RPAR);
    EQUAL;
    b = bexp
    { Big_fun_exp (id, par, b, make $loc) }
;

dec_react:
  | REACT;
    id = IDE;
    EQUAL;
    lhs = bexp;
    l = arrow;
    rhs = bexp;
    eta = option(eta_exp);
    c = option(conds)
    { React_exp (id, "", 0, lhs, rhs, l, eta, c, make $loc) }
  | FUN;
    REACT;
    id = IDE;
    par = delimited(LPAR, ide_list_nonempty, RPAR);
    EQUAL;
    lhs = bexp;
    l = arrow;
    rhs = bexp;
    eta = option(eta_exp);
    c = option(conds)
    { React_fun_exp (id, "", 0, par, lhs, rhs, l, eta, c, make $loc) }
;

arrow:
  | ARR
    { None }
  | r = delimited(LARR, exp, RARR)
    { Some r }
;

conds:
  | IF; conds = separated_list(COMMA, cond)
    { conds }
;

cond:
  | neg = boption(BANG); pred = bexp; IN; p = place
    {
      { neg; pred; place = p; loc = make $loc }
    }
;

place:
  | PARAM
    { Cond_Param }
  | CTX
    { Cond_Ctx }
;

eta_exp:
  | eta = preceded(AT, delimited(LSBR, int_list, RSBR))
    { (eta, make $loc) }
;

int_list:
  | ints = separated_list(COMMA, CINT)
    { ints }
;

rs:
  | BEGIN;
    dbrs_type = rs_type;
    p = list(terminated(param, SEMICOLON));
    i = terminated(init, SEMICOLON);
    dbrs_pri = rules;
    dbrs_actions = actions;
    dbrs_preds = preds;
    END
    {
      { dbrs_type;
	dbrs_params = (p, make $loc(p));
	dbrs_init = (i, make $loc(i));
        dbrs_pri;
	dbrs_actions;
	dbrs_preds;
	dbrs_loc = make $loc;
       }
    }
;

rs_type:
  | BRS
    { (Rs.BRS, make $loc) }
  | PBRS
    { (Rs.PBRS, make $loc) }
  | SBRS
    { (Rs.SBRS, make $loc) }
  | ABRS
    { (Rs.ABRS, make $loc) }
;

preds:
  | /* EMPTY */
    { ([ ], make $loc) }
  | PREDS;
    EQUAL;
    l = delimited(LCBR, separated_nonempty_list(COMMA, pred_def), RCBR);
    SEMICOLON
    { (l, make $loc) }
;

pred_def:
  | id = IDE; reward = option(delimited(LSBR, exp, RSBR))
    { Pred_id (id, reward, make $loc) }
  | id = IDE;
    exps = delimited(LPAR, exp_list, RPAR);
    reward = option(delimited(LSBR, exp, RSBR))
    { Pred_id_fun (id, exps, reward, make $loc) }
;

actions:
  | /* EMPTY */
    { ([ ], make $loc) }
  | ACTIONS;
    EQUAL;
    l = delimited(LSBR, separated_nonempty_list(COMMA, action_def), RSBR);
    SEMICOLON
    { (l, make $loc) }
;

action_def:
  | action_id = IDE;
    action_reward = option(delimited(LSBR, exp, RSBR));
    EQUAL;
    action_rules = delimited(LCBR, separated_nonempty_list(COMMA, IDE), RCBR)
    { { action_id; action_rules; action_reward; action_loc = make $loc; } }
;

init:
  | INIT; id = IDE
    { Init (id, make $loc) }
  | INIT; id = IDE; exps = delimited(LPAR, exp_list, RPAR)
    { Init_fun (id, exps, make $loc) }
;

exp_list:
  | exps = separated_nonempty_list(COMMA, exp)
    { exps }
;

exp:
  | exp = num_exp
    { ENum exp }
  | exp = str_exp
    { EStr exp }
  | exp = var_exp
    { EVar exp }
  | exp = op_exp
    { EOp exp }
  | exp = fn_exp
    { EFn exp }
  | exp = delimited(LPAR, exp, RPAR)
    { exp }
;

num_exp:
  | v = CINT
    { Num_int_val (v, make $loc) }
  | v = CFLOAT
    { Num_float_val (v, make $loc) }
;

var_exp:
  | id = IDE
    { Var (id, make $loc) }
;

str_exp:
  | v = CSTRING
    { Str_val (v, make $loc) }
;

op_exp:
  | a = exp; PLUS; b = exp
    { Plus (a, b, make $loc) }
  | a = exp; MINUS; b = exp
    { Minus (a, b, make $loc) }
  | MINUS; a = exp
    { UMinus (a, make $loc) }
  | a = exp; PROD; b = exp
    { Prod (a, b, make $loc) }
  | a = exp; SLASH; b = exp
    { Div (a, b, make $loc) }
  | a = exp; CARET; b = exp
    { Pow (a, b, make $loc) }
;

fn_exp:
  | ABS; a = delimited(LPAR, exp, RPAR)
    { Abs (a, make $loc) };

ide_list_nonempty:
  | ides = separated_nonempty_list(COMMA, IDE)
    { ides }
;

param:
  | INT; ides = ide_list_nonempty; EQUAL; params = param_int_exp
    { Param_int (ides, params, make $loc) }
  | FLOAT; ides = ide_list_nonempty; EQUAL; params = param_float_exp
    { Param_float (ides, params, make $loc) }
  | STRING; ides = ide_list_nonempty; EQUAL; params = param_str_exp
    { Param_str (ides, params, make $loc) }
;

param_int_exp:
  | exp = exp
    { Param_int_val (exp, make $loc) }
  | l = delimited(LCBR, exp_list, RCBR)
    { Param_int_set (l, make $loc) }
  | LSBR; start = exp; COLON; interval = exp; COLON; stop = exp; RSBR
    { Param_int_range (start, interval, stop, make $loc) }
;

param_float_exp:
  | exp = exp
    { Param_float_val (exp, make $loc) }
  | l = delimited(LCBR, exp_list, RCBR)
    { Param_float_set (l, make $loc) }
  | LSBR; start = exp; COLON; interval = exp; COLON; stop = exp; RSBR
    { Param_float_range (start, interval, stop, make $loc) }
;

param_str_exp:
  | exp = exp
    { Param_str_val (exp, make $loc) }
  | l = delimited(LCBR, exp_list, RCBR)
    { Param_str_set (l, make $loc) }
;

priority_class:
  | l = delimited(LCBR, separated_nonempty_list(COMMA, rule_ide), RCBR)
    { Pr (l, make $loc) }
  | l = delimited(LPAR, separated_nonempty_list(COMMA, rule_ide), RPAR)
    { Pr_red (l, make $loc) }
;

rules:
  | RULES;
    EQUAL;
    l = delimited(LSBR, separated_list(COMMA, priority_class), RSBR);
    SEMICOLON
    { (l, make $loc) }
;

rule_ide:
  | id = IDE
    { Rul_id (id, make $loc) }
  | id = IDE; params = delimited(LPAR, exp_list, RPAR)
    { Rul_id_fun (id, params, make $loc) }
;

bexp:
  | w = wire_exp; b = delimited(LPAR, bexp, RPAR)
    { Big_wire (w, b, make $loc) }
  | w = wire_exp; ion = ion_exp
    { Big_wire (w, Big_ion ion, make $loc) }
  | w = wire_exp; ion = ion_exp; DOT; b = simple_bexp
    {
      Big_wire (w,
                Big_nest (ion, b, make ($startpos(ion), $endpos(b))),
                make $loc)
    }
  | w = wire_exp; id = IDE
    { Big_wire (w,
                Big_var (id, make ($startpos(id), $endpos(id))),
                make $loc)
    }
  | w = wire_exp; id = IDE; params = delimited(LPAR, exp_list, RPAR)
    {
      Big_wire (w,
                Big_var_fun (id, params, make ($startpos(id), $endpos(params))),
                make $loc)
    }
  | b = simple_bexp
    { b }
  | a = bexp; PROD; b = bexp
    { Big_comp (a, b, make $loc) }
  | a = bexp; PLUS; b = bexp
    { Big_tens (a, b, make $loc) }
  | a = bexp; PIPE; b = bexp
    { Big_par (a, b, make $loc) }
  | a = bexp; DPIPE; b = bexp
    { Big_ppar (a, b, make $loc) }
  | SHARE; p = simple_bexp; BY; psi = simple_bexp; IN; c = simple_bexp
    { Big_share (p, psi, c, make $loc) }
;

simple_bexp:
  | b = delimited(LPAR, bexp, RPAR)
    { b }
  | a = delimited(LPAR, bexp, RPAR); b = delimited(LPAR, bexp, RPAR)
    { Big_comp (a, b, make $loc) }
  | b = id_exp
    { Big_id b }
  | MERGE; n = o_delim_int_2
    { Big_merge (n, make $loc) }
  | SPLIT; n = o_delim_int_2
    { Big_split (n, make $loc) }
  | SLASH; cl_name = IDE
    { Big_close { cl_name; cl_loc = make $loc; } }
  | out_name = IDE; SLASH; in_names = delimited(LCBR, ide_list_nonempty, RCBR)
    { Big_sub { out_name; in_names; sub_loc = make $loc; } }
  | id = delimited(LCBR, IDE, RCBR)
    { Big_new_name (id, make $loc) }
  | b = CINT
    { Big_num (b, make $loc) }
  | LPAR;
    plc_parents =
      delimited(LSBR,
                separated_list(COMMA, delimited(LCBR, int_list, RCBR)),
                RSBR);
    COMMA;
    plc_roots = CINT;
    RPAR
    { Big_plc { plc_parents; plc_roots; plc_loc = make $loc; } }
  | id = IDE
    { Big_var (id, make $loc) }
  | id = IDE; params = delimited(LPAR, exp_list, RPAR)
    { Big_var_fun (id, params, make $loc) }
  | b = ion_exp
    { Big_ion b }
  | a = ion_exp; DOT; b = simple_bexp
    { Big_nest (a, b, make $loc) }
  | PAR; LPAR; exp = exp; COMMA; b = bexp; RPAR
    { Big_par_fn (exp, b, make $loc) }
  | PPAR; LPAR; exp = exp; COMMA; b = bexp; RPAR
    { Big_ppar_fn (exp, b, make $loc) }
;

id_exp:
  | ID; id_place = o_delim_int_1
    { { id_place; id_link = []; id_loc = make $loc; } }
  | ID; id_link = delimited(LCBR, ide_list_nonempty, RCBR)
    { { id_place = 0; id_link; id_loc = make $loc; } }
  | ID;
    LPAR;
    id_place = CINT;
    COMMA;
    id_link = delimited(LCBR, ide_list_nonempty, RCBR);
    RPAR
    { { id_place; id_link; id_loc = make $loc; } }
;

o_delim_int_1:
  | n = option(delimited(LPAR, CINT, RPAR))
    { match n with None -> 1 | Some n -> n }
;

o_delim_int_2:
  | n = option(delimited(LPAR,CINT,RPAR))
    { match n with None -> 2 | Some n -> n }
;

ion_exp:
  | id = CIDE; names = loption(delimited(LCBR, ide_list_nonempty, RCBR))
    { Big_ion_exp (id, names, make $loc) }
  | id = CIDE;
    params = delimited(LPAR, exp_list, RPAR);
    names = loption(delimited(LCBR, ide_list_nonempty, RCBR))
    { Big_ion_fun_exp (id, params, names, make $loc) }
;

wire_exp:
  | cs = nonempty_list(closure)
    { Close_exp cs  }
  | out_name = IDE; SLASH; in_names = delimited(LCBR, ide_list_nonempty, RCBR)
    { Sub_exp { out_name; in_names; sub_loc = make $loc; } }
  | SLASH; m_cl_names = delimited(LCBR, ide_list_nonempty, RCBR)
    { Merge_close_exp { m_cl_names; m_cl_loc = make $loc; } }
;

closure:
  | SLASH; cl_name = IDE
    { { cl_name; cl_loc = make $loc; } }
;
