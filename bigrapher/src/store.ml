open Format
open Ast
open Unify
open Bigraph

module Make
    (T : Rs.RS) (P : sig
      val parse_react :
        string ->
        Big.t ->
        Big.t ->
        ?conds:AppCond.t list ->
        [ `E of unit | `F of float | `P of string * int * float ] ->
        Fun.t option ->
        Loc.t ->
        (T.react, string) result
    end) =
struct
  type store_type =
    [ `core_val of core_type
    | `big_val of fun_type
    | `lambda of lambda
    | `param of base_type ]

  let string_of_store_t = function
    | `core_val t -> string_of_core_t t
    | `big_val t -> string_of_fun_t t
    | `lambda t -> string_of_lambda t
    | `param `int -> "int param"
    | `param `float -> "float param"
    | `param `string -> "string param"

  let dom_of_lambda = function
    | `core_val _ | `big_val _ | `param _ -> assert false
    | `lambda t -> fst t

  let resolve_t (env : store_t) = function
    | `core_val t -> `core_val (resolve_type env t)
    | `lambda (t, t') -> `lambda (resolve_types env t, t')
    | t -> t

  type store_val =
    | Int of int
    | Float of float
    | Str of string
    | Big of Big.t
    | Big_fun of big_exp * Id.t list
    | Ctrl of Ctrl.t
    | Ctrl_fun of int * Id.t list
    | A_ctrl of Ctrl.t
    | A_ctrl_fun of int * Id.t list
    | React of T.react
    | React_fun of
        Id.t
        * int
        * big_exp
        * big_exp
        * Fun.t option
        * cond_exp list
        * exp option
        * Id.t list
    | Int_param of int list
    | Float_param of float list
    | Str_param of string list

  let string_of_store_val = function
    | Int x -> string_of_int x
    | Float x -> string_of_float x
    | Str s -> s
    | Big x -> Big.to_string x
    | Big_fun _ -> "<fun big>"
    | A_ctrl c | Ctrl c -> Ctrl.to_string c
    | A_ctrl_fun _ | Ctrl_fun _ -> "<fun ctrl>"
    | React r -> T.string_of_react r
    | React_fun _ -> "<fun react>"
    | Int_param p -> "(" ^ String.concat "," (List.map string_of_int p) ^ ")"
    | Float_param p ->
        "(" ^ String.concat "," (List.map string_of_float p) ^ ")"
    | Str_param p -> "(" ^ String.concat "," p ^ ")"

  let def_val = function
    | `g _ -> ENum (Num_int_val (0, Loc.dummy_loc))
    | `b `int -> ENum (Num_int_val (0, Loc.dummy_loc))
    | `b `float -> ENum (Num_float_val (0.0, Loc.dummy_loc))
    | `b `string -> EStr (Str_val ("str", Loc.dummy_loc))

  type typed_store_val = {
    sval : store_val;
    styp : store_type;
    loc : Loc.t;
    mutable used : bool;
  }

  let assign_type (v : store_val) env_t =
    let assign_type_forms = List.map (fun _ -> next_gen ()) in
    let update forms t =
      let fresh_t = assign_type_forms forms in
      (`lambda (box_gen_types fresh_t, t), add_types fresh_t env_t)
    in
    match v with
    | Int _ -> (`core_val (`b `int), env_t)
    | Float _ -> (`core_val (`b `float), env_t)
    | Str _ -> (`core_val (`b `string), env_t)
    | Big _ -> (`big_val `big, env_t)
    | Big_fun (_, forms) -> update forms `big
    | Ctrl c | A_ctrl c -> (`big_val (`ctrl (Ctrl.arity c)), env_t)
    | Ctrl_fun (arity, forms) | A_ctrl_fun (arity, forms) ->
        update forms (`ctrl arity)
    | React _ -> (`big_val `react, env_t)
    | React_fun (_, _, _, _, _, _, _, forms) -> update forms `react
    | Int_param _ -> (`param `int, env_t)
    | Float_param _ -> (`param `float, env_t)
    | Str_param _ -> (`param `string, env_t)

  type store = typed_store_val Base.H_string.t

  let get_type t = t.styp
  let bindings = Base.H_string.length

  type p_class_list = T.p_class list

  type error =
    | Wrong_type of store_type * store_type (* (current, expected) *)
    | Atomic_ctrl of Id.t
    | Arity of string * int * int (* (id, current, expected) *)
    | Unbound_variable of Id.t
    | Div_by_zero
    | Comp of Big.inter * Big.inter
    | Invalid_class
    | Invalid_priorities
    | Tens of Link.Face.t * Link.Face.t (* (in , out) *)
    | Share
    | Unknown_big of int
    | Reaction of string (* error message *)
    | Init_not_ground
    | Not_face of Id.t list
    | Invalid_iter_op of int

  type warning = Multiple_declaration of Id.t * Loc.t * Loc.t

  exception ERROR of error * Loc.t

  let report_error_aux fmt = function
    | Wrong_type (curr, exp) ->
        fprintf fmt
          "This expression has type %s but an expression was expected of \
           type %s"
          (string_of_store_t curr) (string_of_store_t exp)
    | Atomic_ctrl id ->
        fprintf fmt
          "Control %s is atomic but it is here used in a nesting expression"
          id
    | Arity (id, ar, ar_dec) ->
        fprintf fmt
          "Control %s has arity %d but a control of arity %d was expected" id
          ar ar_dec
    | Unbound_variable s -> fprintf fmt "Unbound variable %s" s
    | Div_by_zero -> fprintf fmt "Division by zero"
    | Comp (i, j) ->
        fprintf fmt "Interfaces %s and %s do not match in the composition"
          (Big.string_of_inter i) (Big.string_of_inter j)
    | Tens (inner, outer) ->
        fprintf fmt
          "Tensor product has common inner names %s and common outer names \
           %s"
          (Link.string_of_face inner)
          (Link.string_of_face outer)
    | Share -> fprintf fmt "Invalid sharing expression"
    | Unknown_big v -> fprintf fmt "Expression %d is not a valid bigraph" v
    | Reaction msg -> fprintf fmt "Invalid Reaction: %s" msg
    | Invalid_class -> fprintf fmt "Invalid epression for a priority class"
    | Invalid_priorities ->
        fprintf fmt "Invalid expression for a priority structure"
    | Init_not_ground -> fprintf fmt "init bigraph is not ground"
    | Not_face ns ->
        fprintf fmt
          "Expression {%s} is not a valid interface as it contains \
           duplicate names"
          (String.concat "," ns)
    | Invalid_iter_op n ->
        fprintf fmt
          "Argument of iterated operator evaluates to %d but a value >= 0 \
           was expected"
          n

  let report_error c err =
    fprintf err_formatter "@[%s%a@]@." (Utils.err_opt c) report_error_aux err

  let report_warning c = function
    | Multiple_declaration (id, p, p') ->
        fprintf err_formatter
          "%a@[%sIdentifier %s was already used at %s@]@." Loc.print_loc p'
          (Utils.warn_opt c) id (Loc.string_of_pos p)

  (******** SCOPE *********)

  module ScopeMap = Map.Make (struct
    type t = Id.t

    let compare = Id.compare
  end)

  type scope = typed_store_val ScopeMap.t

  (******** GET FUNCTIONS *********)

  let fetch id p (aux : typed_store_val -> 'a) (scope : scope) (env : store)
      =
    try aux (ScopeMap.find id scope)
    with Not_found -> (
      match Base.H_string.find_opt env id with
      | Some v -> aux v
      | None -> raise (ERROR (Unbound_variable id, p)))

  let badly_typed v e p = raise (ERROR (Wrong_type (get_type v, e), p))

  let get_int id p scope env =
    let aux = function
      | { sval = Int v; _ } -> v
      | x -> badly_typed x (`core_val (`b `int)) p
    in
    fetch id p aux scope env

  let get_float id p scope env =
    let aux = function
      | { sval = Float v; _ } -> v
      | x -> badly_typed x (`core_val (`b `float)) p
    in
    fetch id p aux scope env

  let get_var id p scope env =
    let aux = function
      | { sval = Float _ as v; _ }
      | { sval = Int _ as v; _ }
      | { sval = Str _ as v; _ } ->
          v
      | x -> badly_typed x (`core_val (`g core_type_str)) p
    in
    fetch id p aux scope env

  let get_num id p scope env =
    let aux = function
      | { sval = Float _ as v; _ } | { sval = Int _ as v; _ } -> v
      | x -> badly_typed x (`core_val (`g int_or_float)) p
    in
    fetch id p aux scope env

  let get_str id p scope env =
    let aux = function
      | { sval = Str s; _ } -> s
      | x -> badly_typed x (`core_val (`b `string)) p
    in
    fetch id p aux scope env

  let get_ctrl id arity p (env : store) =
    match Base.H_string.find_opt env id with
    | None -> raise (ERROR (Unbound_variable id, p))
    | Some ({ sval = A_ctrl c; _ } as s) | Some ({ sval = Ctrl c; _ } as s)
      ->
        let a = Ctrl.arity c in
        if a = arity then (
          s.used <- true;
          c)
        else raise (ERROR (Arity (id, a, arity), p))
    | Some x -> badly_typed x (`big_val (`ctrl arity)) p

  let get_ctrl_fun id arity act_types p (env : store) =
    match Base.H_string.find_opt env id with
    | None -> raise (ERROR (Unbound_variable id, p))
    | Some ({ sval = A_ctrl_fun (a, forms); styp = t; _ } as s)
    | Some ({ sval = Ctrl_fun (a, forms); styp = t; _ } as s) ->
        if a = arity then (
          s.used <- true;
          (a, forms, t))
        else raise (ERROR (Arity (id, a, arity), p))
    | Some x -> badly_typed x (`lambda (act_types, `ctrl arity)) p

  let is_atomic id p (env : store) =
    match Base.H_string.find_opt env id with
    | None -> raise (ERROR (Unbound_variable id, p))
    | Some v -> (
        match v.sval with
        | A_ctrl _ | A_ctrl_fun _ -> true
        | Int _ | Float _ | Str _ | Big _
        | Big_fun (_, _)
        | Ctrl _
        | Ctrl_fun (_, _)
        | React _
        | React_fun (_, _, _, _, _, _, _, _)
        | Int_param _ | Str_param _ | Float_param _ ->
            false)

  let get_big id p (env : store) =
    match Base.H_string.find_opt env id with
    | None -> raise (ERROR (Unbound_variable id, p))
    | Some ({ sval = Big b; _ } as s) ->
        s.used <- true;
        b
    | Some x -> badly_typed x (`big_val `big) p

  let get_big_fun id arg_types p (env : store) =
    match Base.H_string.find_opt env id with
    | None -> raise (ERROR (Unbound_variable id, p))
    | Some ({ sval = Big_fun (exp, forms); styp = t; _ } as s) ->
        s.used <- true;
        (exp, forms, t)
    | Some x -> badly_typed x (`lambda (arg_types, `big)) p

  let get_react id p (env : store) =
    match Base.H_string.find_opt env id with
    | None -> raise (ERROR (Unbound_variable id, p))
    | Some ({ sval = React r; _ } as s) ->
        s.used <- true;
        r
    | Some x -> badly_typed x (`big_val `react) p

  let get_react_fun id arg_types p (env : store) =
    match Base.H_string.find_opt env id with
    | None -> raise (ERROR (Unbound_variable id, p))
    | Some
        ({
           sval = React_fun (a, rew, l, r, eta, conds, label, forms);
           styp = t;
           _;
         } as s) ->
        s.used <- true;
        (a, rew, l, r, label, eta, conds, forms, t)
    | Some x -> badly_typed x (`lambda (arg_types, `react)) p

  (******** EVAL FUNCTIONS *********)

  let div_int l r p =
    match r with 0 -> raise (ERROR (Div_by_zero, p)) | d -> l / d

  let rec pow_int_aux base exp acc =
    match exp with 0 -> 1 | _ -> pow_int_aux base (exp - 1) (acc * base)

  let pow_int b e p =
    if e < 0 then
      raise
        (ERROR (Wrong_type (`core_val (`b `float), `core_val (`b `int)), p))
    else pow_int_aux b e 1

  let eval_str (exp : str_exp) (_scope : scope) (_env : store) =
    match exp with Str_val (v, _) -> Str v

  let eval_num (exp : num_exp) (_scope : scope) (_env : store) =
    match exp with
    | Num_int_val (v, _) -> Int v
    | Num_float_val (v, _) -> Float v

  let eval_var (exp : var_exp) (scope : scope) (env : store) =
    match exp with Var (id, p) -> get_var id p scope env

  let eval_plus (a : store_val) (b : store_val) p =
    match (a, b) with
    | Float f, Float g -> Float (f +. g)
    | Int f, Float g -> Float (float f +. g)
    | Float f, Int g -> Float (f +. float g)
    | Int f, Int g -> Int (f + g)
    | _, Float _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type a []), `core_val (`b `float)), p))
    | Float _, _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type b []), `core_val (`b `float)), p))
    | _, Int _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type a []), `core_val (`b `int)), p))
    | Int _, _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type b []), `core_val (`b `int)), p))
    | _ -> assert false

  let eval_minus (a : store_val) (b : store_val) p =
    match (a, b) with
    | Float f, Float g -> Float (f -. g)
    | Int f, Float g -> Float (float f -. g)
    | Float f, Int g -> Float (f -. float g)
    | Int f, Int g -> Int (f - g)
    | _, Float _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type a []), `core_val (`b `float)), p))
    | Float _, _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type b []), `core_val (`b `float)), p))
    | _, Int _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type a []), `core_val (`b `int)), p))
    | Int _, _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type b []), `core_val (`b `int)), p))
    | _ -> assert false

  let eval_uminus (a : store_val) _p =
    match a with
    | Float f -> Float (-.f)
    | Int i -> Int (-i)
    | _ -> assert false

  let eval_prod (a : store_val) (b : store_val) p =
    match (a, b) with
    | Float f, Float g -> Float (f *. g)
    | Int f, Float g -> Float (float f *. g)
    | Float f, Int g -> Float (f *. float g)
    | Int f, Int g -> Int (f * g)
    | _, Float _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type a []), `core_val (`b `float)), p))
    | Float _, _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type b []), `core_val (`b `float)), p))
    | _, Int _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type a []), `core_val (`b `int)), p))
    | Int _, _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type b []), `core_val (`b `int)), p))
    | _ -> assert false

  let eval_div (a : store_val) (b : store_val) p =
    match (a, b) with
    | Float f, Float g -> Float (f /. g)
    | Int f, Float g -> Float (float f /. g)
    | Float f, Int g -> Float (f /. float g)
    | Int f, Int g -> Int (div_int f g p)
    | _, Float _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type a []), `core_val (`b `float)), p))
    | Float _, _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type b []), `core_val (`b `float)), p))
    | _, Int _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type a []), `core_val (`b `int)), p))
    | Int _, _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type b []), `core_val (`b `int)), p))
    | _ -> assert false

  let eval_pow (a : store_val) (b : store_val) p =
    match (a, b) with
    | Float f, Float g -> Float (f ** g)
    | Int f, Float g -> Float (float f ** g)
    | Float f, Int g -> Float (f ** float g)
    | Int f, Int g -> Int (pow_int f g p)
    | _, Float _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type a []), `core_val (`b `float)), p))
    | Float _, _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type b []), `core_val (`b `float)), p))
    | _, Int _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type a []), `core_val (`b `int)), p))
    | Int _, _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type b []), `core_val (`b `int)), p))
    | _ -> assert false

  let eval_abs (e : store_val) p =
    match e with
    | Float f -> Float (Float.abs f)
    | Int i -> Int (Int.abs i)
    | _ ->
        raise
          (ERROR
             ( Wrong_type
                 (fst (assign_type e []), `core_val (`g int_or_float)),
               p ))

  let rec eval_op (exp : op) (scope : scope) (env : store) =
    let eval' e = eval_exp e scope env in
    match exp with
    | Plus (e1, e2, loc) -> eval_plus (eval' e1) (eval' e2) loc
    | Minus (e1, e2, loc) -> eval_minus (eval' e1) (eval' e2) loc
    | UMinus (e1, loc) -> eval_uminus (eval' e1) loc
    | Prod (e1, e2, loc) -> eval_prod (eval' e1) (eval' e2) loc
    | Div (e1, e2, loc) -> eval_div (eval' e1) (eval' e2) loc
    | Pow (e1, e2, loc) -> eval_pow (eval' e1) (eval' e2) loc

  and eval_fn (exp : fn) (scope : scope) (env : store) =
    let eval' e = eval_exp e scope env in
    match exp with Abs (e, loc) -> eval_abs (eval' e) loc

  and eval_exp (exp : exp) (scope : scope) (env : store) =
    match exp with
    | ENum ne -> eval_num ne scope env
    | EStr se -> eval_str se scope env
    | EVar ve -> eval_var ve scope env
    | EOp op -> eval_op op scope env
    | EFn fn -> eval_fn fn scope env

  (* Upcast exps to use floats throughout. Means labels such as 1/8 eval
     correctly *)
  (* TODO: Will not upcast variables if they are ints; needs casting
     experssions *)
  let rec upcast_to_float (exp : exp) : exp =
    let upcast_num = function
      | Num_int_val (v, p) -> Num_float_val (float_of_int v, p)
      | e -> e
    in
    let upcast_op = function
      | Plus (e1, e2, loc) ->
          Plus (upcast_to_float e1, upcast_to_float e2, loc)
      | Minus (e1, e2, loc) ->
          Minus (upcast_to_float e1, upcast_to_float e2, loc)
      | UMinus (e1, loc) -> UMinus (upcast_to_float e1, loc)
      | Prod (e1, e2, loc) ->
          Prod (upcast_to_float e1, upcast_to_float e2, loc)
      | Div (e1, e2, loc) -> Div (upcast_to_float e1, upcast_to_float e2, loc)
      | Pow (e1, e2, loc) -> Pow (upcast_to_float e1, upcast_to_float e2, loc)
    in
    let upcast_fn = function
      | Abs (e, loc) -> Abs (upcast_to_float e, loc)
    in
    match exp with
    | ENum ne -> ENum (upcast_num ne)
    | EOp op -> EOp (upcast_op op)
    | EFn fn -> EFn (upcast_fn fn)
    | _ -> exp

  let as_int (e : store_val) p =
    match e with
    | Int v -> v
    | _ ->
        raise
          (ERROR (Wrong_type (fst (assign_type e []), `core_val (`b `int)), p))

  let as_float (e : store_val) p =
    match e with
    | Float v -> v
    | Int v -> float v
    | _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type e []), `core_val (`b `float)), p))

  let as_str (e : store_val) p =
    match e with
    | Str v -> v
    | _ ->
        raise
          (ERROR
             (Wrong_type (fst (assign_type e []), `core_val (`b `string)), p))

  let cast_int (e : store_val) p = Int (as_int e p)
  let cast_float (e : store_val) p = Float (as_float e p)
  let cast_str (e : store_val) p = Str (as_str e p)

  let eval_exps exps (scope : scope) (env : store) =
    List.map
      (fun e ->
        match eval_exp e scope env with
        | Int _ as v -> (v, `b `int)
        | Float _ as v -> (v, `b `float)
        | Str _ as s -> (s, `b `string)
        | Big _
        | Big_fun (_, _)
        | Ctrl _
        | Ctrl_fun (_, _)
        | A_ctrl _
        | A_ctrl_fun (_, _)
        | React _
        | React_fun (_, _, _, _, _, _, _, _)
        | Int_param _ | Str_param _ | Float_param _ ->
            assert false)
      exps
    |> List.split

  let extend_scope (scope : scope) (forms : Id.t list)
      (nums : store_val list) (args_t : core_type list) (p : Loc.t) =
    let aux id v t =
      ScopeMap.add id { sval = v; styp = t; loc = p; used = false }
    in
    List.combine forms (List.combine nums args_t)
    |> List.fold_left (fun s (id, (v, t)) -> aux id v (`core_val t) s) scope

  let eval_ctrl_fun id nums arity_i =
    let params =
      List.map
        (function
          | Float v -> Ctrl.F v
          | Int v -> Ctrl.I v
          | Str s -> Ctrl.S s
          | Big _
          | Big_fun (_, _)
          | Ctrl _
          | Ctrl_fun (_, _)
          | A_ctrl _
          | A_ctrl_fun (_, _)
          | React _
          | React_fun (_, _, _, _, _, _, _, _)
          | Int_param _ | Float_param _ | Str_param _ ->
              assert false)
        nums
    in
    Ctrl.{s=id; p=params; i=arity_i}

  let check_atomic id p env face c = function
    | true -> if is_atomic id p env then Big.atom face c else Big.ion face c
    | false ->
        if is_atomic id p env then raise (ERROR (Atomic_ctrl id, p))
        else Big.ion face c

  (* Checking for duplicates. *)
  let parse_face ns p =
    let f = Link.parse_face ns in
    if List.length ns <> Link.Face.cardinal f then
      raise (ERROR (Not_face ns, p))
    else f

  (* flag=true Atomic controls allowed *)
  let eval_ion scope (env : store) env_t flag = function
    | Big_ion_exp (id, names, p) ->
        let c = get_ctrl id (List.length names) p env
        and face = parse_face names p in
        (check_atomic id p env face c flag, env_t)
    | Big_ion_fun_exp (id, args, names, p) -> (
        let exps, args_t = eval_exps args scope env
        and face = parse_face names p in
        let a, _, t = get_ctrl_fun id (List.length names) args_t p env in
        try
          let env_t' = app_exn env_t (dom_of_lambda t) args_t in
          let c = eval_ctrl_fun id exps a in
          (check_atomic id p env face c flag, env_t')
        with UNIFICATION ->
          raise
            (ERROR
               (Wrong_type (`lambda (args_t, `ctrl a), resolve_t env_t t), p))
        )

  let rec eval_big (exp : big_exp) (scope : scope) (env : store)
      (env_t : store_t) =
    let binary_eval l r scope env env_t f =
      let l_v, env_t' = eval_big l scope env env_t in
      let r_v, env_t'' = eval_big r scope env env_t' in
      (f l_v r_v, env_t'')
    in
    match exp with
    | Big_var (id, p) -> (get_big id p env, env_t)
    | Big_var_fun (id, args, p) -> (
        (* fun b(x,y) = 1; fun big a(x,y) = b(x + y, 5); a(3, 2 + 3.4); *)
        let exps, args_t =
          (* id = a --> ([Int 3; Float 5], [`b `int; `b `float]) *)
          eval_exps args scope env
        in
        (* (exp, [x; y], [`g 0; `g 1]) *)
        let exp, forms, t = get_big_fun id args_t p env in
        try
          (* Unification: `g 0 -> `b `int ; `g 1 -> `b `float *)
          let env_t' = app_exn env_t (dom_of_lambda t) args_t in
          (* Extend scope: x -> Int 3 y -> Float 5 *)
          let scope' = extend_scope scope forms exps args_t p in
          eval_big exp scope' env env_t'
        with UNIFICATION ->
          raise
            (ERROR (Wrong_type (`lambda (args_t, `big), resolve_t env_t t), p))
        )
    | Big_new_name (n, _) ->
        (Big.intro (Link.Face.singleton (Link.Name n)), env_t)
    | Big_comp (l, r, p) -> (
        try binary_eval l r scope env env_t Big.comp
        with Big.COMP_ERROR (i, j) -> raise (ERROR (Comp (i, j), p)))
    | Big_tens (l, r, p) -> (
        try binary_eval l r scope env env_t Big.tens
        with Link.NAMES_ALREADY_DEFINED (i, o) ->
          raise (ERROR (Tens (i, o), p)))
    | Big_par (l, r, _) -> binary_eval l r scope env env_t Big.par
    | Big_ppar (l, r, _) -> binary_eval l r scope env env_t Big.ppar
    | Big_share (a, psi, b, p) -> (
        try
          let a_v, env_t' = eval_big a scope env env_t in
          let psi_v, env_t'' = eval_big psi scope env env_t' in
          let b_v, env_t''' = eval_big b scope env env_t'' in
          (Big.share a_v psi_v b_v, env_t''')
        with
        | Big.COMP_ERROR (i, j) -> raise (ERROR (Comp (i, j), p))
        | Big.SHARING_ERROR -> raise (ERROR (Share, loc_of_big_exp psi)))
    | Big_num (v, p) -> (
        match v with
        | 0 -> (Big.zero, env_t)
        | 1 -> (Big.one, env_t)
        | _ -> raise (ERROR (Unknown_big v, p)))
    | Big_id exp ->
        ( Big.id (Big.Inter (exp.id_place, parse_face exp.id_link exp.id_loc)),
          env_t )
    | Big_merge (n, _) -> (Big.merge n, env_t)
    | Big_split (n, _) -> (Big.split n, env_t)
    | Big_plc exp ->
        (Big.placing exp.plc_parents exp.plc_roots Link.Face.empty, env_t)
    | Big_nest (ion, b, p) -> (
        try
          (* No atomic controls allowed here *)
          let i_v, env_t' = eval_ion scope env env_t false ion in
          let b_v, env_t'' = eval_big b scope env env_t' in
          (Big.nest i_v b_v, env_t'')
        with Big.COMP_ERROR (i, j) -> raise (ERROR (Comp (i, j), p)))
    | Big_ion ion -> eval_ion scope env env_t true ion
    | Big_close exp -> (Big.closure (Link.parse_face [ exp.cl_name ]), env_t)
    | Big_sub exp ->
        ( Big.sub
            ~inner:(parse_face exp.in_names exp.sub_loc)
            ~outer:(Link.parse_face [ exp.out_name ]),
          env_t )
    | Big_wire (c, b, _) -> (
        let b_v, env_t' = eval_big b scope env env_t in
        match c with
        | Close_exp cs ->
            (Big.close (Link.parse_face (names_of_closures cs)) b_v, env_t')
        | Sub_exp s ->
            ( Big.rename
                ~inner:(parse_face s.in_names s.sub_loc)
                ~outer:(Link.parse_face [ s.out_name ])
                b_v,
              env_t' )
        | Merge_close_exp cs ->
            let outer = Link.parse_face [ "~0" ] in
            ( Big.rename ~inner:(Link.parse_face cs.m_cl_names) ~outer b_v
              |> Big.close outer,
              env_t' ))
    | Big_par_fn (n, b, p) ->
        let n' = as_int (eval_exp n scope env) p in
        if n' < 0 then raise (ERROR (Invalid_iter_op n', p))
        else
          let b_v, env_t' = eval_big b scope env env_t in
          (Big.par_seq ~start:0 ~stop:n' (fun _ -> b_v), env_t')
    | Big_ppar_fn (n, b, p) ->
        let n' = as_int (eval_exp n scope env) p in
        if n' < 0 then raise (ERROR (Invalid_iter_op n', p))
        else
          let b_v, env_t' = eval_big b scope env env_t in
          (Big.ppar_seq ~start:0 ~stop:n' (fun _ -> b_v), env_t')

  let eval_eta = function Some (l, _) -> Some (Fun.parse l) | None -> None

  let eval_conds conds scope env env_t =
    let eval_cond (acc, e) c =
      (* TODO need to accumulate env_t? *)
      let p, e' = eval_big c.pred scope env e in
      let w =
        match c.place with
        | Cond_Ctx -> AppCond.Ctx
        | Cond_Param -> AppCond.Param
      in
      ({ AppCond.neg = c.neg; AppCond.pred = p; AppCond.where = w } :: acc, e')
    in
    List.fold_left eval_cond ([], env_t) conds

  let eval_reward env loc = function
    | None -> 0
    | Some rew -> as_int (eval_exp rew ScopeMap.empty env) loc

  (* Similar to binary eval *)
  let eval_react_aux lhs rhs conds scope env env_t =
    let lhs_v, env_t' = eval_big lhs scope env env_t in
    let rhs_v, env_t'' = eval_big rhs scope env env_t' in
    let conds', env_t''' = eval_conds conds scope env env_t'' in
    (lhs_v, rhs_v, conds', env_t''')

  let eval_react name action reward lhs rhs eta conds l scope env env_t p =
    let lhs_v, rhs_v, conds', env_t' =
      eval_react_aux lhs rhs conds scope env env_t
    in
    let lblf f_exp = as_float (eval_exp (upcast_to_float f_exp) scope env) in
    match
      P.parse_react name lhs_v rhs_v ~conds:conds'
        (match l with
        | Some f_exp ->
            if String.equal action "" then `F (lblf f_exp p)
            else `P (action, reward, lblf f_exp p)
        | None -> `E ())
        eta p
    with
    | Error e -> raise (ERROR (Reaction e, p))
    | Ok r -> (r, env_t')

  (* Compute all the combinations of input values *)
  let rec param_comb (pars : typed_store_val list list) =
    match pars with
    | [ x ] -> List.map (fun v -> [ v ]) x
    | x :: xs ->
        let aux1 v ls = List.map (fun l -> v :: l) ls in
        let rec aux2 l ls =
          match l with [] -> [] | x :: xs -> aux1 x ls @ aux2 xs ls
        in
        aux2 x (param_comb xs)
    | [] -> []

  let param_to_vals = function
    | { sval = Int_param vals; loc = p; _ } ->
        List.map
          (fun v ->
            {
              sval = Int v;
              styp = `core_val (`b `int);
              loc = p;
              used = false;
            })
          vals
    | { sval = Float_param vals; loc = p; _ } ->
        List.map
          (fun v ->
            {
              sval = Float v;
              styp = `core_val (`b `float);
              loc = p;
              used = false;
            })
          vals
    | { sval = Str_param vals; loc = p; _ } ->
        List.map
          (fun v ->
            {
              sval = Str v;
              styp = `core_val (`b `string);
              loc = p;
              used = false;
            })
          vals
    | _ -> assert false

  let is_param id (env : store) p =
    match Base.H_string.find_opt env id with
    | None -> raise (ERROR (Unbound_variable id, p))
    | Some v -> (
        match v.sval with
        | Int_param _ | Str_param _ | Float_param _ -> true
        | Int _ | Float _ | Str _ | Big _
        | Big_fun (_, _)
        | Ctrl _
        | Ctrl_fun (_, _)
        | A_ctrl _
        | A_ctrl_fun (_, _)
        | React _
        | React_fun (_, _, _, _, _, _, _, _) ->
            false)

  module IdSet = Set.Make (struct
    type t = Id.t

    let compare = Id.compare
  end)

  (* [a + 3.0; c] -> [a , c] *)
  let scan_for_params env args =
    let auxV acc = function
      | Var (id, p) -> if is_param id env p then IdSet.add id acc else acc
    in
    let aux acc = function EVar v -> auxV acc v | _ -> acc in
    List.fold_left aux IdSet.empty args |> IdSet.elements

  (* a = [1.0; 2.0] c = [4; 8] [(a = 1.0; c = 4); (a = 1.0; c = 8); (a = 2.0;
     c = 4); (a = 2.0; c = 8)] *)
  let param_scopes (env : store) = function
    | [] -> [ ScopeMap.empty ]
    | ids ->
        List.map
          (fun id -> param_to_vals (Base.safe @@ Base.H_string.find_opt env id))
          ids
        |> param_comb
        |> List.map (fun comb ->
               List.fold_left2
                 (fun acc id v -> ScopeMap.add id v acc)
                 ScopeMap.empty ids comb)

  let eval_react_fun_app id args env env_t p =
    scan_for_params env args |> param_scopes env
    |> List.fold_left
         (fun (acc, env_t) scope ->
           let exps, args_t = eval_exps args scope env in
           let action, reward, l, r, label, eta, conds, forms, t =
             get_react_fun id args_t p env
           in
           try
             let env_t' = app_exn env_t (dom_of_lambda t) args_t in
             let scope' = extend_scope scope forms exps args_t p in
             let full_name =
               Printf.sprintf "%s(%s)" id
                 (List.map string_of_store_val exps |> String.concat ",")
             in
             let r, env_t'' =
               eval_react full_name action reward l r eta conds label scope'
                 env env_t' p
             in
             (r :: acc, env_t'')
           with UNIFICATION ->
             raise
               (ERROR
                  ( Wrong_type (`lambda (args_t, `react), resolve_t env_t t),
                    p )))
         ([], env_t)

  let eval_pr env env_t pr =
    let aux env_t = function
      | Rul_id (id, p) -> ([ get_react id p env ], env_t)
      | Rul_id_fun (id, args, p) -> eval_react_fun_app id args env env_t p
    in
    let aux' (acc, env_t) id =
      let rs, env_t' = aux env_t id in
      (acc @ rs, env_t')
    in
    let pr_class, p =
      match pr with
      | Pr (ids, p) ->
          let rs, env_t' = List.fold_left aux' ([], env_t) ids in
          ((T.P_class rs, env_t'), p)
      | Pr_red (ids, p) ->
          let rs, env_t' = List.fold_left aux' ([], env_t) ids in
          ((T.P_rclass rs, env_t'), p)
    in
    if T.is_valid_priority (fst pr_class) then pr_class
    else raise (ERROR (Invalid_class, p))

  let eval_p_list eval_f chk_f env env_t l p =
    ( List.fold_left
        (fun (acc, env_t) pr ->
          let pr_class, env_t' = eval_f env env_t pr in
          (pr_class :: acc, env_t'))
        ([], env_t) l
    |> fun (l, e) -> (List.rev l, e) )
    |> fun x -> if chk_f x then x else raise (ERROR (Invalid_priorities, p))

  let eval_prs =
    let chk_fn (pl, _) =
      match pl with [] -> true | ps -> T.is_valid_priority_list ps
    in
    eval_p_list eval_pr chk_fn

  let eval_pred_fun_app id args env env_t p =
    let print_id id exps =
      let exp_s =
        List.map
          (function
            | (Int _ | Float _ | Str _) as v -> string_of_store_val v
            | _ -> assert false)
          exps
      in
      id ^ "(" ^ String.concat "," exp_s ^ ")"
    in
    scan_for_params env args |> param_scopes env
    |> List.fold_left
         (fun (acc, env_t) scope ->
           let exps, args_t = eval_exps args scope env in
           let exp, forms, t = get_big_fun id args_t p env in
           try
             let env_t' = app_exn env_t (dom_of_lambda t) args_t in
             let scope' = extend_scope scope forms exps args_t p in
             let b, env_t'' = eval_big exp scope' env env_t' in
             ((print_id id exps, b) :: acc, env_t'')
           with UNIFICATION ->
             raise
               (ERROR
                  (Wrong_type (`lambda (args_t, `big), resolve_t env_t t), p)))
         ([], env_t)

  let eval_preds env env_t preds =
    let aux env_t = function
      | Pred_id (id, reward, p) ->
          ([ ((id, eval_reward env p reward), get_big id p env) ], env_t)
      | Pred_id_fun (id, args, reward, p) ->
          let reward = eval_reward env p reward in
          let ps, env_t = eval_pred_fun_app id args env env_t p in
          (List.map (fun (id, bigraph) -> ((id, reward), bigraph)) ps, env_t)
    in
    let aux' (acc, env_t) id =
      let ps, env_t' = aux env_t id in
      (acc @ ps, env_t')
    in
    List.fold_left aux' ([], env_t) preds

  let eval_init exp env env_t =
    let read_from_file file =
      let ch = open_in file in
      let s =
        really_input_string ch (in_channel_length ch) |> Big.of_string
      in
      close_in ch;
      ((s, []), Loc.dummy_loc)
    in
    let read_from_model =
      match exp with
      | Init (id, p) ->
          (eval_big (Big_var (id, p)) ScopeMap.empty env env_t, p)
      | Init_fun (id, args, p) ->
          (eval_big (Big_var_fun (id, args, p)) ScopeMap.empty env env_t, p)
    in
    let (b, store), p =
      match Cmd.defaults.initfile with
      | Some f ->
          (* TODO: Generalise info log output *)
          if not Cmd.defaults.quiet then
            fprintf err_formatter
              "@[<v>@[Info: Reading initial bigraph from file: %s@]@." f;
          read_from_file f
      | None -> read_from_model
    in
    if Big.is_ground b then (b, store)
    else raise (ERROR (Init_not_ground, p))

  (******** ADD TO STORE FUNCTIONS *********)

  let add_to_store c (env : store) id (v : typed_store_val) =
    (match Base.H_string.find_opt env id with
    | Some x -> report_warning c (Multiple_declaration (id, x.loc, v.loc))
    | None -> ());
    Base.H_string.replace env id v

  let update c id (v : store_val) p env env_t =
    let t, env_t' = assign_type v env_t in
    add_to_store c env id { sval = v; styp = t; loc = p; used = false };
    env_t'

  let get_action (rid : Id.t) (acts : daction list) env =
    match List.find_opt (fun act -> List.mem rid act.action_rules) acts with
    | None -> ("", 0)
    | Some a -> (a.action_id, eval_reward env Loc.dummy_loc a.action_reward)

  let store_decs c decs acts env env_t =
    let aux env_t d =
      let unpack_cnds = function Some cnds -> cnds | None -> [] in
      let upd id v p = update c id v p env env_t in
      match d with
      | Dctrl (Atomic (Ctrl_exp (id, ar, _), p)) ->
          upd id (A_ctrl (Ctrl.{s=id; p=[]; i=ar})) p
      | Dctrl (Atomic (Ctrl_fun_exp (id, forms, ar, _), p)) ->
          upd id (A_ctrl_fun (ar, forms)) p
      | Dctrl (Non_atomic (Ctrl_exp (id, ar, _), p)) ->
          upd id (Ctrl (Ctrl.{s=id; p=[]; i=ar})) p
      | Dctrl (Non_atomic (Ctrl_fun_exp (id, forms, ar, _), p)) ->
          upd id (Ctrl_fun (ar, forms)) p
      | Dint (d, p) ->
          upd d.d_id
            (cast_int (eval_exp d.d_exp ScopeMap.empty env) d.d_loc)
            p
      | Dfloat (d, p) ->
          upd d.d_id
            (cast_float (eval_exp d.d_exp ScopeMap.empty env) d.d_loc)
            p
      | Dstr (d, p) ->
          upd d.d_id
            (cast_str (eval_exp d.d_exp ScopeMap.empty env) d.d_loc)
            p
      | Dbig (Big_exp (id, exp, p)) ->
          let b_v, env_t' = eval_big exp ScopeMap.empty env env_t in
          update c id (Big b_v) p env env_t'
      | Dbig (Big_fun_exp (id, forms, exp, p)) ->
          upd id (Big_fun (exp, forms)) p
      | Dreact (React_exp (id, _, _, lhs, rhs, label, eta, conds, p)) ->
          let r_v, env_t' =
            let a_id, a_rew = get_action id acts env in
            eval_react id a_id a_rew lhs rhs (eval_eta eta)
              (unpack_cnds conds) label ScopeMap.empty env env_t p
          in
          update c id (React r_v) p env env_t'
      | Dreact
          (React_fun_exp (id, _, _, forms, lhs, rhs, label, eta, conds, p))
        ->
          let a_id, a_rew = get_action id acts env in
          upd id
            (React_fun
               ( a_id,
                 a_rew,
                 lhs,
                 rhs,
                 eval_eta eta,
                 unpack_cnds conds,
                 label,
                 forms ))
            p
    in
    List.fold_left aux env_t decs

  let store_consts c consts (env : store) =
    let aux = function
      | Cint d ->
          let v = cast_int (eval_exp d.d_exp ScopeMap.empty env) d.d_loc in
          ignore (update c d.d_id v d.d_loc env [])
      | Cfloat d ->
          let v = cast_float (eval_exp d.d_exp ScopeMap.empty env) d.d_loc in
          ignore (update c d.d_id v d.d_loc env [])
      | Cstr s ->
          let v = cast_str (eval_exp s.d_exp ScopeMap.empty env) s.d_loc in
          ignore (update c s.d_id v s.d_loc env [])
    in
    List.iter aux consts

  (* Store simple Ints or Floats when the list of values has only one
     element *)
  let store_params c (params : param_exp list) env =
    let rec eval_int_range start incr stop acc =
      let start' = start + incr in
      if start' <= stop then eval_int_range start' incr stop (start' :: acc)
      else acc
    in
    let rec eval_float_range start incr stop acc =
      let start' = start +. incr in
      if start' <= stop then eval_float_range start' incr stop (start' :: acc)
      else acc
    in
    let flatten_int = function [ v ] -> Int v | v -> Int_param v
    and flatten_float = function [ v ] -> Float v | v -> Float_param v
    and flatten_str = function [ v ] -> Str v | v -> Str_param v in
    let create_tval v t l = { sval = v; styp = t; loc = l; used = false } in
    (* let eval_int_param exp = *)
    (* try Int (eval_int exp ScopeMap.empty env) with *)
    (* | ERROR (Wrong_type _, _) -> get_int_param *)
    let aux = function
      | Param_int (ids, Param_int_val (exp, _), p) ->
          let v = cast_int (eval_exp exp ScopeMap.empty env) p in
          List.iter
            (fun id ->
              add_to_store c env id
              @@ create_tval v (fst (assign_type v [])) p)
            ids
      | Param_int (ids, Param_int_range (start, incr, stop, _), p) ->
          let s = as_int (eval_exp start ScopeMap.empty env) p in
          let v =
            flatten_int
              (eval_int_range s
                 (as_int (eval_exp incr ScopeMap.empty env) p)
                 (as_int (eval_exp stop ScopeMap.empty env) p)
                 [ s ])
          in
          List.iter
            (fun id ->
              add_to_store c env id
              @@ create_tval v (fst (assign_type v [])) p)
            ids
      | Param_int (ids, Param_int_set (exps, _), p) ->
          let v =
            List.map (fun e -> as_int (eval_exp e ScopeMap.empty env) p) exps
            |> List.sort_uniq (fun a b -> a - b)
            |> flatten_int
          in
          List.iter
            (fun id ->
              add_to_store c env id
              @@ create_tval v (fst (assign_type v [])) p)
            ids
      | Param_float (ids, Param_float_val (exp, _), p) ->
          let v = cast_float (eval_exp exp ScopeMap.empty env) p in
          List.iter
            (fun id ->
              add_to_store c env id
              @@ create_tval v (fst (assign_type v [])) p)
            ids
      | Param_float (ids, Param_float_range (start, incr, stop, _), p) ->
          let s = as_float (eval_exp start ScopeMap.empty env) p in
          let v =
            flatten_float
              (eval_float_range s
                 (as_float (eval_exp incr ScopeMap.empty env) p)
                 (as_float (eval_exp stop ScopeMap.empty env) p)
                 [ s ])
          in
          List.iter
            (fun id ->
              add_to_store c env id
              @@ create_tval v (fst (assign_type v [])) p)
            ids
      | Param_float (ids, Param_float_set (exps, _), p) ->
          let v =
            List.map
              (fun e -> as_float (eval_exp e ScopeMap.empty env) p)
              exps
            |> List.sort_uniq compare |> flatten_float
          in
          List.iter
            (fun id ->
              add_to_store c env id
              @@ create_tval v (fst (assign_type v [])) p)
            ids
      | Param_str (ids, Param_str_val (exp, _), p) ->
          let s = cast_str (eval_exp exp ScopeMap.empty env) p in
          List.iter
            (fun id ->
              add_to_store c env id
              @@ create_tval s (fst (assign_type s [])) p)
            ids
      | Param_str (ids, Param_str_set (exps, _), p) ->
          let v =
            List.map (fun e -> as_str (eval_exp e ScopeMap.empty env) p) exps
            |> List.sort_uniq String.compare
            |> flatten_str
          in
          List.iter
            (fun id ->
              add_to_store c env id
              @@ create_tval v (fst (assign_type v [])) p)
            ids
    in
    List.iter aux params

  (******** INSTANTIATE REACTIVE SYSTEM *********)

  let init_env c consts =
    let store = Base.H_string.create 1000 in
    store_consts c consts store;
    store

  let eval_model c m env =
    let env_t = store_decs c (fst m.model_decs) (fst m.model_rs.dbrs_actions) env [] in
    store_params c (fst m.model_rs.dbrs_params) env;
    let b, env_t' = eval_init (fst m.model_rs.dbrs_init) env env_t in
    let p, env_t'' =
      eval_prs env env_t' (fst m.model_rs.dbrs_pri) m.model_rs.dbrs_loc
    in
    let preds, env_t''' = eval_preds env env_t'' (fst m.model_rs.dbrs_preds) in
    (b, p, preds, env_t''')

  (******** EXPORT STORE *********)

  let export decs (env : store) (env_t : store_t) path formats fmt c
      (print_fun : string -> int -> unit) =
    let concat = Filename.concat path in
    let dummy_args (args_t : core_type list) =
      resolve_types env_t args_t |> List.map def_val
    in
    let aux id =
      let args_t =
        Base.safe @@ Base.H_string.find_opt env id |> get_type |> dom_of_lambda
      in
      dummy_args args_t
    in
    let aux' eval_f id args p =
      eval_f id args env env_t p |> fst |> List.hd
    in

    let write f_write r_write ext = function
      | Dctrl _ | Dint _ | Dstr _ | Dfloat _ -> ()
      | Dbig (Big_exp (id, _, p)) ->
          f_write (get_big id p env) ~name:(id ^ ext) ~path
          |> print_fun (concat (id ^ ext))
      | Dbig (Big_fun_exp (id, _, _, p)) ->
          let args = aux id in
          let b =
            fst
              (eval_big (Big_var_fun (id, args, p)) ScopeMap.empty env env_t)
          in
          f_write b ~name:(id ^ ext) ~path |> print_fun (concat (id ^ ext))
      | Dreact (React_exp (id, _, _, _, _, _, _, _, p)) ->
          let r = get_react id p env in
          r_write r ~name:(id ^ ext) ~path |> print_fun (concat (id ^ ext))
      | Dreact (React_fun_exp (id, _, _, _, _, _, _, _, _, p)) ->
          let args = aux id in
          let r = aux' eval_react_fun_app id args p in
          r_write r ~name:(id ^ ext) ~path |> print_fun (concat (id ^ ext))
    in
    List.iter
      (fun (f_write, r_write, ext) ->
        List.iter
          (fun d ->
            try write f_write r_write ext d
            with Failure msg ->
              pp_print_flush fmt ();
              fprintf err_formatter "@[<v>@[%s%s@]@." (Utils.err_opt c) msg)
          decs)
      formats

  (** Warning based on the store *)
  let warn_unused env c =
    let warn typ n p =
      fprintf err_formatter "%a@[%s%s %s is defined but not used@]@."
        Loc.print_loc p (Utils.warn_opt c) typ n
    in
    let check_unused id v =
      match v with
      | { sval = React _; loc = p; used = false; _ }
      | { sval = React_fun _; loc = p; used = false; _ } ->
          warn "react" id p
      | { sval = A_ctrl _; loc = p; used = false; _ }
      | { sval = Ctrl _; loc = p; used = false; _ } ->
          warn "ctrl" id p
      | { sval = Big _; loc = p; used = false; _ }
      | { sval = Big_fun _; loc = p; used = false; _ } ->
          warn "big" id p
      | _ -> ()
    in
    Base.H_string.iter check_unused env
end
