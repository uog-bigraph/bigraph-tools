open Ast
open Format

let pp_strings =
  pp_print_list
    ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
    (fun fmt n -> fprintf fmt "%s" n)

let pp_intsets =
  pp_print_list
    ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
    (fun fmt s ->
      fprintf fmt "@[<hov 1>{%a}@]"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
           (fun fmt n -> fprintf fmt "%d" n))
        s)

let pp_ctrl fmt = function
  | Ctrl_exp (id, n, _) -> fprintf fmt "ctrl %s =@ %d" id n
  | Ctrl_fun_exp (id, args, n, _) ->
      fprintf fmt "fun ctrl @[<h 2>%s@,@[<hov 1>(%a)@]@] =@ %d" id pp_strings
        args n

let pp_comma fmt () = fprintf fmt ",@ "

let is_atom_exp = function
  | ENum _ | EStr _ | EVar _ | EFn _ -> true
  | EOp _ -> false

let is_same_assoc_exp p_exp exp =
  match (p_exp, exp) with
  | EOp (Plus _), EOp (Plus _)
  | EOp (Plus _), EOp (Minus _)
  | EOp (Minus _), EOp (Minus _)
  | EOp (Minus _), EOp (Plus _)
  | EOp (Prod _), EOp (Prod _) ->
      true
  | _ -> false

let rec pp_exp fmt = function
  | ENum (Num_int_val (v, _)) -> fprintf fmt "%d" v
  | ENum (Num_float_val (v, _)) when v = infinity -> fprintf fmt "inf"
  | ENum (Num_float_val (v, _)) -> fprintf fmt "%F" v
  | EStr (Str_val (v, _)) | EVar (Var (v, _)) -> fprintf fmt "%s" v
  | EOp (Plus (a, b, _)) as p_exp ->
      fprintf fmt "@[<hov 2>%a +@ %a@]" (pp_exp_brackets p_exp) a
        (pp_exp_brackets p_exp) b
  | EOp (Minus (a, b, _)) as p_exp ->
      fprintf fmt "@[<hov 2>%a -@ %a@]" (pp_exp_brackets p_exp) a
        (pp_exp_brackets p_exp) b
  | EOp (UMinus (exp, _)) as p_exp ->
      fprintf fmt "-%a" (pp_exp_brackets p_exp) exp
  | EOp (Prod (a, b, _)) as p_exp ->
      fprintf fmt "@[<hov 2>%a *@ %a@]" (pp_exp_brackets p_exp) a
        (pp_exp_brackets p_exp) b
  | EOp (Div (a, b, _)) as p_exp ->
      fprintf fmt "@[<hov 2>%a /@ %a@]" (pp_exp_brackets p_exp) a
        (pp_exp_brackets p_exp) b
  | EOp (Pow (a, b, _)) as p_exp ->
      fprintf fmt "@[<hov 2>%a ^@ %a@]" (pp_exp_brackets p_exp) a
        (pp_exp_brackets p_exp) b
  | EFn (Abs (exp, _)) -> fprintf fmt "abs(%a)" pp_exp exp

and pp_exp_brackets p_exp fmt exp =
  if is_atom_exp exp || is_same_assoc_exp p_exp exp then
    fprintf fmt "%a" pp_exp exp
  else fprintf fmt "@,@[<hov 0>(%a)@]" pp_exp exp

let is_atom_bexp = function
  | Big_var _ | Big_var_fun _ | Big_new_name _ | Big_num _ | Big_id _
  | Big_merge _ | Big_split _ | Big_par_fn _ | Big_ppar_fn _ | Big_close _
  | Big_sub _ | Big_plc _ | Big_ion _ |  Big_nest _
    -> true
  | Big_comp _ | Big_tens _ | Big_par _ | Big_ppar _ | Big_share _
  | Big_wire _
    -> false

let is_same_assoc_bexp p_bexp bexp =
  match (p_bexp, bexp) with
  | Big_comp _, Big_comp _
  | Big_tens _, Big_tens _
  | Big_par _, Big_par _
  | Big_ppar _, Big_ppar _ ->
      true
  | _ -> false

let pp_ion_exp fmt = function
  | Big_ion_exp (ctrl, [], _) -> fprintf fmt "%s" ctrl
  | Big_ion_fun_exp (ctrl, args, [], _) ->
      fprintf fmt "@[<hov 2>%s@,@[<hv 1>(%a)@]@]" ctrl
        (pp_print_list ~pp_sep:pp_comma pp_exp)
        args
  | Big_ion_exp (ctrl, names, _) ->
      fprintf fmt "%s@,@[<hov 1>{%a}@]" ctrl pp_strings names
  | Big_ion_fun_exp (ctrl, args, names, _) ->
      fprintf fmt "%s@,@[<hv 1>(%a)@]@,@[<hov 1>{%a}@]" ctrl
        (pp_print_list ~pp_sep:pp_comma pp_exp)
        args pp_strings names

let rec pp_bexp fmt = function
  | Big_var (id, _) -> fprintf fmt "%s" id
  | Big_var_fun (id, args, _) ->
      fprintf fmt "@[<h 2>%s@,@[<hv 1>(%a)@]@]" id
        (pp_print_list ~pp_sep:pp_comma pp_exp)
        args
  | Big_new_name (id, _) -> fprintf fmt "{%s}" id
  | Big_comp (a, b, _) as p_bexp ->
      fprintf fmt "@[<hv 0>%a@;* %a@]" (pp_bexp_brackets p_bexp) a
        (pp_bexp_brackets p_bexp) b
  | Big_tens (a, b, _) as p_bexp ->
      fprintf fmt "@[<hv 0>%a@;+ %a@]" (pp_bexp_brackets p_bexp) a
        (pp_bexp_brackets p_bexp) b
  | Big_par (a, b, _) as p_bexp ->
      fprintf fmt "@[<hv 0>%a@;| %a@]" (pp_bexp_brackets p_bexp) a
        (pp_bexp_brackets p_bexp) b
  | Big_ppar (a, b, _) as p_bexp ->
      fprintf fmt "@[<hv 0>%a@;|| %a@]" (pp_bexp_brackets p_bexp) a
        (pp_bexp_brackets p_bexp) b
  | Big_share (a, b, c, _) as p_bexp ->
      fprintf fmt "@[<hv 0>share %a@;by %a@;in %a@]"
        (pp_bexp_brackets p_bexp) a pp_bexp b (pp_bexp_brackets p_bexp) c
  | Big_num (x, _) -> fprintf fmt "%d" x
  | Big_id { id_place = 1; id_link = []; _ } -> fprintf fmt "id"
  | Big_id { id_place = 0; id_link; _ } ->
      fprintf fmt "@[<h 2>id{@[<hov 2>%a@]}@]" pp_strings id_link
  | Big_id { id_place; id_link = []; _ } ->
      fprintf fmt "id(%d)" id_place
  | Big_id { id_place; id_link; _ } ->
      fprintf fmt "@[<h 2>id@[<hov 1>(%d,@;@[<hov 1>{%a}@])@]@]" id_place
        pp_strings id_link
  | Big_merge (2, _) -> fprintf fmt "merge"
  | Big_merge (n, _) -> fprintf fmt "merge(%d)" n
  | Big_split (2, _) -> fprintf fmt "split"
  | Big_split (n, _) -> fprintf fmt "split(%d)" n
  | Big_plc { plc_parents; plc_roots; _ } ->
      fprintf fmt "(@[<hv 0>@[<hov 2>[ %a ]@],@ %d)@]" pp_intsets plc_parents
        plc_roots
  | Big_nest (a, b, _) as p_bexp ->
      fprintf fmt "@[<hov 2>%a.@,%a@]" pp_ion_exp a (pp_bexp_brackets p_bexp)
        b
  | Big_ion exp -> fprintf fmt "@[<hov 2>%a@]" pp_ion_exp exp
  | Big_close { cl_name; _ } -> fprintf fmt "/%s" cl_name
  | Big_sub { out_name; in_names; _ } ->
      fprintf fmt "@[<hov 2>%s/{@[<hov 1>%a@]}@]" out_name pp_strings
        in_names
  | Big_wire (Close_exp closures, bexp, _) as p_bexp ->
      fprintf fmt "@[<hov 2>@[<hov 0>%a@]@;%a@]"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt "@ ")
           (fun fmt { cl_name; _ } -> fprintf fmt "/%s" cl_name))
        closures (pp_bexp_brackets p_bexp) bexp
  | Big_wire (Merge_close_exp { m_cl_names; _ }, bexp, _) as p_bexp ->
      fprintf fmt "@[<hov 2>/{@[<hov 1>%a@]}@;%a@]" pp_strings m_cl_names
        (pp_bexp_brackets p_bexp) bexp
  | Big_wire (Sub_exp { out_name; in_names; _ }, bexp, _) as p_bexp ->
      fprintf fmt "@[<hov 2>%s/{@[<hov 1>%a@]}@;%a@]" out_name pp_strings
        in_names (pp_bexp_brackets p_bexp) bexp
  | Big_par_fn (exp, bexp, _) ->
      fprintf fmt "par(@[<hov 1>%a,@;%a@])" pp_exp exp pp_bexp bexp
  | Big_ppar_fn (exp, bexp, _) ->
      fprintf fmt "ppar(@[<hov 1>%a,@;%a@])" pp_exp exp pp_bexp bexp

and pp_bexp_brackets p_bexp fmt bexp =
  if is_atom_bexp bexp || is_same_assoc_bexp p_bexp bexp then
    fprintf fmt "%a" pp_bexp bexp
  else fprintf fmt "@[<hov 1>(%a)@]" pp_bexp bexp

let pp_label fmt = function
  | None -> fprintf fmt "-->"
  | Some exp -> fprintf fmt "-[ @[<hov 2>%a@] ]->" pp_exp exp

let pp_inst fmt = function
  | None -> ()
  | Some (eta, _) ->
      fprintf fmt "@;@[<hov 2>%@ [ %a ]@]"
        (pp_print_list
           ~pp_sep:pp_comma
           (fun fmt n -> fprintf fmt "%d" n))
        eta

let pp_conds fmt conds =
  let pp_cond fmt { neg; pred; place; _ } =
    fprintf fmt "@[<hv 0>@[<hov 2>%a%a@]@;in %a@]"
      (fun fmt neg ->
         if neg then fprintf fmt "!" else ())
      neg pp_bexp pred
      (fun fmt -> function
        | Cond_Ctx -> fprintf fmt "ctx"
        | Cond_Param -> fprintf fmt "param")
      place
  in
  match conds with
  | Some c ->
      fprintf fmt "@;@[<hov 2>if@;@[<hv 2>%a@]@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@;") pp_cond)
        c
  | None -> ()

let pp_dec fmt = function
  | Dctrl (Atomic (exp, _)) ->
      fprintf fmt "@[<hov 2>atomic %a;@]" pp_ctrl exp
  | Dctrl (Non_atomic (exp, _)) -> fprintf fmt "@[<hov 2>%a;@]" pp_ctrl exp
  | Dint ({ d_id; d_exp; _ }, _) ->
      fprintf fmt "@[<hov 2>int %s =@;@[<hov 2>%a;@]@]" d_id pp_exp d_exp
  | Dfloat ({ d_id; d_exp; _ }, _) ->
      fprintf fmt "@[<hov 2>float %s =@;@[<hov 2>%a;@]@]" d_id pp_exp d_exp
  | Dstr ({ d_id; d_exp; _ }, _) ->
      fprintf fmt "@[<hov 2>string %s =@;@[<hov 2>%a;@]@]" d_id pp_exp d_exp
  | Dbig (Big_exp (id, exp, _)) ->
      fprintf fmt "@[<hov 2>big %s =@;@[<hov 2>%a;@]@]" id pp_bexp exp
  | Dbig (Big_fun_exp (id, args, exp, _)) ->
      fprintf fmt
        "@[<hov 2>fun big @[<h 2>%s(@[<hov 0>%a@])@] =@;@[<hov 2>%a;@]@]" id
        (pp_print_list
           ~pp_sep:pp_comma
           (fun fmt arg -> fprintf fmt "%s" arg))
        args pp_bexp exp
  | Dreact (React_exp (id, _, _, lhs, rhs, label, inst, conds, _)) ->
      fprintf fmt "@[<hov 2>react %s =@;@[<hv 0>%a@;%a@;%a%a%a;@]@]" id
        pp_bexp lhs pp_label label pp_bexp rhs pp_inst inst pp_conds conds
  | Dreact (React_fun_exp (id, _, _, args, lhs, rhs, label, inst, conds, _))
    ->
      fprintf fmt
        "@[<hov 2>fun react @[<h 2>%s(@[<hov 0>%a@])@] =@;\
         @[<hv 0>%a@;\
         %a@;\
         %a%a%a;@]@]"
        id pp_strings args pp_bexp lhs pp_label label pp_bexp rhs pp_inst
        inst pp_conds conds

let pp_rul_id fmt = function
  | Rul_id (id, _) -> fprintf fmt "%s" id
  | Rul_id_fun (id, args, _) -> fprintf fmt "@[<hov 2>%s@,@[<hov 1>(%a)@]@]" id
        (pp_print_list ~pp_sep:pp_comma pp_exp)
        args

let pp_param_int fmt = function
  | Param_int_val (exp, _) -> fprintf fmt "%a" pp_exp exp
  | Param_int_range (exp0, expi, expn, _) ->
      fprintf fmt "@[<hov 2>[ %a@ :@ %a@ :@ %a ]@]" pp_exp exp0 pp_exp expi pp_exp expn
  | Param_int_set (exps, _) ->
      fprintf fmt "{ %a }"
        (pp_print_list ~pp_sep:pp_comma pp_exp)
        exps

let pp_param_float fmt = function
  | Param_float_val (exp, _) -> fprintf fmt "%a" pp_exp exp
  | Param_float_range (exp0, expi, expn, _) ->
      fprintf fmt "@[<hov 2>[ %a@ :@ %a@ :@ %a ]@]" pp_exp exp0 pp_exp expi pp_exp expn
  | Param_float_set (exps, _) ->
      fprintf fmt "{ %a }"
        (pp_print_list ~pp_sep:pp_comma pp_exp)
        exps

let pp_param_str fmt = function
  | Param_str_val (exp, _) -> fprintf fmt "%a" pp_exp exp
  | Param_str_set (exps, _) ->
      fprintf fmt "{ %a }"
        (pp_print_list ~pp_sep:pp_comma pp_exp)
        exps

let concat id args =
  id ^ "(" ^ (List.map string_of_exp args |> String.concat ", ") ^ ")"

let sort_preds sort preds =
  if sort then
    let to_string = function
      | Pred_id (a, _, _) -> a
      | Pred_id_fun (a, args, _, _) -> concat a args in
    List.sort
      (fun a b -> String.compare (to_string a) (to_string b))
      preds
  else preds

and sort_rules sort rules =
  if sort then
    let to_string = function
      | Rul_id (a, _) -> a
      | Rul_id_fun (a, args, _) -> concat a args in
    List.sort
      (fun a b -> String.compare (to_string a) (to_string b))
      rules
  else rules

and sort_act sort acts =
  if sort then
    List.sort
      (fun { action_id = a; _ } { action_id = b; _ } -> String.compare a b)
      acts
  else acts

and sort_act_rules sort rules =
  if sort then List.sort String.compare rules else rules

let pp_rs sort strip_comments margin fmt
    {
      dbrs_pri;
      dbrs_init;
      dbrs_params;
      dbrs_actions;
      dbrs_preds;
      dbrs_type;
      dbrs_loc;
    } =
  Comments.pp_before fmt strip_comments margin dbrs_loc;
  fprintf fmt "@[<v 2>begin %a@;@[<v 0>@;%a%a@;@;%a%a%a@]@]@;@;end"
    Bigraph.(
      fun fmt -> function
        | Rs.BRS -> fprintf fmt "brs"
        | Rs.PBRS -> fprintf fmt "pbrs"
        | Rs.SBRS -> fprintf fmt "sbrs"
        | Rs.ABRS -> fprintf fmt "abrs")
    (fst dbrs_type)
    (* Optional *)
    (fun fmt -> function
       | [] -> ()
       | params ->
         fprintf fmt "%a@;@;"
           (Comments.pp_list
              strip_comments
              (margin - 2)
              ~pp_sep:(fun fmt () -> fprintf fmt "@;@;")
              ~lex_loc:loc_of_param
              ~lex_pp:(fun fmt param ->
                  fprintf fmt "@[<hov 2>";
                  (match param with
                   | Param_int (ids, exp, _) ->
                     fprintf fmt "int %a =@;@[<hov 2>%a;@]"
                       pp_strings ids pp_param_int exp;
                   | Param_float (ids, exp, _) ->
                     fprintf fmt "float %a =@;@[<hov 2>%a;@]"
                       pp_strings ids pp_param_float exp
                   | Param_str (ids, exp, _) ->
                     fprintf fmt "string %a =@;@[<hov 2>%a;@]"
                       pp_strings ids pp_param_str exp);
                  fprintf fmt "@]")
              ~p_next:(snd dbrs_init))
           params)
    (fst dbrs_params)
    (fun fmt b ->
       Comments.pp_before_after fmt strip_comments (margin - 2)
         (loc_of_init b)
         (snd dbrs_pri)
         (fun () -> fprintf fmt "@[<hov 2>init ";
           (match b with
            | Init (id, _) -> fprintf fmt "%s;" id
            | Init_fun (id, args, _) ->
              fprintf fmt "%s(@[<hv 1>%a@]);" id
                (pp_print_list ~pp_sep:pp_comma pp_exp)
                args);
           fprintf fmt "@]"))
    (fst dbrs_init)
    (fun fmt (pr_exps, p1) ->
       let p_next = snd dbrs_actions in
       Comments.pp_before_after fmt strip_comments (margin - 2) p1 p_next
         (fun () -> fprintf fmt "@[<hov 2>rules =@;@[<v 2>[ %a ];@]@]"
             (Comments.pp_list
                strip_comments
                (margin - 6)
                ~pp_sep:pp_comma  (* FIX newline after comments *)
                ~lex_loc:loc_of_pclass
                ~lex_pp:(fun fmt pclass ->
                    let lex_pp =
                      (Comments.pp_list
                         strip_comments
                         (margin - 8)
                         ~pp_sep:pp_comma  (* FIX newline after comments *)
                         ~lex_loc:loc_of_rul_id
                         ~lex_pp:pp_rul_id
                         ~p_next) in (* FIX we need the location of the next class *)
                    match pclass with (* match two classes to get the loc of the next one *)
                    | Pr_red (ids, _) ->
                      fprintf fmt "( @[<hov 0>%a )@]" lex_pp (sort_rules sort ids)
                    | Pr (ids, _) ->
                      fprintf fmt "{ @[<hov 0>%a }@]" lex_pp (sort_rules sort ids))
                ~p_next)
         pr_exps))
    dbrs_pri
    (* Optional *)
    (fun fmt -> function
       | ([], _) -> ()
       | (act_exp, p1) ->
         fprintf fmt "@;@;";
         let p_next = snd dbrs_preds in
         Comments.pp_before_after fmt strip_comments (margin - 2) p1 p_next
           (fun () ->
              fprintf fmt "@[<hov 2>actions =@;@[<v 2>[ %a ];@]@]"
                (Comments.pp_list
                   strip_comments
                   (margin - 6)
                   ~pp_sep:pp_comma (* FIX newline after comments *)
                   ~lex_loc:loc_of_daction
                   ~lex_pp:(fun fmt a ->
                        fprintf fmt "@[<hov 2>%s%a =@;{ @[<hov 0>%a }@]@]" a.action_id
                          (fun fmt -> function
                             | Some exp -> fprintf fmt "@,@[<hov 1>[%a]@]" pp_exp exp
                             | None -> ())
                          a.action_reward
                          (pp_print_list
                             ~pp_sep:pp_comma
                             pp_print_string)
                          (sort_act_rules sort a.action_rules))
                   ~p_next)
                (sort_act sort act_exp)))
    dbrs_actions
    (* Optional *)
    (fun fmt -> function
       | ([], _) -> ()
       | (preds, p1) ->
         fprintf fmt "@;@;";
         let p_end = { Loc.dummy_loc with lstart = dbrs_loc.lend } in
         Comments.pp_before_after fmt strip_comments (margin - 2) p1 p_end
           (fun () ->
              fprintf fmt "@[<hov 2>preds =@;@[<hov 2>{ %a };@]@]"
                (Comments.pp_list
                   strip_comments
                   (margin - 6)
                   ~pp_sep:pp_comma (* FIX newline after comments *)
                   ~lex_loc:loc_of_pred
                   ~lex_pp:(fun fmt -> function
                           | Pred_id (id, None, _) -> fprintf fmt "@[<hov 2>%s@]" id
                           | Pred_id (id, Some exp, _) ->
                             fprintf fmt "@[<hov 2>%s@,@[<hov 1>[%a]@]@]" id pp_exp exp
                           | Pred_id_fun (id, args, None, _) ->
                             fprintf fmt "@[<hov 2>%s@,@[<hov 1>(%a)@]@]" id
                               (pp_print_list
                                  ~pp_sep:pp_comma
                                  pp_exp)
                               args
                           | Pred_id_fun (id, args, Some exp, _) ->
                             fprintf fmt "@[<hov 2>%s@,@[<hov 1>(%a)@]@,@[<hov 1>[%a]@]@]" id
                               (pp_print_list
                                  ~pp_sep:pp_comma
                                  pp_exp)
                               args pp_exp exp)
                   ~p_next:p_end)
                (sort_preds sort preds)))
    dbrs_preds;
  Comments.pp_all fmt strip_comments margin dbrs_loc

let pp fmt ?(margin = 80) ?(sort = true) ?(strip_comments = false) m =
  pp_set_margin fmt margin;
  Comments.init ();
  fprintf fmt "@[<v 0>%a@;@;%a@]@."
    (Comments.pp_list
       strip_comments
       margin
       ~pp_sep:(fun fmt () -> fprintf fmt "@;@;")
       ~lex_loc:loc_of_dec
       ~lex_pp:(fun fmt d -> fprintf fmt "%a" pp_dec d)
       ~p_next:(m.model_rs.dbrs_loc))
    (fst m.model_decs)
    (pp_rs sort strip_comments margin) m.model_rs
