open Lexing
open Format

type t = { lstart : Lexing.position; lend : Lexing.position }

let col_offset p =
  p.pos_cnum - p.pos_bol

let print_loc fmt loc =
  (* same line *)
  fprintf fmt "@[File '%s', line %i, characters %i-%i@]@," loc.lstart.pos_fname
    loc.lstart.pos_lnum (col_offset loc.lstart) (col_offset loc.lend)

let string_of_pos loc =
  (* same line *)
  "line " ^ string_of_int loc.lstart.pos_lnum ^ ", characters " ^ string_of_int (col_offset loc.lstart)
  ^ "-" ^ string_of_int (col_offset loc.lend)

let curr lexbuf = { lstart = lexbuf.lex_start_p; lend = lexbuf.lex_curr_p }

let make (lstart, lend) = { lstart; lend }

let dummy_loc = { lstart = Lexing.dummy_pos; lend = Lexing.dummy_pos }

let pp_debug fmt loc =
  fprintf fmt "@[lines %i-%i, characters %i-%i@]@,"
    loc.lstart.pos_lnum
    loc.lend.pos_lnum
    (col_offset loc.lstart)
    (col_offset loc.lend)
