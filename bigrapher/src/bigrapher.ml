open Ast
open Bigraph

(******** PRETTY PRINTING FUNCTIONS *********)

let colorise c msg =
  if Cmd.(defaults.colors) then Utils.colorise c msg else msg

type val_type = [ `s of string | `i of int | `f of float ]

type row = {
  descr : string * Utils.text_style;
  value : val_type;
  pp_val : Format.formatter -> val_type -> unit;
  display : bool;
}

let print_msg fmt c msg =
  if not Cmd.(defaults.debug || defaults.running_time) then
    Format.fprintf fmt "@?@[%s@]@." (colorise c msg)

let print_descr fmt (d, c) = Format.fprintf fmt "%s" (colorise c d)

let pp_float fmt = function
  | `f f -> Format.fprintf fmt "@[<h>%-3g@]" f
  | `i _ | `s _ -> assert false

let pp_string fmt = function
  | `s s -> Format.fprintf fmt "@[<h>%s@]" s
  | `i _ | `f _ -> assert false

let pp_int fmt = function
  | `i i -> Format.fprintf fmt "@[<h>%-8d@]" i
  | `f _ | `s _ -> assert false

let print_table fmt (rows : row list) =
  let pp_row fmt r =
    Format.(
      pp_print_tab fmt ();
      fprintf fmt "%a" print_descr r.descr;
      pp_print_tab fmt ();
      fprintf fmt "%a" r.pp_val r.value)
  in
  match List.filter (fun r -> r.display) rows with
  | r :: rows ->
      Format.(
        pp_open_tbox fmt ();
        (* First row *)
        pp_set_tab fmt ();
        fprintf fmt "@[<h>%s" (colorise (snd r.descr) (fst r.descr));
        pp_print_break fmt (16 - String.length (fst r.descr)) 0;
        fprintf fmt "@]";
        pp_set_tab fmt ();
        fprintf fmt "%a" r.pp_val r.value;
        List.iter (pp_row fmt) rows;
        pp_close_tbox fmt ();
        pp_print_newline fmt ())
  | _ -> ()

let print_header fmt () =
  if not Cmd.(defaults.debug || defaults.running_time) then (
    Format.fprintf fmt "@[<v>@,%s@,%s@,"
      (colorise `bold "BigraphER: Bigraph Evaluator & Rewriting")
      "========================================";
    [
      {
        descr = ("Version:", `blue);
        value = `s (String.trim Version.version);
        pp_val = pp_string;
        display = true;
      };
      {
        descr = ("Date:", `blue);
        value = `s (Utils.format_time ());
        pp_val = pp_string;
        display = true;
      };
      {
        descr = ("Hostname:", `blue);
        value = `s (Unix.gethostname ());
        pp_val = pp_string;
        display = true;
      };
      {
        descr = ("OS type:", `blue);
        value = `s Sys.os_type;
        pp_val = pp_string;
        display = true;
      };
      {
        descr = ("Solver:", `blue);
        value = `s Cmd.(string_of_solver_type defaults.solver);
        pp_val = pp_string;
        display = true;
      };
      {
        descr = ("Command line:", `blue);
        value = `s (String.concat " " (Array.to_list Sys.argv));
        pp_val = pp_string;
        display = true;
      };
    ]
    |> print_table fmt)

let print_max fmt =
  print_table fmt
    [
      {
        descr = ("Max # states:", `cyan);
        value = `i Cmd.(defaults.max_states);
        pp_val = pp_int;
        display = not Cmd.defaults.running_time;
      };
    ]

let print_max_sim fmt typ seed =
  (match typ with
  | Rs.BRS | Rs.PBRS | Rs.ABRS ->
      {
        descr = ("Max sim steps:", `cyan);
        value = `i Cmd.(defaults.steps);
        pp_val = pp_int;
        display = not Cmd.defaults.running_time;
      }
  | Rs.SBRS ->
      {
        descr = ("Max sim time:", `cyan);
        value = `f Cmd.(defaults.time);
        pp_val = pp_float;
        display = not Cmd.defaults.running_time;
      })
  ::
  (match seed with
  | None -> []
  | Some x ->
      [
        {
          descr = ("Seed:", `cyan);
          value = `i x;
          pp_val = pp_int;
          display = not Cmd.defaults.running_time;
        };
      ])
  |> print_table fmt

let print_fun fmt c verb fname i =
  if verb then
    print_msg fmt c (string_of_int i ^ " bytes written to `" ^ fname ^ "'")

(* Progress bars *)
module B = struct
  open Progress

  let progress_bar ~total =
    Line.(
      list
        [
          spinner
            ?color:
              (if Cmd.defaults.colors then Some (Color.ansi `green) else None)
            ();
          spacer 1;
          brackets (elapsed ());
          spacer 2;
          bar
            ~style:(if Cmd.defaults.progress_ascii then `ASCII else `UTF8)
            ~data:`Sum
            ?color:
              (if Cmd.defaults.colors then Some (Color.ansi `cyan) else None)
            ~width:(`Fixed 60) total;
          count_to total;
        ])

  let checkpoint_f fmt ~i s ~c =
    if i mod c = 0 then
      interject_with (fun () ->
          try
            let name = string_of_int i ^ ".txt" in
            let path =
              Option.value ~default:"." Cmd.defaults.export_states
            in
            Export.B.write_txt s ~name ~path
            |> print_fun fmt `white
                 Cmd.(defaults.verb)
                 (Filename.concat path name)
          with Failure msg ->
            Format.(
              pp_print_flush fmt ();
              fprintf err_formatter "@[<v>@[%s%s@]@."
                (Utils.err_opt Cmd.(defaults.colors))
                msg))

  let open_bar ~total fmt f =
    let bar = progress_bar ~total in
    match Cmd.defaults.checkpoint with
    | None ->
        if Cmd.(defaults.debug || defaults.quiet || defaults.running_time)
        then f (fun _ _ -> ())
        else
          with_reporter bar (fun report ->
              f (fun i _ -> if i < total then report 1 else report 0))
    | Some c ->
        assert (c > 0);
        if Cmd.(defaults.debug || defaults.quiet || defaults.running_time)
        then f (fun i s -> checkpoint_f fmt ~i s ~c)
        else
          with_reporter bar (fun report ->
              f (fun i s ->
                  checkpoint_f fmt ~i s ~c;
                  if i < total then report 1 else report 0))

  let open_bar_sim ~total fmt f =
    let bar = progress_bar ~total in
    match Cmd.defaults.checkpoint with
    | None ->
        if Cmd.(defaults.debug || defaults.quiet || defaults.running_time)
        then f (fun _ _ _ -> ())
        else
          with_reporter bar (fun report ->
              f (fun i _ _ -> if i < total then report 1 else report 0))
    | Some c ->
        assert (c > 0);
        if Cmd.(defaults.debug || defaults.quiet || defaults.running_time)
        then f (fun i s _ -> checkpoint_f fmt ~i s ~c)
        else
          with_reporter bar (fun report ->
              f (fun i s _ ->
                  checkpoint_f fmt ~i s ~c;
                  if i < total then report 1 else report 0))

  let open_bar_sim_float ~total fmt f ~eval =
    let bar =
      Line.Using_float.(
        list
          [
            spinner
              ?color:
                (if Cmd.defaults.colors then Some (Color.ansi `green)
                 else None)
              ();
            spacer 1;
            brackets (elapsed ());
            spacer 2;
            bar
              ~style:(if Cmd.defaults.progress_ascii then `ASCII else `UTF8)
              ~data:`Sum (*`Latest does not work for some reason *)
              ?color:
                (if Cmd.defaults.colors then Some (Color.ansi `cyan)
                 else None)
              ~width:(`Fixed 60) total;
            count_to total;
          ])
    and limit_eval = function `I _ -> assert false | `F x -> x in
    match Cmd.defaults.checkpoint with
    | None ->
        if Cmd.(defaults.debug || defaults.quiet || defaults.running_time)
        then f (fun _ _ _ -> ())
        else
          let count = ref 0.0 in
          with_reporter bar (fun report ->
              f (fun _ _ t ->
                  let t_sim = limit_eval (eval t) in
                  let t_delta = t_sim -. !count
                  and t_fill = total -. !count in
                  count := t_sim;
                  if t_sim < total then report (Float.round t_delta)
                  else report (Float.round t_fill)))
    | Some c ->
        assert (c > 0);
        if Cmd.(defaults.debug || defaults.quiet || defaults.running_time)
        then f (fun i s _ -> checkpoint_f fmt ~i s ~c)
        else
          let count = ref 0.0 in
          with_reporter bar (fun report ->
              f (fun i s t ->
                  checkpoint_f fmt ~i s ~c;
                  let t_sim = limit_eval (eval t) in
                  let t_delta = t_sim -. !count
                  and t_fill = total -. !count in
                  count := t_sim;
                  if t_sim < total then report (Float.round t_delta)
                  else report (Float.round t_fill)))
end

module Run
    (T : Rs.RS)
    (L : sig
      val stop : T.limit
    end)
    (P : sig
      val parse_react :
        string ->
        Big.t ->
        Big.t ->
        ?conds:AppCond.t list ->
        [ `E of unit | `F of float | `P of string * int * float ] ->
        Fun.t option ->
        Loc.t ->
        (T.react, string) result
    end)
    (J : sig
      val f : T.graph -> String.t
    end) =
struct
  module S = Store.Make (T) (P)
  module E = Export.T (T) (J)
  module ER = Export.R (T)

  (******** EXPORT FUNCTIONS *********)

  let format_map = function
    | Cmd.Svg -> (Export.B.write_svg, ER.write_svg, ".svg")
    | Cmd.Dot -> (Export.B.write_dot, ER.write_dot, ".dot")
    | Cmd.Json -> (Export.B.write_json, ER.write_json, ".json")
    | Cmd.Txt -> (Export.B.write_txt, ER.write_txt, ".txt")
    | Cmd.Tikz -> (Export.B.write_tikz, ER.write_tikz, ".tikz")

  let export_decs fmt path m env env_t =
    print_msg fmt `yellow ("Exporting declarations to " ^ path ^ " ...");
    S.export (fst m.model_decs) env env_t path
      (List.map format_map Cmd.(defaults.out_format))
      fmt
      Cmd.(defaults.colors)
      (print_fun fmt `white Cmd.(defaults.verb))

  let write_file fmt f fname =
    try f |> print_fun fmt `white Cmd.(defaults.verb) fname
    with E.EXPORT_ERROR e ->
      Format.(
        pp_print_flush fmt ();
        fprintf err_formatter "@[<v>@[%s%s@]@."
          (Utils.err_opt Cmd.(defaults.colors))
          e)

  let export_prism argument fmt msg f =
    match argument with
    | None -> ()
    | Some file ->
        print_msg fmt `yellow (msg ^ file ^ " ...");
        write_file fmt
          (f ~name:(Filename.basename file) ~path:(Filename.dirname file))
          file

  let export_csl fmt f =
    match Cmd.(defaults.export_lab) with
    | None -> ()
    | Some file ->
        print_msg fmt `yellow ("Exporting properties to " ^ file ^ " ...");
        write_file fmt
          (f ~name:(Filename.basename file) ~path:(Filename.dirname file))
          file

  let export_states fmt f g =
    if Cmd.(defaults.export_states_flag) then
      match Cmd.(defaults.export_states) with
      | None -> assert false
      | Some path ->
          print_msg fmt `yellow ("Exporting states to " ^ path ^ " ...");
          f
            (fun i s ->
              let aux i s f ext =
                let fname = string_of_int i ^ ext in
                try
                  f s ~name:fname ~path
                  |> print_fun fmt `white
                       Cmd.(defaults.verb)
                       (Filename.concat path fname)
                with Failure msg ->
                  Format.(
                    pp_print_flush fmt ();
                    fprintf err_formatter "@[<v>@[%s%s@]@."
                      (Utils.err_opt Cmd.(defaults.colors))
                      msg)
              in
              Cmd.(defaults.out_format)
              |> List.map format_map
              |> List.iter (fun (f, _, ext) -> aux i s f ext))
            g

  let export_ts fmt msg formats =
    match Cmd.(defaults.export_graph) with
    | None -> ()
    | Some file ->
        let name =
          let n = Filename.basename file in
          try Filename.chop_extension n with Invalid_argument _ -> n
        and path = Filename.dirname file in
        List.iter
          (fun (f, ext) ->
            let file' = Filename.concat path (name ^ ext) in
            print_msg fmt `yellow (msg ^ file' ^ " ...");
            write_file fmt (f ~name:(name ^ ext) ~path) file)
          formats

  let print_stats_store fmt env priorities =
    [
      {
        descr = ("Type:", `cyan);
        value = `s (Rs.string_of_rs_type T.typ);
        pp_val = pp_string;
        display = true;
      };
      {
        descr = ("Bindings:", `cyan);
        value = `i (Base.H_string.length env);
        pp_val = pp_int;
        display = true;
      };
      {
        descr = ("Rules:", `cyan);
        value = `i (T.cardinal priorities);
        pp_val = pp_int;
        display = true;
      };
    ]
    |> print_table fmt

  let print_stats fmt stats =
    if Cmd.defaults.running_time then
      Format.fprintf fmt "%a@." pp_float (`f Rs.(stats.time))
    else
      Rs.stats_descr stats
      |> List.map (fun (descr, value, flag) ->
             {
               descr = (descr, `green);
               value = `s value;
               pp_val = pp_string;
               display = not (Cmd.defaults.debug && flag);
             })
      |> print_table fmt

  let print_internal_stats fmt stats =
    print_table fmt [
      {
        descr = ("Avg state size:", `green);
        value = `f (Rs.(stats.mean_size));
        pp_val = pp_float;
        display = Cmd.defaults.debug_extra;
      };
      {
        descr = ("Avg match size:", `green);
        value = `f (Rs.(stats.mean_size_match));
        pp_val = pp_float;
        display = Cmd.defaults.debug_extra;
      };
    ];
    Base.H_int.stats_descr Rs.(stats.internal)
    |> List.map (fun (descr, value, flag) ->
           {
             descr = (descr, `green);
             value = `s value;
             pp_val = pp_string;
             display = Cmd.defaults.debug_extra && flag;
           })
    |> print_table fmt

  let after fmt (graph, stats) =
    let format_map = function
      | Cmd.Svg -> (E.write_svg graph, ".svg")
      | Cmd.Json -> (E.write_json graph, ".json")
      | Cmd.Dot -> (E.write_dot graph, ".dot")
      | Cmd.Txt -> (E.write_prism graph, ".txt")
      | Cmd.Tikz -> (E.write_prism graph, ".txt")
    in
    print_stats fmt stats;
    print_internal_stats fmt stats;
    export_ts fmt
      ("Exporting " ^ Rs.string_of_rs_type T.typ ^ " to ")
      (List.map format_map Cmd.(defaults.out_format));
    export_states fmt T.iter_states graph;
    export_prism
      Cmd.(defaults.export_prism)
      fmt
      ("Exporting " ^ Rs.string_of_rs_type T.typ ^ " in PRISM format to ")
      (E.write_prism graph);
    export_prism
      Cmd.(defaults.export_state_rewards)
      fmt
      ("Exporting the state rewards of "
      ^ Rs.string_of_rs_type T.typ
      ^ " in PRISM format to ")
      (E.write_state_rewards graph);
    export_prism
      Cmd.(defaults.export_transition_rewards)
      fmt
      ("Exporting the transition rewards of "
      ^ Rs.string_of_rs_type T.typ
      ^ " in PRISM format to ")
      (E.write_transition_rewards graph);
    export_csl fmt (E.write_lab graph);
    Format.(pp_print_flush err_formatter ());
    exit 0

  let sim fmt s0 priorities preds =
    print_msg fmt `yellow
      ("Starting "
      ^ (match T.typ with
        | Rs.BRS | PBRS | ABRS -> "simulation"
        | SBRS -> "stochastic simulation")
      ^ " ...");
    print_max_sim fmt T.typ Cmd.defaults.seed;
    (match T.typ with
    | Rs.BRS | PBRS | ABRS ->
        B.open_bar_sim ~total:Cmd.defaults.steps fmt
          (T.sim ~s0 ?seed:Cmd.defaults.seed ~priorities ~predicates:preds
             ~init_size:Cmd.(defaults.max_states)
             ~stop:L.stop)
    | SBRS ->
        B.open_bar_sim_float ~total:Cmd.defaults.time fmt ~eval:T.eval_limit
          (T.sim ~s0 ?seed:Cmd.defaults.seed ~priorities ~predicates:preds
             ~init_size:Cmd.(defaults.max_states)
             ~stop:L.stop))
    |> after fmt

  let full fmt s0 priorities preds =
    let ts_type = function
      | Rs.BRS -> "transition system"
      | PBRS -> "DTMC" (* Discrete Time Markov Chain *)
      | SBRS -> "CTMC" (* Continuous Time Markov Chain *)
      | ABRS -> "MDP" (* Markov Decision Process *)
    in
    print_msg fmt `yellow ("Computing " ^ ts_type T.typ ^ " ...");
    print_max fmt;
    B.open_bar ~total:Cmd.defaults.max_states fmt
      (T.bfs ~s0 ~priorities ~predicates:preds
         ~max:Cmd.(defaults.max_states))
    |> after fmt

  let set_trap fmt =
    Sys.set_signal Sys.sigint
      (Sys.Signal_handle
         (fun _ ->
           print_msg fmt `yellow "Execution interrupted by the user.";
           Format.(pp_print_flush err_formatter ());
           exit 0))

  let check fmt =
    print_msg fmt `yellow "Model file parsed correctly";
    Format.(pp_print_flush err_formatter ());
    exit 0

  let format fmt ast =
    Format.(pp_print_flush err_formatter ());
    try
      match Cmd.defaults.fmt_out with
      | None ->
          Pp_ast.pp Format.std_formatter
            ~margin:Cmd.defaults.fmt_columns
            ~sort:Cmd.defaults.fmt_norm
            ~strip_comments:Cmd.defaults.fmt_no_comments
            ast
      | Some file ->
          print_msg fmt `yellow ("Saving formatted model to " ^ file ^ " ...");
          let ch_out = open_out file in
          Pp_ast.pp
            (Format.formatter_of_out_channel ch_out)
            ~margin:Cmd.defaults.fmt_columns
            ~sort:Cmd.defaults.fmt_norm
            ~strip_comments:Cmd.defaults.fmt_no_comments
            ast;
          print_fun fmt `white
            Cmd.(defaults.verb)
            file
            (out_channel_length ch_out);
          close_out ch_out;
          exit 0
    with Sys_error s ->
      Format.(
        pp_print_flush fmt ();
        fprintf err_formatter "@[<v>@[%s%s@]@."
          (Utils.err_opt Cmd.(defaults.colors))
          s)

  let run_aux fmt ast s0 priorities preds = function
    | Cmd.Sim -> (
        set_trap fmt;
        try sim fmt s0 priorities preds with
        | T.LIMIT (graph, stats) ->
            let limit_msg = function
              | Rs.BRS | PBRS | ABRS -> "number of simulation steps"
              | SBRS -> "simulation time"
            in
            print_msg fmt `yellow ("Maximum " ^ limit_msg T.typ ^ " reached.");
            after fmt (graph, stats)
        | T.DEADLOCK (graph, stats, limit) ->
            let limit_type = function
              | Rs.BRS | PBRS | ABRS -> "step"
              | SBRS -> "time"
            in
            print_msg fmt `yellow
              ("Deadlock state reached at " ^ limit_type T.typ ^ " "
             ^ T.string_of_limit limit ^ ".");
            after fmt (graph, stats))
    | Cmd.Full -> (
        set_trap fmt;
        try full fmt s0 priorities preds
        with T.MAX (graph, stats) ->
          print_msg fmt `yellow "Maximum number of states reached.";
          after fmt (graph, stats))
    | Cmd.Check -> check fmt
    | Cmd.Fmt -> format fmt ast
    | Cmd.Exit -> ()

  let run fmt c m exec_type =
    try
      let env = S.init_env c Cmd.(defaults.consts) in
      let s0, pri, preds, env_t = S.eval_model c m env in
      if Cmd.defaults.warn then S.warn_unused env Cmd.defaults.colors;
      if Option.is_some Cmd.defaults.export_decls then
        export_decs fmt (Option.get Cmd.defaults.export_decls) m env env_t;
      if not Cmd.defaults.running_time then print_stats_store fmt env pri;
      run_aux fmt m s0 pri preds exec_type
    with S.ERROR (e, p) ->
      Format.(
        pp_print_flush fmt ();
        fprintf err_formatter "@[<v>";
        Loc.print_loc err_formatter p;
        S.report_error c e;
        pp_print_flush err_formatter ();
        exit 1)
end

(******** BIGRAPHER *********)

let open_lex model =
  let fname, file =
    match model with
    | None -> ("stdin", stdin)
    | Some path -> (Filename.basename path, open_in path)
  in
  let lexbuf = Lexing.from_channel file in
  lexbuf.Lexing.lex_curr_p <-
    {
      Lexing.pos_fname = fname;
      Lexing.pos_lnum = 1;
      Lexing.pos_bol = 0;
      Lexing.pos_cnum = 0;
    };
  (lexbuf, file)

(* Ignore mutually exlusive flags *)
let ignore_flags () =
  Cmd.(
    if defaults.quiet then defaults.verb <- false;
    if defaults.debug_extra then defaults.debug <- true;
    if defaults.debug then (
      defaults.running_time <- false;
      defaults.colors <- false);
    if defaults.running_time then defaults.colors <- false)

let set_output_ch () =
  if Cmd.(defaults.quiet) then Format.str_formatter else Format.std_formatter

let report_invalid_arrow name loc =
  Format.fprintf Format.err_formatter
    "@[%sInvalid or missing arrow expression for rule `%s' at `%s'@]@."
    (Utils.err_opt Cmd.(defaults.colors))
    name (Loc.string_of_pos loc);
  exit 1

let () =
  (* Allow early exit, e.g. when the config is to be printed *)
  match Cmd.parse_cmds with
  | Ok (`Ok Cmd.Exit) -> exit 1
  | Error `Parse | Error `Term -> exit Cmdliner.Cmd.Exit.cli_error
  | Error `Exn -> exit Cmdliner.Cmd.Exit.internal_error
  | Ok `Version | Ok `Help -> exit 0
  | Ok (`Ok exec_type) -> (
      (* Utils.setup_gc (); *)
      ignore_flags ();
      try
        let fmt = set_output_ch () in
        print_header fmt ();
        print_msg fmt `yellow
          ("Parsing model file "
          ^ (match Cmd.(defaults.model) with
            | None -> "stdin"
            | Some name -> name)
          ^ " ...");
        Format.pp_print_flush fmt ();
        let lexbuf, file = open_lex Cmd.(defaults.model) in
        try
          let m = Parser.model Lexer.token lexbuf in
          close_in file;
          let module Selector
              (T : sig
                val rs_type : Bigraph.Rs.rs_type
              end)
              (M : Solver.M) =
          struct
            let run fmt colors m exec_type =
              match T.rs_type with
              | Rs.BRS ->
                  let module BRS = Brs.Make (M) in
                  let module R =
                    Run
                      (BRS)
                      (struct
                        let stop = Cmd.(defaults.steps)
                      end)
                      (struct
                        let parse_react name lhs rhs ?(conds = []) _ eta _ =
                          BRS.parse_react_err ~name ~lhs ~rhs ~conds () eta
                      end)
                      (struct
                        let f big =
                          Yojson.Safe.pretty_to_string
                          @@ BRS.yojson_of_graph big
                      end)
                  in
                  R.run fmt colors m exec_type
              | Rs.PBRS ->
                  let module PBRS = Pbrs.Make (M) in
                  let module R =
                    Run
                      (PBRS)
                      (struct
                        let stop = Cmd.(defaults.steps)
                      end)
                      (struct
                        let parse_react name lhs rhs ?(conds = []) l eta loc
                            =
                          match l with
                          | `F f ->
                              PBRS.parse_react_err ~name ~lhs ~rhs ~conds f
                                eta
                          | _ -> report_invalid_arrow name loc
                      end)
                      (struct
                        let f big =
                          Yojson.Safe.pretty_to_string
                          @@ PBRS.yojson_of_graph big
                      end)
                  in
                  R.run fmt colors m exec_type
              | Rs.SBRS ->
                  let module SBRS = Sbrs.Make (M) in
                  let module R =
                    Run
                      (SBRS)
                      (struct
                        let stop = Cmd.(defaults.time)
                      end)
                      (struct
                        let parse_react name lhs rhs ?(conds = []) l eta loc
                            =
                          match l with
                          | `F f ->
                              SBRS.parse_react_err ~name ~lhs ~rhs ~conds f
                                eta
                          | _ -> report_invalid_arrow name loc
                      end)
                      (struct
                        let f big =
                          Yojson.Safe.pretty_to_string
                          @@ SBRS.yojson_of_graph big
                      end)
                  in
                  R.run fmt colors m exec_type
              | Rs.ABRS ->
                  let module ABRS = Abrs.Make (M) in
                  let module R =
                    Run
                      (ABRS)
                      (struct
                        let stop = Cmd.(defaults.steps)
                      end)
                      (struct
                        let parse_react name lhs rhs ?(conds = []) l eta loc
                            =
                          match l with
                          | `P p ->
                              ABRS.parse_react_err ~name ~lhs ~rhs ~conds p
                                eta
                          | _ -> report_invalid_arrow name loc
                      end)
                      (struct
                        let f big =
                          Yojson.Safe.pretty_to_string
                          @@ ABRS.yojson_of_graph big
                      end)
                  in
                  R.run fmt colors m exec_type
          end in
          let module T = struct
            let rs_type = fst m.model_rs.dbrs_type
          end in
          match Cmd.(defaults.solver) with
          | Bigraph.Solver.MSAT ->
              let module S = Selector (T) (Solver.Make_SAT (Solver.MS)) in
              S.run fmt Cmd.(defaults.colors) m exec_type
          | Bigraph.Solver.MCARD ->
              let module S = Selector (T) (Solver.Make_SAT (Solver.MC)) in
              S.run fmt Cmd.(defaults.colors) m exec_type
          | Bigraph.Solver.GBS ->
              let module S = Selector (T) (Solver.GBS) in
              S.run fmt Cmd.(defaults.colors) m exec_type
        with
        | Place.NOT_PRIME ->
            Format.(
              fprintf err_formatter
                "@[<v>@[%sThe parameter of a reaction rule is not prime.@]@."
                (Utils.err_opt Cmd.(defaults.colors)));
            exit 1
        | Parser.Error ->
            Format.(
              pp_print_flush fmt ();
              fprintf err_formatter "@[<v>";
              Loc.print_loc err_formatter
                Loc.
                  {
                    lstart = Lexing.(lexbuf.lex_start_p);
                    lend = Lexing.(lexbuf.lex_curr_p);
                  };
              fprintf err_formatter "@[%sSyntax error near token `%s'@]@."
                (Utils.err_opt Cmd.(defaults.colors))
                (Lexing.lexeme lexbuf);
              exit 1)
      with
      | Lexer.ERROR (e, p) ->
          Format.(
            fprintf err_formatter "@[<v>";
            Loc.print_loc err_formatter p;
            Lexer.report_error err_formatter
              (Utils.err_opt Cmd.(defaults.colors))
              e;
            exit 1)
      | Sys_error s ->
          Format.(
            fprintf err_formatter "@[%s%s@]@."
              (Utils.err_opt Cmd.(defaults.colors))
              s;
            exit 1)
      | e ->
          Format.(
            fprintf err_formatter "@[%s@ %s@]@." (Printexc.to_string e)
              (Printexc.get_backtrace ());
            exit 1))
