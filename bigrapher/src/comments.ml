let comments : (String.t * Loc.t) list ref = ref []

let store lexbuf =
  let open Lexing in
  let s = lexeme lexbuf
  and pos = Loc.make (lexeme_start_p lexbuf, lexeme_end_p lexbuf) in
  comments := ((s, pos) :: !comments);
  new_line lexbuf

let init () =
  comments := List.rev !comments

let is_consecutive p0 p1 =
  Loc.(p0.lend.Lexing.pos_lnum + 1 = p1.lstart.Lexing.pos_lnum) (* next line *)
  || Loc.(p0.lend.Lexing.pos_lnum = p1.lstart.Lexing.pos_lnum) (* same line *)

let before_p xp p =
  Loc.(xp.lstart.Lexing.pos_lnum < p.lstart.Lexing.pos_lnum)

let before_p_end xp p =
  Loc.(xp.lstart.Lexing.pos_lnum <= p.lend.Lexing.pos_lnum)

let trim c =
  String.sub c 1 ((String.length c) - 1) |> String.trim

let next p f =
  let rec aux res = function
    | [] -> res
    | (c, xp) :: xs ->
      if f xp p then
        ( comments := xs;
          aux ((trim c, xp) :: res) xs )
      else res
  in
  aux [] !comments |> List.rev

let next_block p p_next =
  let rec aux last_p res = function
    | [] -> res
    | (c, p) :: xs ->
      if (is_consecutive last_p p) && (before_p p p_next)
      then ( comments := xs;
             aux p  ((trim c, p) :: res) xs )
      else res
  in
  aux p [] !comments |> List.rev

let warp columns s =
  assert (columns > 0);
  let tokens = String.split_on_char ' ' s
  and buff = Buffer.create columns in
  let lines =
    List.fold_left (fun acc t ->
        if ((Buffer.length buff) + (String.length t) + 1 > columns)
        then (let l = Buffer.contents buff in
              Buffer.clear buff;
              Buffer.add_string buff t;
              l :: acc)
        else (if (Buffer.length buff) != 0 then Buffer.add_char buff ' ';
              Buffer.add_string buff t;
              acc)) [] tokens in
  List.rev (Buffer.contents buff :: lines)

(* Print a warped comment *)
let pp fmt columns c =
  warp (columns - 2) c
  |> List.map (fun l -> "# " ^ l)
  |> Format.(pp_print_list
               ~pp_sep:(fun fmt () -> fprintf fmt "@;")
               (fun fmt c ->
                  fprintf fmt "@[<h 0>%s@]" c))
    fmt

(* Comments before position p.

   # cccccc
   # cccccc

   # cccccc
   # cccccc

   # cccccc           no new line if this is attached
   xxxxxxxxxxxxxxx p.lstart

*)
let pp_before fmt strip_comments columns p =
  if strip_comments then ()
  else Format.(
    let rec pp_list = function
      | [] -> ()
      | [(c, p_last)] ->
        (pp fmt columns c;
         fprintf fmt "@;";
         if not (is_consecutive p_last p) then fprintf fmt "@;")
      | (c0, p0) :: (c1, p1) :: cs ->
        (pp fmt columns c0;
         fprintf fmt "@;";
         if not (is_consecutive p0 p1) then fprintf fmt "@;";
         pp_list ((c1, p1) :: cs))
    and comments = next p before_p in
    fprintf fmt "@[<v 0>";
    pp_list comments;
    fprintf fmt "@]")

(* Comments before or on the same line of position p and consecutive
   comments before lexeme starting at p_next. No new line is added. *)
let pp_after fmt strip_comments columns p p_next =
  if strip_comments then ()
  else
    (
      (*Format.fprintf fmt "pp_after p=%a p_next=%a@;" Loc.pp_debug p Loc.pp_debug p_next;*)
      match (next p before_p_end) @ (next_block p p_next) with
      | [] -> ()
      | cs ->
        Format.(fprintf fmt "@;@[<v 0>";
                (List.map fst cs
                 |> pp_print_list
                   ~pp_sep:(fun fmt () -> fprintf fmt "@;")
                   (fun fmt c -> pp fmt columns c) fmt);
                fprintf fmt "@]")
    )

let pp_before_after fmt strip_comments columns p p_next f =
  pp_before fmt strip_comments columns p;
  f ();
  pp_after fmt strip_comments columns p p_next

(* Print a list of lexemes. p_next is the location of the first lexeme
   after the list. *)
let pp_list strip_comments columns ~pp_sep ~lex_loc ~lex_pp ~p_next fmt lex_list =
  let is_first = ref true
  and head_loc = function
  | [] -> p_next
  | l::_ -> lex_loc l in
  let rec aux = function
    | [] -> ()
    | l :: list ->
      (if !is_first then is_first := false else pp_sep fmt ();
       pp_before_after fmt strip_comments columns (lex_loc l) (head_loc list)
         (fun () -> Format.fprintf fmt "%a" lex_pp l);
       aux list)
  in aux lex_list

let pp_all fmt strip_comments columns p =
  if strip_comments then ()
  else
      match !comments with
      | [] -> ()
      | (c0, p0) :: cs ->
        Format.(fprintf fmt "@;@[<v 0>";
                if not (is_consecutive p p0) then fprintf fmt "@;";
                pp_print_list
                  ~pp_sep:(fun fmt () -> fprintf fmt "@;@;") (* FIX Don't split blocks *)
                  (fun fmt (c, _) -> pp fmt columns (trim c))
                  fmt ((c0, p0) :: cs);
                fprintf fmt "@]")
