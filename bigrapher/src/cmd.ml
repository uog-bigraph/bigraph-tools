open Format
open Utils
open Cmdliner

type path = string
type file = string
type format_op = Dot | Svg | Txt | Json | Tikz

type settings = {
  mutable consts : Ast.const list;
  mutable debug : bool;
  mutable debug_extra : bool;
  mutable dot_installed : bool;
  mutable export_graph : file option;
  mutable export_lab : file option;
  mutable export_prism : file option;
  mutable export_decls : file option;
  mutable export_states : path option;
  mutable export_states_flag : bool;
  mutable export_state_rewards : path option;
  mutable export_transition_rewards : path option;
  mutable help : bool;
  mutable max_states : int;
  mutable model : string option;
  mutable out_format : format_op list;
  mutable quiet : bool;
  mutable seed : int option;
  mutable steps : int;
  mutable time : float;
  mutable verb : bool;
  mutable warn : bool;
  mutable colors : bool;
  mutable running_time : bool;
  mutable initfile : file option;
  mutable checkpoint : int option;
  mutable solver : Bigraph.Solver.solver_t;
  mutable fmt_columns : int;
  mutable fmt_norm : bool;
  mutable fmt_out : file option;
  mutable fmt_no_comments : bool;
  mutable progress_ascii : bool;
  mutable tikz_config : file option;
}

let default_formats = [ Dot ]

let defaults =
  {
    consts = [];
    debug = false;
    debug_extra = false;
    dot_installed = dot_installed ();
    export_graph = None;
    export_lab = None;
    export_prism = None;
    export_decls = None;
    export_states = None;
    export_states_flag = false;
    export_state_rewards = None;
    export_transition_rewards = None;
    help = false;
    max_states = 1000;
    model = None;
    out_format = default_formats;
    quiet = false;
    seed = None;
    steps = 1000;
    time = 1000.0;
    verb = false;
    warn = false;
    colors = true;
    running_time = false;
    initfile = None;
    checkpoint = None;
    solver = Bigraph.Solver.MCARD;
    (* MiniCARD *)
    fmt_columns = 80;
    fmt_norm = false;
    fmt_out = None;
    fmt_no_comments = false;
    progress_ascii = false;
    tikz_config = None;
  }

type cmd_t = Check | Fmt | Full | Sim | Exit

(* Stand alone options *)
let print_table fmt rows ?(offset = 0) f_l f_r =
  (* Find longest row *)
  let l_max =
    List.fold_left
      (fun max r ->
        let l = String.length (f_l r) in
        if l > max then l else max)
      0 rows
  in
  let pp_row fmt row =
    pp_print_tab fmt ();
    pp_print_string fmt (f_l row);
    pp_print_tab fmt ();
    fprintf fmt "%a" f_r row
  in
  match rows with
  | first :: rows ->
      pp_open_tbox fmt ();
      pp_set_tab fmt ();
      let l = f_l first in
      fprintf fmt "@[<h>%s" l;
      pp_print_break fmt (l_max - offset) 0;
      fprintf fmt "@]";
      pp_set_tab fmt ();
      fprintf fmt "%a" f_r first;
      List.iter (pp_row fmt) rows;
      pp_close_tbox fmt ()
  | _ -> assert false

let string_of_format f =
  List.map
    (function
      | Svg -> "svg"
      | Json -> "json"
      | Dot -> "dot"
      | Txt -> "txt"
      | Tikz -> "tikz")
    f
  |> String.concat ","

let string_of_solver_type = function
  | Bigraph.Solver.MSAT -> "MiniSAT"
  | Bigraph.Solver.MCARD -> "MiniCARD"
  | Bigraph.Solver.GBS -> "Glasgow Subgraph Solver"

let string_of_file = function None -> "-" (* Not set *) | Some f -> f

let parse_formats s =
  String.split_on_char ',' s
  |> List.fold_left
       (fun acc s ->
         match s with
         | "dot" -> Dot :: acc
         | "svg" -> Svg :: acc
         | "txt" -> Txt :: acc
         | "json" -> Json :: acc
         | "tikz" -> Tikz :: acc
         | _ -> acc)
       []
  |> function
  | [] -> default_formats
  | x -> x

let parse_solver_type = function
  | "MCARD" -> Bigraph.Solver.MCARD
  | "MSAT" -> Bigraph.Solver.MSAT
  | "GBS" -> Bigraph.Solver.GBS
  | x ->
      fprintf err_formatter
        "@[<h 2>%s@ \"%s\" is not a recognised solver.@;\
         Using MiniCARD instead.@]@."
        (warn_opt defaults.colors)
        x;
      Bigraph.Solver.MCARD

let supported_solvers_str =
  "Supported solvers are `MSAT' (MiniSAT), `MCARD' (MiniCARD), and `GBS' \
   (Glasgow subgraph solver)"

let eval_config fmt () =
  let config_str fmt () =
    let conf =
      [
        ( "checkpoint",
          fun fmt () ->
            match defaults.checkpoint with
            | None -> fprintf fmt "@[<hov>%s@]" "-"
            | Some x -> fprintf fmt "@[<hov>%d@]" x );
        ( "colors",
          fun fmt () ->
            (match Sys.getenv_opt "BIGNOCOLORS" with
            | None -> ()
            | Some _ -> defaults.colors <- false);
            fprintf fmt "@[<hov>%b@]" defaults.colors );
        ( "consts",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]"
              (match Ast.string_of_consts defaults.consts with
              | "" -> "-"
              | s -> s) );
        ("debug", fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.debug);
        ( "debug_extra",
          fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.debug_extra );
        ( "export_graph",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]" (string_of_file defaults.export_graph)
        );
        ( "export_lab",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]" (string_of_file defaults.export_lab) );
        ( "export_prism",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]" (string_of_file defaults.export_prism)
        );
        ( "export_states",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]" (string_of_file defaults.export_states)
        );
        ( "export_states_flag",
          fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.export_states_flag
        );
        ( "export_state_rewards",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]"
              (string_of_file defaults.export_state_rewards) );
        ( "export_transition_rewards",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]"
              (string_of_file defaults.export_transition_rewards) );
        ( "fmt_columns",
          fun fmt () -> fprintf fmt "@[<hov>%d@]" defaults.fmt_columns );
        ( "fmt_no_comments",
          fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.fmt_no_comments );
        ( "fmt_norm",
          fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.fmt_norm );
        ( "fmt_out",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]" (string_of_file defaults.fmt_out) );
        ("help", fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.help);
        ( "initfile",
          fun fmt () ->
            match defaults.initfile with
            | None -> fprintf fmt "@[<hov>%s@]" "-"
            | Some x -> fprintf fmt "@[<hov>%s@]" x );
        ( "max_states",
          fun fmt () -> fprintf fmt "@[<hov>%d@]" defaults.max_states );
        ( "out_format",
          fun fmt () ->
            (match Sys.getenv_opt "BIGFORMAT" with
            | None -> ()
            | Some x -> defaults.out_format <- parse_formats x);
            fprintf fmt "@[<hov>%s@]" (string_of_format defaults.out_format)
        );
        ( "progress_ascii",
          fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.progress_ascii );
        ( "quiet",
          fun fmt () ->
            (match Sys.getenv_opt "BIGQUIET" with
            | None -> ()
            | Some _ -> defaults.quiet <- true);
            fprintf fmt "@[<hov>%b@]" defaults.quiet );
        ( "running_time",
          fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.running_time );
        ( "seed",
          fun fmt () ->
            match defaults.seed with
            | None -> fprintf fmt "@[<hov>%s@]" "-"
            | Some x -> fprintf fmt "@[<hov>%d@]" x );
        ( "solver",
          fun fmt () ->
            (match Sys.getenv_opt "BIGSOLVER" with
            | None -> ()
            | Some x -> defaults.solver <- parse_solver_type x);
            fprintf fmt "@[<hov>%s@]" (string_of_solver_type defaults.solver)
        );
        ("steps", fun fmt () -> fprintf fmt "@[<hov>%d@]" defaults.steps);
        ("time", fun fmt () -> fprintf fmt "@[<hov>%g@]" defaults.time);
        ( "verb",
          fun fmt () ->
            (match Sys.getenv_opt "BIGVERBOSE" with
            | None -> ()
            | Some _ -> defaults.verb <- true);
            fprintf fmt "@[<hov>%b@]" defaults.verb );
        ("warn", fun fmt () -> fprintf fmt "@[<hov>%b@]" defaults.warn);
        ( "tikz_config",
          fun fmt () ->
            fprintf fmt "@[<hov>%s@]" (string_of_file defaults.tikz_config) );
      ]
    in
    print_table fmt conf (fun (x, _) -> x) (fun fmt (_, f) -> f fmt ())
  in
  fprintf fmt "@[<v 2>CONFIGURATION:@,%a@]@." config_str ()

let check_states () =
  if defaults.export_states_flag then
    match (defaults.export_states, defaults.export_graph) with
    | None, Some f ->
        defaults.export_states <- Some (Filename.dirname f);
        true
    | None, None ->
        fprintf err_formatter
          "@[<h 2>%s@,\
           A path must be specified for@ option@ \"-s\"@ unless it@ is@ \
           used@ with@ option@ \"-t\" @]@."
          (err_opt defaults.colors);
        false
    | Some _, None | Some _, Some _ -> true
  else true

let check_dot () =
  if not defaults.dot_installed then
    if List.mem Svg defaults.out_format then (
      (* reset flag *)
      defaults.out_format <- default_formats;
      fprintf err_formatter
        "@[<h 2>%s@,\
         `dot' is not installed on this system.@;\
         Reverting to dot output format.@]@."
        (warn_opt defaults.colors))

let set_tikz_config () =
  match defaults.tikz_config with
  | None -> ()
  | Some jsonf -> Yojson.Safe.from_file jsonf |> Bigraph.Tikz.read_config

let const_conv =
  let split s =
    try
      let i = String.index s '=' in
      let len = String.length s in
      Some (String.sub s 0 i, String.sub s (i + 1) (len - i - 1))
    with Not_found -> None
  and err_msg = "Could not parse assignment" in
  let parse s =
    match split s with
    | None -> Error (`Msg err_msg)
    | Some (id, v) -> (
        match int_of_string_opt v with
        | Some v ->
            Ok
              (Ast.Cint
                 {
                   d_id = id;
                   d_exp = ENum (Num_int_val (v, Loc.dummy_loc));
                   d_loc = Loc.dummy_loc;
                 })
        | None -> (
            match float_of_string_opt v with
            | Some v ->
                Ok
                  (Ast.Cfloat
                     {
                       d_id = id;
                       d_exp = ENum (Num_float_val (v, Loc.dummy_loc));
                       d_loc = Loc.dummy_loc;
                     })
            | None ->
                Ok
                  (Ast.Cstr
                     {
                       d_id = id;
                       d_exp = EStr (Str_val (v, Loc.dummy_loc));
                       d_loc = Loc.dummy_loc;
                     })))
  and print_format pf = function
    | Ast.Cint i -> pp_print_string pf (i.d_id ^ "=<exp>")
    | Ast.Cfloat f -> pp_print_string pf (f.d_id ^ "=<exp>")
    | Ast.Cstr s -> pp_print_string pf (s.d_id ^ "=<exp>")
  in
  (parse, print_format)

let fconv =
  let parse (s : string) =
    match s with
    | "dot" -> Ok Dot
    | "svg" -> Ok Svg
    | "txt" -> Ok Txt
    | "json" -> Ok Json
    | "tikz" -> Ok Tikz
    | _ -> Error (`Msg "Could not parse format")
  in
  let print_format pf = function
    | Dot -> pp_print_string pf "dot"
    | Svg -> pp_print_string pf "svg"
    | Txt -> pp_print_string pf "txt"
    | Json -> pp_print_string pf "json"
    | Tikz -> pp_print_string pf "tikz"
  in
  (parse, print_format)

(* Option parsing *)
let opt_if = function Some _ -> true | None -> false

let empty_to_none = function
  | Some "" -> None
  | Some _ as s -> s
  | None -> None

let copts consts debug debug_extra ext graph lbls prism decls quiet states
    srew trew verbose warn nocols rtime solver initf checkpoint progress tikzconf =
  defaults.consts <- consts;
  defaults.debug <- debug;
  defaults.debug_extra <- debug_extra;
  defaults.out_format <- ext;
  defaults.export_graph <- graph;
  defaults.export_lab <- lbls;
  defaults.export_prism <- prism;
  defaults.export_decls <- decls;
  defaults.quiet <- quiet;
  (* States have extra handling to ensure that the directory is inferred
   * from export-ts if required *)
  defaults.export_states <- empty_to_none states;
  defaults.export_states_flag <- opt_if states;
  defaults.export_state_rewards <- empty_to_none srew;
  defaults.export_transition_rewards <- empty_to_none trew;
  defaults.verb <- verbose;
  defaults.warn <- warn;
  defaults.colors <- not nocols;
  defaults.running_time <- rtime;
  defaults.solver <- parse_solver_type solver;
  defaults.initfile <- initf;
  defaults.checkpoint <- checkpoint;
  defaults.progress_ascii <- progress;
  defaults.tikz_config <- tikzconf

let vopt_int = Arg.opt ~vopt:(Some 1) (Arg.some Arg.int) None
and opt_str = Arg.opt (Arg.some Arg.string) None
and vopt_str = Arg.opt ~vopt:(Some "") (Arg.some Arg.string) None

let debug = Arg.(value & flag & info [ "debug" ])
and debug_extra = Arg.(value & flag & info [ "debug_extra" ])
and rtime = Arg.(value & flag & info [ "running-time" ])

and consts =
  let doc =
    "Specify a comma-separated list of variable assignments.\n\
    \               Example: `x=4,t=.56'."
  in
  Arg.(
    value
    & opt (list (conv const_conv)) []
    & info [ "c"; "const" ] ~docv:"ASSIGNMENT" ~doc)

and ext =
  let doc =
    "A comma-separated list of output formats.\n\
    \               Supported formats are `dot', `json', `svg', `txt', and \
     `tikz'."
  in
  let env = Cmd.Env.info "BIGFORMAT" ~doc in
  Arg.(
    value
    & opt (list (conv fconv)) [ Dot ]
    & info [ "f"; "format" ] ~docv:"FORMAT" ~doc ~env)

and graph =
  let doc = "Export the transition system to $(docv)." in
  Arg.(value & opt_str & info [ "t"; "export-ts" ] ~docv:"FILE" ~doc)

and lbls =
  let doc =
    "Export the labelling function in PRISM .csl format to $(docv)."
  in
  Arg.(value & opt_str & info [ "l"; "export-labels" ] ~docv:"FILE" ~doc)

and prism =
  let doc =
    "Export the transition system in PRISM .tra format to $(docv)."
  in
  Arg.(value & opt_str & info [ "p"; "export-prism" ] ~docv:"FILE" ~doc)

and decls =
  let doc = "Export each declaration to a file in $(docv)." in
  Arg.(value & opt_str & info [ "d"; "export-decls" ] ~docv:"DIR" ~doc)

and srew =
  let doc = "Export state rewards in PRISM .rews format to $(docv)." in
  Arg.(
    value & opt_str & info [ "r"; "export-state-rewards" ] ~docv:"FILE" ~doc)

and trew =
  let doc = "Export transition rewards in PRISM .rewt format to $(docv)." in
  Arg.(
    value & opt_str
    & info [ "R"; "export-transition-rewards" ] ~docv:"FILE" ~doc)

and quiet =
  let doc = "Suppress normal output. Flag \"verbose\" is ignored if set." in
  let env = Cmd.Env.info "BIGQUIET" ~doc in
  Arg.(value & flag & info [ "q"; "quiet" ] ~doc ~env)

and states =
  let doc =
    "Export each state to a file in $(docv).\n\
    \               State indices are used as file names.\n\
    \               When $(docv) is omitted, it is inferred from\n\
    \               option \"export-ts\"."
  in
  Arg.(value & vopt_str & info [ "s"; "export-states" ] ~docv:"DIR" ~doc)

and verbose =
  let doc = "Be more verbose." in
  let env = Cmd.Env.info "BIGVERBOSE" ~doc in
  Arg.(value & flag & info [ "v"; "verbose" ] ~doc ~env)

and warn =
  let doc = "Enable useful warnings." in
  Arg.(value & flag & info [ "w"; "warnings" ] ~doc)

and nocols =
  let doc = "Disable colored output." in
  let env = Cmd.Env.info "BIGNOCOLORS" ~doc in
  Arg.(value & flag & info [ "n"; "no-colors" ] ~doc ~env)

and solver =
  let doc = "Select solver for matching engine.\n" ^ supported_solvers_str in
  let env = Cmd.Env.info "BIGSOLVER" ~doc in
  Arg.(value & opt string "MCARD" & info [ "solver" ] ~doc ~env)

and initfile =
  let doc = "Use bigraph specified in $(docv) as the intial state." in
  Arg.(value & opt_str & info [ "initfile"; "i" ] ~docv:"FILE" ~doc)

and checkpoint =
  let doc =
    "Checkpoint every $(docv) state to txt. The destination is\n\
    \               inferred from option \"export-ts\" if it is set.\n\n\
    \                           The current directory is used otherwise."
  in
  Arg.(value & vopt_int & info [ "checkpoint" ] ~docv:"ith" ~doc)

and progress =
  let doc = "Use ASCII for the progress bar." in
  Arg.(value & flag & info [ "a"; "ascii-bar" ] ~doc)

and tikzconf =
  let doc = "Config JSON for tikz export." in
  Arg.(value & opt_str & info [ "tikzconf" ] ~docv:"FILE" ~doc)

(* Common options for full and sim commands. *)
let copts_t =
  Term.(
    const copts $ consts $ debug $ debug_extra $ ext $ graph $ lbls $ prism
    $ decls $ quiet $ states $ srew $ trew $ verbose $ warn $ nocols $ rtime
    $ solver $ initfile $ checkpoint $ progress $ tikzconf)

let columns =
  let doc = "Set the line-wrap to $(docv)." in
  Arg.(
    value
    & opt int defaults.fmt_columns
    & info [ "W"; "wrap" ] ~docv:"INT" ~doc)

and no_comments =
  let doc =
    "Remove comments from the formatted model."
  in
  Arg.(value & flag & info [ "no_comments"; ] ~doc)

and norm =
  let doc =
    "Enable model normalisation, i.e. rules, predicates, and actions\n\
    \                are sorted in lexicographic order within the [aps]?brs \
     block."
  in
  Arg.(value & flag & info [ "norm"; "N" ] ~doc)

and format =
  let doc =
    "Export the formatted model to $(docv). Write to stdout if omitted."
  in
  Arg.(value & opt_str & info [ "o"; "output" ] ~docv:"FILE" ~doc)

(* Sim options *)
let sim_opts time steps seed =
  defaults.time <- time;
  defaults.steps <- steps;
  match seed with
  | None ->
      Random.self_init ();
      defaults.seed <- Some (Random.bits ())
  | Some _ as s -> defaults.seed <- s

let sim_opts_t =
  let time =
    let doc =
      "Set the maximum simulation time.\n\
      \               This option is only valid for stochastic models."
    in
    Arg.(
      value & opt float defaults.time
      & info [ "T"; "simulation-time" ] ~docv:"FLOAT" ~doc)
  in
  let steps =
    let doc =
      "Set the maximum number of simulation steps. This option is valid\n\
      \               only for deterministic and probabilistic models."
    in
    Arg.(
      value & opt int defaults.steps
      & info [ "S"; "simulation-steps" ] ~docv:"INT" ~doc)
  in
  let seed =
    let doc =
      "Initialise the pseudo-random number generator using $(docv) as seed."
    in
    Arg.(value & opt (some int) None & info [ "seed" ] ~docv:"INT" ~doc)
  in
  Term.(const sim_opts $ time $ steps $ seed)

(* Full options *)
let full_opts states = defaults.max_states <- states

let full_opts_t =
  let states =
    let doc = "Set the maximum number of states." in
    Arg.(
      value
      & opt int defaults.max_states
      & info [ "M"; "max-states" ] ~docv:"INT" ~doc)
  in
  Term.(const full_opts $ states)

(* Commandline *)
let run f typ =
  check_dot ();
  set_tikz_config ();
  if check_states () then (
    defaults.model <- f;
    typ)
  else Exit

let run_sim _copts _sopts f = run f Sim
let run_check _check_opts f = run f Check
let run_full _copts _fopts f = run f Full
let run_format _copts f = run f Fmt

let mdl_file =
  let doc = "Input model. Read from stdin if omitted." in
  Arg.(value & pos 0 (some file) None & info [] ~docv:"FILE" ~doc)

let sim_cmd =
  let doc = "Simulate a model." in
  Cmd.v
    (Cmd.info "sim" ~doc ~exits:Cmd.Exit.defaults ~man:[])
    Term.(const run_sim $ copts_t $ sim_opts_t $ mdl_file)

let check_cmd =
  let doc = "Parse a model and check its validity."
  and check_opts consts debug ext decls quiet verbose warn nocols =
    defaults.consts <- consts;
    defaults.debug <- debug;
    defaults.out_format <- ext;
    defaults.export_decls <- decls;
    defaults.quiet <- quiet;
    defaults.verb <- verbose;
    defaults.warn <- warn;
    defaults.colors <- not nocols
  in
  let check_opts_t =
    Term.(
      const check_opts $ consts $ debug $ ext $ decls $ quiet $ verbose
      $ warn $ nocols)
  in
  Cmd.v
    (Cmd.info "validate" ~doc ~exits:Cmd.Exit.defaults ~man:[])
    Term.(const run_check $ check_opts_t $ mdl_file)

let format_cmd =
  let doc = "Automatically reformat a model."
  and fmt_opts consts debug quiet verbose warn nocols columns no_comments norm format =
    defaults.consts <- consts;
    defaults.debug <- debug;
    defaults.quiet <- quiet;
    defaults.verb <- verbose;
    defaults.warn <- warn;
    defaults.colors <- not nocols;
    defaults.fmt_columns <- columns;
    defaults.fmt_no_comments <- no_comments;
    defaults.fmt_norm <- norm;
    defaults.fmt_out <- format
  in
  let fmt_opts_t =
    Term.(
      const fmt_opts $ consts $ debug $ quiet $ verbose $ warn $ nocols
      $ columns $ no_comments $ norm $ format)
  in
  Cmd.v
    (Cmd.info "format" ~doc ~exits:Cmd.Exit.defaults ~man:[])
    Term.(const run_format $ fmt_opts_t $ mdl_file)

let full_cmd =
  let doc = "Compute the transition system of a model." in
  Cmd.v
    (Cmd.info "full" ~doc ~exits:Cmd.Exit.defaults ~man:[])
    Term.(const run_full $ copts_t $ full_opts_t $ mdl_file)

let run_default cfg =
  match cfg with
  | true ->
      eval_config Format.std_formatter ();
      `Ok Exit
  | false -> `Help (`Pager, None)

let default_cmd =
  let cfg =
    let doc = "Print a summary of your configuration." in
    Arg.(value & flag & info [ "C"; "config" ] ~doc)
  in
  Term.(ret (const run_default $ cfg))

let cmds = [ check_cmd; sim_cmd; full_cmd; format_cmd ]

let parse_cmds =
  let doc =
    "An implementation of Bigraphical Reactive System (BRS)\n\
    \             that supports bigraphs with sharing, stochastic reaction \
     rules,\n\
    \             rule priorities and functional rules."
  in

  Cmd.group
    (Cmd.info "bigrapher" ~version:Version.version ~doc
       ~exits:Cmd.Exit.defaults ~man:[])
    ~default:default_cmd cmds
  |> Cmd.eval_value
